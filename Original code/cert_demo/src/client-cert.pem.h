const unsigned char client_cert[] = { "\
Certificate:\n\
    Data:\n\
        Version: 3 (0x2)\n\
        Serial Number:\n\
            c5:d7:6c:11:36:f0:35:e1\n\
        Signature Algorithm: md5WithRSAEncryption\n\
        Issuer: C=US, ST=Oregon, L=Portland, O=yaSSL, OU=programming, CN=www.yassl.com/emailAddress=info@yassl.com\n\
        Validity\n\
            Not Before: Jun 30 18:39:39 2010 GMT\n\
            Not After : Mar 26 18:39:40 2013 GMT\n\
        Subject: C=US, ST=Oregon, L=Portland, O=yaSSL, OU=programming, CN=www.yassl.com/emailAddress=info@yassl.com\n\
        Subject Public Key Info:\n\
            Public Key Algorithm: rsaEncryption\n\
            RSA Public Key: (512 bit)\n\
                Modulus (512 bit):\n\
                    00:bd:51:4a:14:fd:6a:19:84:0c:33:38:fc:27:32:\n\
                    9c:97:0b:fc:a4:18:60:69:4e:d9:d8:78:50:0b:e9:\n\
                    20:5d:d6:1d:70:1c:0c:24:9f:23:82:cc:3a:01:d5:\n\
                    97:17:b2:73:6c:86:cf:b5:f1:e5:ce:68:0c:d9:a2:\n\
                    12:39:7c:f2:53\n\
                Exponent: 65537 (0x10001)\n\
        X509v3 extensions:\n\
            X509v3 Subject Key Identifier: \n\
                5C:F7:29:21:69:7A:09:78:9E:7B:CD:53:42:02:EC:CE:29:0D:11:DF\n\
            X509v3 Authority Key Identifier: \n\
                keyid:5C:F7:29:21:69:7A:09:78:9E:7B:CD:53:42:02:EC:CE:29:0D:11:DF\n\
                DirName:/C=US/ST=Oregon/L=Portland/O=yaSSL/OU=programming/CN=www.yassl.com/emailAddress=info@yassl.com\n\
                serial:C5:D7:6C:11:36:F0:35:E1\n\
\n\
            X509v3 Basic Constraints: \n\
                CA:TRUE\n\
    Signature Algorithm: md5WithRSAEncryption\n\
        b4:a5:f1:71:26:4d:b9:ff:54:f3:09:1f:ac:e1:19:59:e5:ec:\n\
        57:e3:f1:0b:b2:8f:f3:29:eb:6b:c6:fa:27:33:3e:91:d0:77:\n\
        43:c9:ce:1e:0f:71:07:a9:f7:26:e0:7e:ff:30:7d:52:0a:e1:\n\
        80:48:46:bb:99:e9:d9:77:ce:75\n\
-----BEGIN CERTIFICATE-----\n\
MIIDDjCCArigAwIBAgIJAMXXbBE28DXhMA0GCSqGSIb3DQEBBAUAMIGOMQswCQYD\n\
VQQGEwJVUzEPMA0GA1UECBMGT3JlZ29uMREwDwYDVQQHEwhQb3J0bGFuZDEOMAwG\n\
A1UEChMFeWFTU0wxFDASBgNVBAsTC3Byb2dyYW1taW5nMRYwFAYDVQQDEw13d3cu\n\
eWFzc2wuY29tMR0wGwYJKoZIhvcNAQkBFg5pbmZvQHlhc3NsLmNvbTAeFw0xMDA2\n\
MzAxODM5MzlaFw0xMzAzMjYxODM5NDBaMIGOMQswCQYDVQQGEwJVUzEPMA0GA1UE\n\
CBMGT3JlZ29uMREwDwYDVQQHEwhQb3J0bGFuZDEOMAwGA1UEChMFeWFTU0wxFDAS\n\
BgNVBAsTC3Byb2dyYW1taW5nMRYwFAYDVQQDEw13d3cueWFzc2wuY29tMR0wGwYJ\n\
KoZIhvcNAQkBFg5pbmZvQHlhc3NsLmNvbTBcMA0GCSqGSIb3DQEBAQUAA0sAMEgC\n\
QQC9UUoU/WoZhAwzOPwnMpyXC/ykGGBpTtnYeFAL6SBd1h1wHAwknyOCzDoB1ZcX\n\
snNshs+18eXOaAzZohI5fPJTAgMBAAGjgfYwgfMwHQYDVR0OBBYEFFz3KSFpegl4\n\
nnvNU0IC7M4pDRHfMIHDBgNVHSMEgbswgbiAFFz3KSFpegl4nnvNU0IC7M4pDRHf\n\
oYGUpIGRMIGOMQswCQYDVQQGEwJVUzEPMA0GA1UECBMGT3JlZ29uMREwDwYDVQQH\n\
EwhQb3J0bGFuZDEOMAwGA1UEChMFeWFTU0wxFDASBgNVBAsTC3Byb2dyYW1taW5n\n\
MRYwFAYDVQQDEw13d3cueWFzc2wuY29tMR0wGwYJKoZIhvcNAQkBFg5pbmZvQHlh\n\
c3NsLmNvbYIJAMXXbBE28DXhMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEEBQAD\n\
QQC0pfFxJk25/1TzCR+s4RlZ5exX4/ELso/zKetrxvonMz6R0HdDyc4eD3EHqfcm\n\
4H7/MH1SCuGASEa7menZd851\n\
-----END CERTIFICATE-----\n\
" };
