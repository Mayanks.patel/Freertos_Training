/*
 * Copyright (C) 2013, Marvell International Ltd.
 * All Rights Reserved.
 */

/*
 * Note:
 * A number of Debug messages have been put in different threads to enable
 * debugging. However, since all the activities are very time critical, the
 * size of debug messages has been kept small. The meaning of the messages
 * are as follows:
 *
 * UB D : UnBlocking Decode thread
 * D UB : Decode thread UnBlocked
 * D B	: Decode thread Blocked
 * UB H	: Unblocking HTTP data fetch thread
 * H UB	: HTTP data fetch thread unblocked
 * H B	: HTTP data fetch thread blocked
 * W Buf[num]	: MP3 data buffer number <num> written
 * R Buf[num]	: Reading MP3 data buffer <num>
 *
 * To enable debugging set APPCONFIG_DEBUG_ENABLE=1 from Makefile
 */
#include <wmstdio.h>
#include <wm_os.h>
#include <wm_net.h>
#include <mdev_usb_host.h>
#include <httpc.h>
#include <http_parse.h>
#include <http-strings.h>
#include <psm.h>
#include <app_framework.h>
#include <appln_dbg.h>
#include <mp3dec.h>
os_thread_stack_define(audio_decode_stack, 1024);
os_thread_t audio_decode_thread;

os_thread_stack_define(audio_stream_stack, 1024);
os_thread_t audio_stream_thread;

static os_semaphore_t fetch_data_sem;
static os_semaphore_t decode_sem;

static uint8_t usb_drv_buffer[1024];

#define MAX_DECODE_ERRORS	5

#define MP3_PKT_SIZE		384
#define MAX_MP3_DATA_BUF_CNT	8

/* The number 6 below in MAX_MP3_DATA_BUF_SIZE is arbritary number and
 * has no relation to any other macros.
 */
#define MAX_MP3_DATA_BUF_SIZE	(6 * MP3_PKT_SIZE)
#define MAX_DECODED_SAMPLES	24

#define ERR_FETCH_ERROR		-2
#define ERR_DECODE_ERROR	-3
/* These buffers will hold the mp3 data that will be downloaded from the server.
 * For MC200, heap must be in SRAM1 which is small compared to SRAM0.
 * Hence, mp3_data cannot be dynamically allocated from heap.
 * Instead, it will be statically allocated from .bss which is in SRAM0.
 */
#ifdef CONFIG_CPU_MC200
uint8_t mp3_data[MAX_MP3_DATA_BUF_CNT + 1][MAX_MP3_DATA_BUF_SIZE];
#else
uint8_t *mp3_data[MAX_MP3_DATA_BUF_CNT + 1];
#endif

/* These buffers will be allocated memory from heap and will be used to
 * store the decoded raw PCM data. There will be 2 such buffers used
 * alternatingly. Please do not change the size
 */
static int16_t *audio_buffer[2];

/* This flag which is set by the decode thread denotes that the audio
 * streaming has now started
 */
static uint8_t started;

/* This signifies that the end of the MP3 file has been reached */
static uint8_t end_of_file;
mdev_t *p;

/* Data structures required by the MP3 Decoder */
HMP3Decoder hMP3Decoder;
MP3FrameInfo mp3FrameInfo;

/* The buffer indices for reading and writing the MP3 DATA Buffers */
static int rindex, windex;
int get_next_read_index()
{
	rindex++;
	if (rindex >= MAX_MP3_DATA_BUF_CNT)
		rindex = 0;
/* If rindex becomes equal to windex, this means that we are
 * trying to read a location that has not yet been written.
 * Hence, decrement rindex and return -1, notifying an error.
 */
	if (rindex == windex) {
		rindex--;
		return -1;
	}
	return rindex;
}
int get_next_write_index()
{
	windex++;
	if (windex >= MAX_MP3_DATA_BUF_CNT)
		windex = 0;
	return windex;
}

/* A single http_read_content() call may not give the complete data that
 * is expected from the web server. This function guarantees that the buffer
 * will be filled completely
 */
int get_full_http_data(http_session_t session, uint8_t *buf, int size)
{
	int len = 0;
	while (size) {
		len = http_read_content(session, (void *)buf, size);
		if (len <= 0) {
			return -WM_FAIL;
		}
		size -= len;
		buf += len;
	}
	return WM_SUCCESS;
}

/* The index of audio_buffer which will be used for writing the next
 * decoded raw PCM data.
 */
static uint8_t w_audio_buf_index;

/* Return the correct data's pointer to the USB thread to be used for
 * playing audio on the speakers
 */
void *get_next_ptr(size)
{
	static int sample_cnt;
/* The index of audio_buffer which will be used for reading the next
 * decoded raw PCM data.
 */
	static uint8_t r_audio_buf_index;
	static void *sample_ptr;
/* If no more decoded samples are available, fetch them from the
 * next buffer
 */
	if (!sample_cnt) {
		/* If this condition is true, it means that the decode
		 * logic is still writing to the buffer which this
		 * thread is trying to read. Hence, return NULL pointer.
		 */
		if (r_audio_buf_index == w_audio_buf_index) {
			return NULL;
		}
		sample_ptr = (void *)audio_buffer[r_audio_buf_index];
		r_audio_buf_index ^= 1;
		dbg("UB D");
		/* Notify the Mp3 decode thread to start the next decoding */
		os_semaphore_put(&decode_sem);
		/* A single decoding gives us MAX_DECODED_SAMPLES packets.
		 * One of them will be returned immediately and the
		 * remaining (MAX_DECODED_SAMPLES - 1) in subsequent calls.
		 * Hence initialising to (MAX_DECODED_SAMPLES - 1)
		 */
		sample_cnt = MAX_DECODED_SAMPLES - 1;
	} else {
		sample_ptr += size;
		sample_cnt--;
	}
	return sample_ptr;
}

int find_valid_mp3_header(unsigned char *mp3_ptr, int bytes_left)
{
	int offset, g_offset = 0;
	MP3FrameInfo l_mp3FrameInfo;
	while (bytes_left) {
		offset = MP3FindSyncWord(mp3_ptr, bytes_left);
		if (offset < 0)
			return offset;
		g_offset += offset;
		mp3_ptr += offset;
		bytes_left -= offset;
		if (!MP3GetNextFrameInfo(hMP3Decoder, &l_mp3FrameInfo,
				mp3_ptr)) {
			return g_offset;
		} else {
			mp3_ptr++;
			g_offset++;
			bytes_left--;
		}
	}
	return -1;
}

int decode_data(int buf_index)
{

	int offset, err;
	static int index, underflow;
	static uint8_t *read_ptr;
	static int bytes_left;
	uint8_t *t_read_ptr;
	int t_bytes_left;
	int16_t *samples;
	static uint8_t internal_flag;

	samples = audio_buffer[buf_index];
	if ((bytes_left < MP3_PKT_SIZE) || (underflow == 1)) {
		index = get_next_read_index();
		dbg("R Buf%d", index);
		if (index < 0)  {
			if (end_of_file == 1) {
				started = 0;
				return WM_SUCCESS;
			}
			return ERR_FETCH_ERROR;
		}
		underflow = 0;
		/* If index is 0, we just set the read_ptr to point to the
		 * first location of the buffer. For all other cases, we
		 * increment the bytes_left variable so that the read_ptr can
		 * read through frames crossing the buffer boundaries.
		 */
		if (index == 0) {
			read_ptr = &mp3_data[0][0];
			bytes_left  = 0;
		}
		bytes_left += MAX_MP3_DATA_BUF_SIZE;
		/* If an MP3 frame spans beyond the (MAX_MP3_DATA_BUF_CNT - 1)
		 * index location, it will be continued at the location 0 since
		 * we are using a circular queue. However, since the MP3
		 * decoder is not aware of the circular queue, it will get just
		 * a partial frame and the ERR_MP3_INDATA_UNDERFLOW error will
		 * occur. To prevent this, in the data fetch logic, we copy the
		 * contents of index location 0, to the location
		 * MAX_MP3_DATA_BUF_CNT so that the decoder will get a
		 * continuous frame. Since the size of this redundant buffer is
		 * MAX_MP3_DATA_BUF_SIZE, that size is added here.
		 */
		if (index == (MAX_MP3_DATA_BUF_CNT - 1))
			bytes_left += MAX_MP3_DATA_BUF_SIZE;
		dbg("UB H");
		/* Notify the HTTP data fetch thread to start the next fetch.
		 * However, for the first 2 runs, do not notify immediately
		 * since this may over write the buffer that we are using for
		 * reading the MP3 data.
		 */
		if (internal_flag >= 2)
			os_semaphore_put(&fetch_data_sem);
		else
			internal_flag++;
	}
	offset = find_valid_mp3_header((unsigned char *)read_ptr, bytes_left);
	if (offset < 0) {
		bytes_left -= bytes_left;
		read_ptr += bytes_left;
		return ERR_DECODE_ERROR;
	}
	bytes_left -= offset;
	read_ptr += offset;
	/* Save the values in temporary variables. If a data undeflow error
	 * is received, we reset the read_ptr and bytes_left to the original
	 * values and then during the next run of this function, add the next
	 * chunk of the fetch buffer so that the underflow error does not
	 * occur again.
	 */
	t_bytes_left = bytes_left;
	t_read_ptr = read_ptr;
	err = MP3Decode(hMP3Decoder, (unsigned char **)&read_ptr,
			&bytes_left, samples, 0);
	if (err) {
		dbg("MP3 Error %d, Bytes left %d", err, bytes_left);
		/* Error occurred */
		switch (err) {
		case ERR_MP3_INVALID_FRAMEHEADER:
			/* If the header is invalid, move onto the next
			 * location.
			 */
			bytes_left -= 1;
			read_ptr += 1;
			break;
		case ERR_MP3_INDATA_UNDERFLOW:
			/* Reset the variables to their older values */
			bytes_left = t_bytes_left;
			read_ptr = t_read_ptr;
			underflow = 1;
			break;
		case ERR_MP3_MAINDATA_UNDERFLOW:
			underflow = 1;
			break;
		case ERR_MP3_FREE_BITRATE_SYNC:
		default:
			break;
		}
		if ((index == (MAX_MP3_DATA_BUF_CNT - 1)) &&
				(bytes_left <= MAX_MP3_DATA_BUF_SIZE))
			bytes_left = 0;
		return ERR_DECODE_ERROR;
	} else {
		if ((index == (MAX_MP3_DATA_BUF_CNT - 1)) &&
				(bytes_left <= MAX_MP3_DATA_BUF_SIZE))
			bytes_left = 0;
		started = 1;
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}

void audio_decode_thread_func()
{
	int ret;
	uint8_t error_cnt = 0;
	dbg("Audio Thread started\n");
	os_semaphore_get(&decode_sem, OS_WAIT_FOREVER);
	while (1) {
		dbg("D UB");
		/* If an error occurs in decoding, try again immediately.
		 * If it fails for MAX_DECODE_ERRORS successive attempts,
		 * sleep for some time and then continue.
		 */
		if ((ret = decode_data(w_audio_buf_index)) == WM_SUCCESS) {
			error_cnt = 0;
			w_audio_buf_index ^= 1;
			dbg("D B");
			os_semaphore_get(&decode_sem, OS_WAIT_FOREVER);
		} else {
			error_cnt++;
			if ((error_cnt >= MAX_DECODE_ERRORS) ||
					(ret == ERR_DECODE_ERROR)) {
				error_cnt = 0;
				os_thread_sleep(os_msec_to_ticks(3));
			}
		}
	}
}
#define NETWORK_MOD_NAME	"network"
#define VAR_URL			"url"
#define DEF_URL			"http://10.31.131.74/piano2.mp3"

void audio_stream_thread_func()
{
	http_session_t session;
	int status, offset, index, i;
	char url[100];
	http_req_t req;
	if (psm_get_single(NETWORK_MOD_NAME, VAR_URL, url,
			sizeof(url)) != WM_SUCCESS) {
		strncpy(url, DEF_URL, sizeof(url));
	}
	wmprintf("Using URL : %s\r\n", url);

	status = http_open_session(&session, url, NULL);
	if (status != WM_SUCCESS) {
		dbg("Could not open HTTP session with %s", url);
		goto error;
	}
	memset(&req, 0, sizeof(req));
	req.type = HTTP_GET;
	req.resource = url;
	req.version = HTTP_VER_1_1;
	status = http_prepare_req(session, &req, STANDARD_HDR_FLAGS);
	if (status != WM_SUCCESS) {
		dbg("Could not prepare HTTP request");
		goto http_error;
	}
	status = http_send_request(session, &req);
	if (status == -WM_E_INVAL) {
		dbg("Could not send HTTP request");
		goto http_error;
	}
/* First we read just one MAX_MP3_DATA_BUF_SIZE worth of data in mp3_data[1] and find
 * the offset for the MP3 Sync Word. The data after the offset is then
 * copied into mp3_data[0] and then the remaing portion of the mp3_data
 * buffer is filled. This has been done to keep the buffers Sync_Word
 * aligned.
 */
	status = get_full_http_data(session, &mp3_data[1][0], MAX_MP3_DATA_BUF_SIZE);
	if (status  != WM_SUCCESS) {
		dbg("Failed to get HTTP data");
		goto http_error;
	}
	offset = MP3FindSyncWord((unsigned char *)&mp3_data[1][0], MAX_MP3_DATA_BUF_SIZE);
	if (offset < 0) {
		dbg("Failed to get MP3 offset");
		goto http_error;
	}
	memcpy(&mp3_data[0][0], &mp3_data[1][0] + offset, MAX_MP3_DATA_BUF_SIZE - offset);
	status = get_full_http_data(session, &mp3_data[0][offset],
			(MAX_MP3_DATA_BUF_SIZE) - offset);
	os_semaphore_get(&fetch_data_sem, OS_WAIT_FOREVER);
	for (i=1; i< MAX_MP3_DATA_BUF_CNT; i++) {
		dbg("Reading %d", i);
		if ((status = get_full_http_data(session, &mp3_data[i][0],
				MAX_MP3_DATA_BUF_SIZE)) != WM_SUCCESS)
			break;
		os_semaphore_get(&fetch_data_sem, OS_WAIT_FOREVER);
	}
	if (status  != WM_SUCCESS) {
		dbg("Failed to get HTTP data");
		goto http_error;
	}
	memcpy(&mp3_data[i][0], &mp3_data[0][0], MAX_MP3_DATA_BUF_SIZE);
	windex = MAX_MP3_DATA_BUF_CNT;
	rindex = -1;
	hMP3Decoder = MP3InitDecoder();
	end_of_file = 0;
	os_semaphore_put(&decode_sem);
	while (1) {
		dbg("H B");
		/* Wait till a buffer is available for writing */
		os_semaphore_get(&fetch_data_sem, OS_WAIT_FOREVER);
		dbg("H UB");
		index = get_next_write_index();
		status = get_full_http_data(session,
				&mp3_data[index][0], MAX_MP3_DATA_BUF_SIZE);
		if (status == WM_SUCCESS) {
			dbg("W Buf%d", index);
			/* Keep a copy of data from index location 0 at
			 * location MAX_MP3_DATA_BUF_CNT so that the
			 * MP3 decoder always gets continuous data.
			 */
			if (index == 0)
				memcpy(&mp3_data[MAX_MP3_DATA_BUF_CNT][0],
					&mp3_data[index][0],
					MAX_MP3_DATA_BUF_SIZE);
		} else {
			break;
		}
	}
http_error:
	http_close_session(&session);
	end_of_file = 1;
error:
	wmprintf("End of audio stream thread\r\n");
	while (1) {
		os_thread_sleep(os_msec_to_ticks(100));
	}
}

void *AudioCallback(int size)
{
	if (!started) 
		return NULL;
	else
		return get_next_ptr(size);
}
int audio_init()
{
	int ret;
	struct usb_uac_drv_data drv;
	memset(&drv, 0, sizeof(drv));
/* Initialize usb host driver */
	drv.buf = usb_drv_buffer;
	drv.len = sizeof(usb_drv_buffer);
/* Set highest priority for the usb threads since the audio output
 * is time critical.
 */
	USBConfig_SetTaskPriority(OS_PRIO_1);
	ret = usb_host_uac_drv_init(&drv);
	if (ret != WM_SUCCESS) {
		dbg("Failed to init usb host driver");
		return -WM_FAIL;
	}
	p = usb_host_drv_open("MDEV_USB_HOST");
	if (p == NULL) {
		dbg("Failed to get mdev handle");
		return -WM_FAIL;
	}
	ret = usb_host_drv_start(p);
	if (ret != WM_SUCCESS) {
		dbg("Failed to start host driver: %d", ret);
		return -WM_FAIL;
	}

	ret = os_semaphore_create_counting(&fetch_data_sem,
			"fetch_data_sem",
			MAX_MP3_DATA_BUF_CNT, MAX_MP3_DATA_BUF_CNT);

	if (ret == -WM_FAIL) {
		dbg("Failed to create decode semaphore");
		return -WM_FAIL;
	}
	ret = os_semaphore_create_counting(&decode_sem,
				  "decode_sem", 1, 0);
	if (ret == -WM_FAIL) {
		dbg("Failed to create decode semaphore");
		return -WM_FAIL;
	}
	register_OUT_packet_callbacks(AudioCallback, NULL);
	return WM_SUCCESS;
}

int audio_stream_start()
{
	int ret;
	os_thread_sleep(os_msec_to_ticks(1000));
	ret = audio_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: Audio Initialisation failed!!");
		return ret;
	}
	audio_buffer[0] = os_mem_alloc(sizeof(int16_t) * 4096);
	if (!audio_buffer[0]) {
		dbg("audio buffer 0 failed");
		return -WM_E_NOMEM;
	}
	audio_buffer[1] = os_mem_alloc(sizeof(int16_t) * 4096);
	if (!audio_buffer[1]) {
		dbg("audio buffer 1 failed");
		os_mem_free(audio_buffer[0]);
		return -WM_E_NOMEM;
	}
	wmprintf("Audio buffer allocation successful\r\n");

#ifndef CONFIG_CPU_MC200
	/* Allocate memory for mp3 buffers
	 * For MC200 it is statically allocated. */
	int i;
	for (i = 0; i < MAX_MP3_DATA_BUF_CNT + 1; i++) {
		mp3_data[i] = os_mem_alloc(MAX_MP3_DATA_BUF_SIZE);
		if (mp3_data[i] == NULL) {
			wmprintf("Failed to allocate mp3 buffer %d\r\n", i);
			os_mem_free(audio_buffer[0]);
			os_mem_free(audio_buffer[1]);
			while (--i >= 0)
				os_mem_free(mp3_data[i]);
			return -WM_E_NOMEM;
		}
	}
	wmprintf("Mp3 buffer allocation successful\r\n");
#endif

	ret = os_thread_create(&audio_decode_thread, "audio_decode",
			audio_decode_thread_func, 0,
			&audio_decode_stack, OS_PRIO_2);
	if (ret != WM_SUCCESS) {
		dbg("Audio Decode thread creation error");
		os_mem_free(audio_buffer[0]);
		os_mem_free(audio_buffer[1]);
		return ret;
	}
	ret = os_thread_create(&audio_stream_thread, "audio_stream",
			audio_stream_thread_func, 0,
			&audio_stream_stack, OS_PRIO_3);
	if (ret != WM_SUCCESS) {
		os_thread_delete(&audio_decode_thread);
		os_mem_free(audio_buffer[0]);
		os_mem_free(audio_buffer[1]);
		dbg("Audio Stream thread creation error");
		return ret;
	}
	return ret;
}
