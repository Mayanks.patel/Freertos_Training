/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/* Define applicatipn specific events */
typedef enum {
	APP_EVT_START_AUDIO_STREAM = APP_CTRL_EVT_MAX + 1,
} app_event_t;

