/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 *
 *  Simple Wireless MP3 audio streaming application using USB Host
 *  Stack and MP3 decoder library.
 *
 *  Summary:
 *
 *  Connecting to Network:
 *
 *  A USB Speaker is connected to the device over the USB-OTG interface.
 *  If provisioning has been enabled
 *  (by setting APPCONFIG_PROVISIONING_ENABLE=y)
 *  application starts a Micro-AP network and a Web Server.
 *  Connect to the micro-AP and go to http://192.168.10.1 from a web browser
 *  and go through the provisioning steps.
 *
 *  If provisioning has not been enabled, please use psm commands to
 *  set the network parameters using
 *  # psm-set network ssid <SSID>
 *  # psm-set network passphrase <passphrase>
 *  # psm-set network security <security> ( 4 for wpa2 , 3 for wpa, 0 for Open)
 *  # psm-set network configured 1
 *  Reboot the device after these steps
 *
 *  Setting the MP3 file location:
 *
 *  Set the mp3 url using
 *  # psm-set network url <mp3_url>
 *  You will have to reboot the board for this change to take effect.
 *
 *  Streaming audio:
 *  Connect a USB speaker/headset to the USB-OTG port of the board.
 *  When the device connects to the configured network, it will start
 *  fetching the MP3 file from the provided location and start playing
 *  it on the speakers. The streaming will stop after the end of file
 *  is reached.
 *
 */

#include <wmstdio.h>
#include <wm_os.h>
#include <wm_net.h>
#include <mdev_usb_host.h>
#include <wmsysinfo.h>
#include <wlan.h>
#include <httpc.h>
#include <http_parse.h>
#include <http-strings.h>
#include <psm.h>
#include <mdev_gpio.h>
#include <app_framework.h>
#include <wmtime.h>
#include <appln_dbg.h>
#include <appln_cb.h>
#include <cli.h>
#include <mrvlperf.h>
#include <reset_prov_helper.h>
#include <mp3dec.h>
#include <overlays.h>
#include "app_events.h"

int audio_stream_start();
extern struct overlay a;

appln_config_t appln_cfg = {
	.ssid = "Marvell_Speaker_Demo",
	.passphrase = "marvellwm",
	.hostname = "mdemo"
};

int ftfs_api_version = 100;
char *ftfs_part_name = "ftfs";
struct fs *fs;

static os_timer_t audio_stream_start_timer;

static int provisioned;

#define AUDIO_STREAM_START_TIMER (5 * 1000)
static void audio_stream_start_cb(os_timer_arg_t arg)
{
	app_ctrl_notify_application_event(APP_EVT_START_AUDIO_STREAM, NULL);
}

int appln_config_init()
{
	/* Initialize reset to provisioning push button settings */
	appln_cfg.reset_prov_pb_gpio = board_button_2();
	return 0;
}
#ifdef APPCONFIG_PROVISIONING_ENABLE
void event_uap_started(void *data)
{
	if (!provisioned)
		app_provisioning_start(PROVISIONING_WLANNW);
}

void event_uap_stopped(void *data)
{
	dbg("uap interface stopped");
}

void event_prov_done(void *data)
{
	int ret;
	hp_configure_reset_prov_pushbutton();
	app_provisioning_stop();
	ret = app_httpd_stop();
	if (ret != WM_SUCCESS)
		dbg("Error: Failed to Stop HTTP Server");
	ret = app_uap_stop();
	if (ret != WM_SUCCESS)
		dbg("Error: Failed to Stop Micro-AP");
}

static void event_normal_reset_prov(void *data)
{
	app_reboot(REASON_SWITCH_TO_PROV);
}
#endif /* APPCONFIG_PROVISIONING_ENABLE */
void event_normal_connected(void *data)
{
	char ip[16];
	app_network_ip_get(ip);
	dbg("Connected to Home Network with IP address = %s", ip);
	os_timer_activate(&audio_stream_start_timer);
}
void event_wlan_init_done(void *data)
{
	int ret;
	os_timer_tick ticks = 100;
	provisioned = (int)data;
	psm_cli_init();
#ifdef APPCONFIG_PROVISIONING_ENABLE
	if (!provisioned)
		ticks = os_msec_to_ticks(AUDIO_STREAM_START_TIMER);
#endif /* APPCONFIG_PROVISIONING_ENABLE */
	ret = os_timer_create(&audio_stream_start_timer,
				  "audio-stream-start-timer",
				  ticks,
				  &audio_stream_start_cb,
				  NULL,
				  OS_TIMER_ONE_SHOT,
				  OS_TIMER_NO_ACTIVATE);
	if (ret != WM_SUCCESS)
		dbg("Failed to create audio stream start timer");

	dbg("Event: WLAN_INIT_DONE provisioned=%d", provisioned);
	if (provisioned) {
		hp_configure_reset_prov_pushbutton();
		app_sta_start();
	} else {
#ifdef APPCONFIG_PROVISIONING_ENABLE
		app_uap_start_with_dhcp(appln_cfg.ssid, appln_cfg.passphrase);
		if (app_httpd_with_fs_start(ftfs_api_version,
				ftfs_part_name, &fs) != WM_SUCCESS)
			dbg("Failed to start HTTP Server");
#endif /* APPCONFIG_PROVISIONING_ENABLE */
	}
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
#ifdef APPCONFIG_PROVISIONING_ENABLE
	case AF_EVT_UAP_STARTED:
		event_uap_started(data);
		break;
	case AF_EVT_UAP_STOPPED:
		event_uap_stopped(data);
		break;
	case AF_EVT_PROV_DONE:
		event_prov_done(data);
		break;
	case AF_EVT_NORMAL_RESET_PROV:
		event_normal_reset_prov(data);
		break;
#endif /* APPCONFIG_PROVISIONING_ENABLE */
	case APP_EVT_START_AUDIO_STREAM:
#ifdef APPCONFIG_PROVISIONING_ENABLE
		overlay_load(&a, 1);
#endif /* APPCONFIG_PROVISIONING_ENABLE */
		audio_stream_start();
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;

	ret = wmstdio_init(UART0_ID, 0);
	if (ret != WM_SUCCESS) {
		while (1)
			;
		/* do nothing -- stall */
	}

	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		while (1)
			;
	       /* do nothing -- stall */
	}
	ret = gpio_drv_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: gpio_drv_init failed");
		while (1)
			;
	       /* do nothing -- stall */
	}
	return;
}

/*
 * The application entry point is main(). At this point, minimal
 * initialization of the hardware is done, and also the RTOS is
 * initialized.
 */
int main()
{
#ifdef APPCONFIG_PROVISIONING_ENABLE
	overlay_load(&a, 0);
#endif /* APPCONFIG_PROVISIONING_ENABLE */
	modules_init();

	/* Set WiFi driver level TX retry count limit, needed to improve
	 * streaming experience over TCP */
	wifi_set_packet_retry_count(50);

	appln_config_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");
	wmprintf("[usb-audio-app] USB Audio Class demo application "
				"started\r\n");

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
		while (1)
			;
		/* do nothing -- stall */
	}

	return 0;
}
