#include <wmstdio.h>
#include <wm_os.h>
#include <mdev_gpio.h>
#include <mdev_pinmux.h>
#include <board.h>

#define GPIO_LED_FN	PINMUX_FUNCTION 0
#if defined( CONFIG_CPU_MW300 )
#define GPIO_PUSHBUTTON_FN PINMUX_FUNCTION_1
#define GPIO_PUSHBUTTON_FN PIN_MUX_FUNCTION_0
#endif

static unsigned int gpio_led;
static unsigned int gpio_pushbutton1 , gpio_pushbutton2;
static unsigned int gpio_led_state;

static void configure_gpio()
static void pushbutton_cb( int pin , void *data );
static void gpio_led_on( void );

int main( void )
{
    int ret = 0;
    
    ret = wmstdio_init( UART0_ID , 0 );
    if( ret == -WM_FAIL )
    {
		wnprintf("Failed to Initialized console on uart0\r\n");
		return -1;
	}
	
	wmprintf("Exercise1 application started....\n");
	
	gpio_led = ( board_led_2() ).gpio;
	gpio_pushbutton1 = board_button1();
	gpio_pushbutton2 = board_button2();
	
	wmprintf( "LED Pin : %d\r\n",gpio_led );
	wmprintf("Pushbutton pin : %d\r\n",gpio_pushbutton1);
	wmprintf("Pushbutton pin : %d\r\n",gpio_pushbutton2);
		
	configure_gpio();
	
	while(1)
	;
	
	return 0;
}

static void configure_gpio()
{
	mdev_t *pinmux_dev , *gpio_dev;
	
	pinmux_drv_init();
	
	pinmux_dev = pinmux_drv_open("MDEV_PINMUX");
	
	gpio_drv_init();
	
	gpio_dev = gpio_drv_open("MDEV_GPIO");

	pinmux_drv_setfunc(gpio_dev , gpio_led , GPIO_LED_FN );
	
	gpio_drv_write( gpio_dev , gpio_led , 1 );
	
	pinmux_drv_setfunc( gpio_dev , gpio_pushbutton1 , GPIO_INPUT );
	
	gpio_drv_setdir( gpio_dev , gpio_pushbutton1 , GPIO_INPUT );
	
	gpio_drv_set_cb( gpio_dev , gpio_pushbutton1 , GPIO_INT_FALLING_EDGE , 
								NULL , pushbuton_cb );
	
	pinmux_drv_close( pinmux_dev );
	
	gpio_drv_close( gpio_dev );
}

static void pushbutton_cb( int pin , void *data )
{
	gpio_led_on();
}


static void gpio_led_on( void )
{
	mdev_t *gpio_dev = gpio_drv_open("MDEV_GPIO");
	
	gpio_drv_write(gpio_dev , gpio_led , 0);
	gpio_drv_close(gpio_dev);
}
