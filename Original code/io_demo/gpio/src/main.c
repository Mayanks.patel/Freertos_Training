#include <stdio.h>
#include <wmstdio.h>
#include <wm_os.h>
#include <mdev_gpio.h>
#include <mdev_pinmux.h>
#include <board.h>
#include <pthread.h>

#define GPIO_LED_FN  PINMUX_FUNCTION_0
#if defined(CONFIG_CPU_MW300)
#define GPIO_PUSHBUTTON_FN PINMUX_FUNCTION_1
#else
#define GPIO_PUSHBUTTON_FN PINMUX_FUNCTION_0
#endif

static unsigned int gpio_led;
static unsigned int gpio_pushbutton;
//static unsigned int gpio_pushbutton1;

static unsigned int gpio_led_state;


static void gpio_led_on(void)
{
	wmprintf("Led on");
	mdev_t *gpio_dev = gpio_drv_open("MDEV_GPIO");
	/* Turn on LED by writing  0 in GPIO register */
	gpio_drv_write(gpio_dev, gpio_led, 0);
	gpio_drv_close(gpio_dev);
	gpio_led_state = 1;
}

/* This function turns off the LED*/
static void gpio_led_off(void)
{
	wmprintf("Led off");
	mdev_t *gpio_dev = gpio_drv_open("MDEV_GPIO");
	/* Turn off LED by writing  1 in GPIO register */
	gpio_drv_write(gpio_dev, gpio_led, 1);
	gpio_drv_close(gpio_dev);
	gpio_led_state = 0;
}

static void pushbutton_cb( int pin , void *data )
{
	gpio_led_on();
	gpio_led_off();
    return;
}

static void configure_gpios()
{
	mdev_t *pinmux_dev , *gpio_dev;
	
	pinmux_drv_init();
	pinmux_dev = pinmux_drv_open("MDEV_PINMUX");
	
	gpio_drv_init();
	
	gpio_dev = gpio_drv_open("MDEV_GPIO");
	pinmux_drv_setfunc(pinmux_dev, gpio_led, GPIO_LED_FN);
	gpio_drv_setdir( gpio_dev , gpio_led , GPIO_OUTPUT );
	
	gpio_drv_write( gpio_dev , gpio_led , 1 );
	
	pinmux_drv_setfunc(pinmux_dev, gpio_pushbutton, GPIO_PUSHBUTTON_FN);
	gpio_drv_setdir(gpio_dev, gpio_pushbutton, GPIO_INPUT);
	gpio_drv_set_cb(gpio_dev, gpio_pushbutton, GPIO_INT_FALLING_EDGE,
			NULL,
			pushbutton_cb);
	
	pinmux_drv_close(pinmux_dev);
	gpio_drv_close(gpio_dev);
}

static void *thread1_fun( void *arg )
{
	wmprintf("Thread1 Created\n");
	configure_gpios();
}

int main()
{
	pthread_t tid;
	void *thread_result;
	int ret = 0;
	
	ret = wmstdio_init( UART0_ID , 0 );
	
	if( ret == -WM_FAIL )
	{
		wmprintf("Failed to initialize console on uart0\r\n");
	}
	
	wmprintf("Application started....\n");
	
	gpio_led = ( board_led_2() ).gpio;
	gpio_pushbutton = board_button_1();
	//gpio_pushbutton1 = board_button_2();
	
	ret = pthread_create( &tid , NULL , thread1_fun , NULL );
	
	if( ret != 0 )
	{
		wmprintf("Thread1 creation error\n");
		return 0;
	}
	
/*	ret = pthread_create( &tid2 , NULL , thread2_fun , NULL );
	
	if( ret != 0 )
	{
		wmprintf("Thread2 creation error\n");
		return 0;
	}
*/	
	wmprintf("Waiting for thread1 to finish...\n");
	ret = pthread_join( tid , &thread_result );
	
	if( ret != 0 )
	{
		wmprintf("Thread1 join Failure\n");
		return 0;
	}
	
/*	printf("Waiting for thread2 to finish..\n");
	
	if( ret != 0 )
	{
		wmprintf("Thread2 join Failure\n");
		return 0;
	}
*/	
	return 0;
}






