
*** This document shows the ble_demo application usage ****

A. Pre-requiste:

   1. ble_demo app should be compiled and flashed on our board.
   2. Need a remote device with BLE support.

B. Steps to execute on console ( minicom) for demo.

1. When ble_demo.bin file is flashed, you will observe below prints, which indicate that firmware is loaded, BT driver is registered and bluetooth stack is initialized.

# BLE Demo application Started
[bt_drv] revision=0x10
[bt_drv] IOPORT : (0x10000)
[bt_drv] Firmware Ready
[bt_drv] BT driver initialized
gap_test_cb:  event id 1
BT_ENABLE_COMPLETED_EVENT

2. Press "Enter" key to go to command prompt

3. Type "help" at command prompt to show all possible test options

# help

help
system-conf
echo <on/off>
ble-opt

4.  To start advertising, so that remote devices can search us, use option 1

# ble-opt 1
Selected menu option =1
Start Advertising

  On a remote device, which supports BLE, make sure bluetooth is switched ON. If you perform discovery (Scanning) from remote device,
  device with name "MRVL-WMSDK" should now show up. This is friendly name of our Bluetooth device.

5. To stop advertisement, use option 2.

# ble-opt 2
 Selected menu option =2
 Stop Advertising


6. In order to perform scan to find nearby remote devices, use option 3.

    NOTE: Make sure, that remote device with BLE support has it's bluetooth switched ON and is advertising, only then the remote device would show up in our discovery
          result.

# ble-opt 3
Selected menu option =3
Start Scanning

  Scanning is performed for 10 seconds and it displays devices found on console . Sample output is as below:

ble demo app handler: event id 4
 BT_DISCOVERY_RESULTS_EVENT
 BT_DISCOVERY_RESULTS_EVENT: Device 0x8F:0xBA:0x21:0x43:0x50:0x00
 Device Type 1 Address Type 0
 RSSI value -40 dbm
 Device Name Len 10 MRVl_SDK-1

ble demo app handler: event id 4
 BT_DISCOVERY_RESULTS_EVENT
 BT_DISCOVERY_RESULTS_EVENT: Device 0x59:0xAD:0x21:0x43:0x50:0x00
 Device Type 1 Address Type 0
 RSSI value -72 dbm
 Device Name Len 8 MRVL SDK

ble demo app handler: BT_DISCOVERY_COMPLETED_EVENT Discovery Completed


  Number of devices found can vary in each scan. Also multiple entries of same device can be seen, since the remote device may be advertising for more than one time in 10 seconds.



