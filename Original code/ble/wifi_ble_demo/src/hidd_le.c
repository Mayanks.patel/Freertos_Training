/*
*                Copyright 2014, Marvell International Ltd.
* This code contains confidential information of Marvell Semiconductor, Inc.
* No rights are granted herein under any patent, mask work right or copyright
* of Marvell or any third party.
* Marvell reserves the right at its sole discretion to request that this code
* be immediately returned to Marvell. This code is provided "as is".
* Marvell makes no warranties, express, implied or otherwise, regarding its
* accuracy, completeness or performance.
*/

/*!
 * \file   hidd_le.c
 * \brief  This file contains the implementation of the HOGP Device Role Profile
 * \author Marvell Semiconductor
 */

#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)

#include "mrvlstack_config.h"
#include "main_gap_api.h"
#include "main_gap_declares.h"
#include "main_gatt_api.h"
#include "hidd_le.h"
#include "hidd_main.h"
#include "types.h"
#include "utils.h"
#include "oss.h"

#include "queue.h"

#define HIDS_SERVICE_VERSION  "MRVL-LE-HIDS-10.0.0.0"

/***********************************************************************
   
                       Sturcture & Function Declarations
   
************************************************************************/
static tgatt_uuid16_service_record HIDS_SrvcRecord;
static tgatt_uuid16_include_decl_record BAS_SrvcINCL;
static tgatt_uuid16_include_decl_record SCPS_SrvcINCL;
static tgatt_uuid16_char_decl_record CharDecl_PrtlMode;
static tHOGP_PRTLMode_CharValDecl CharVal_PrtlModeVal;
//static tgatt_uuid16_char_decl_record CharDecl_KBIN;
static tgatt_uuid16_charval_decl_record_std CharVal_KBIN;
static tHOGP_KBIN_CCC KBIN_CCC;
static tHOGP_KBIN_RptRef KBIN_RPT_REF;
//static tgatt_uuid16_char_decl_record CharDecl_KBOUT;
static tHOGP_KBOut CharVal_KBOUT;
static tHOGP_KBOUT_RptRef KBOUT_RPT_REF;
static tgatt_uuid16_char_decl_record CharDecl_MouseIN;
static tgatt_uuid16_charval_decl_record_std CharVal_MouseIN;
static tHOGP_MouseIN_CCC MouseIN_CCC;
static tHOGP_MouseIN_RptRef MouseIN_RPT_REF;
static tgatt_uuid16_char_decl_record CharDecl_ReportMap;
static tgatt_uuid16_charval_decl_record_std CharVal_ReportMap;
static tHOGP_ReportMap_ERR CharDescERR_ReportMap_BAL;
static tHOGP_ReportMap_ERR CharDescERR_ReportMap_ScRef;
static tgatt_uuid16_char_decl_record CharDecl_BT_KBIN;
static tgatt_uuid16_charval_decl_record_std CharVal_BT_KBIN;
static tHOGP_BT_KBIN_CCC BT_KBIN_CCC;
//static tgatt_uuid16_char_decl_record CharDecl_BT_KBOUT;
static tHOGP_KBOut CharVal_BT_KBOUT;
//static tgatt_uuid16_char_decl_record CharDecl_BT_MouseIN;
static tgatt_uuid16_charval_decl_record_std CharVal_BT_MouseIN;
static tHOGP_BT_MouseIN_CCC BT_MouseIN_CCC;
static tgatt_uuid16_char_decl_record CharDecl_HIDInfo;
static tgatt_uuid16_charval_decl_record_std CharVal_HIDInfo;
static tgatt_uuid16_char_decl_record CharDecl_HID_CtrlPt;
static tHOGP_CtrlPoint CharVal_CtrlPt;
//static tgatt_uuid16_char_decl_record CharDecl_KB_Feature1;
static tHOGP_KB_FEAT1_Val CharVal_KB_Feature1;
static tHOGP_KBIN_RptRef KB_Feature1_RPT_REF;

extern boolean isAuthorized();



static void hidd_write_attr_value(tconn_id connID, tgatts_req *write_req, tgatt_status *p_status);
static void hidd_read_attr_value(tconn_id connID, tgatts_req *read_req, tHOGP_AttrValue *p_attrVal, tgatt_status *p_status);
static uint16 get_attr_id(tconn_id connID, uint8 rpt_ID, uint8 rpt_type);
static uint8 fetch_cccIdx(tconn_id connID);        // Return Index value for Particular Gatt connection Handle, which is mapped to its CCC.
static uint8 fetch_appId(tconn_id connID);         // Return Index value for Particular Gatt connection Handle, which is mapped to its AppID


/***********************************************************************
   
                       Macro Definitions, Global Variables & Data Sturctures
   
************************************************************************/
#define KBIN_RPT_REF_VAL (( HOGP_REPORT_TYPE_INPUT<< 8)|HOGP_REPORT_ID_KB)
#define KBOUT_RPT_REF_VAL (( HOGP_REPORT_TYPE_OUTPUT<< 8)|HOGP_REPORT_ID_KB)
#define MouseIN_RPT_REF_VAL (( HOGP_REPORT_TYPE_INPUT<< 8)|HOGP_REPORT_ID_MOUSE)
#define KB_Feature1_RPT_REF_VAL (( HOGP_REPORT_TYPE_FEATURE << 8)|HOGP_REPORT_ID_KB)


tHOGP_AttrValue attr_val; // Attr_Val is declared Global as this Pointer would be given to Stack which runs in different Context.
static boolean indicate_ack[MAX_CONN_PER_DEV]={TRUE}; //Per Gatt Client, Indication acknowledgement is maintained. Default value shall be set to 1 in INIT. And
 //when indication is sent for a Gatt Client, this value is reset to ZERO. Once we receive Indication Confirmation this value is set High for this particular client.

const uint8 reportDescriptor[] = {
    0x05, 0x01,         /* Usage page Desktop 01 */
    0x09, 0x06,         /* Usage Keyboard 06 */
    0xa1, 0x01,         /* Collection application */
        0x05, 0x07,        /* Usage page Keyboard */
        0x85, HOGP_REPORT_ID_KB,  // BTA_HD_REPT_ID_KBD,        /* Report ID 1 */
        0x19, 0xe0,        /* Usage minimum e0 (leftControl) */
        0x29, 0xe7,        /* Usage maximum e7 (right gui) */
        0x15, 0x00,        /* Logical minimum 0 */
        0x25, 0x01,        /* Logical Maximum 1 */
        0x75, 0x01,        /* Report size 1 */
        0x95, 0x08,        /* Report count 8 */
        0x81, 0x02,        /* Input Variable Abs */
        0x95, 0x01,        /* Report count 1 */
        0x75, 0x08,        /* Report size 8 */
        0x81, 0x01,        /* Input constant variable   */
        0x95, 0x05,        /* report count 5 */
        0x75, 0x01,        /* Report size 1 */
        0x05, 0x08,        /* LED page */
        0x19, 0x01,        /* Usage minimum 1 Num lock */
        0x29, 0x05,        /* Usage maximum 5 Kana  */
        0x91, 0x02,        /* Output Data, Variable, Absolute */
        0x95, 0x01,        /* Report Count 1 */
        0x75, 0x03,        /* Report Size 3 */
        0x91, 0x01,        /* Output constant, Absolute  */
        0x95, 0x06,        /* Report Count 6 */
        0x75, 0x08,        /* Report size 8 */
        0x15, 0x00,        /* Logical minimum 0 */
        0x26, 0xa4, 0x00,  /* Logical Maximum 00a4 */
        0x05, 0x07,        /* Usage page Keyboard */
        0x19, 0x00,        /* Usage minimum 0 */
        0x29, 0xa4,        /* Usage maximum a4 */
        0x81, 0x00,        /* Input data array absolute */
		0x09, 0x05, 	   /*	USAGE (Gamepad) - Keyboard Feature1 */ 
		0x75, 0x08, 	   /*	REPORT_SIZE (8) */ 
		0x95, 0x01, 	   /*	REPORT_COUNT (1) */ 
		0x15, 0x00, 	   /*	LOGICAL_MINIMUM (0) */ 
		0x25, 0x64, 	   /*	LOGICAL_MAXIMUM (100) */ 
		0xb1, 0x02, 	   /*	FEATURE (Data,Var,Abs) */ 
    0xc0,
    
    0x05, 0x01,             /* Usage page Desktop 01 */
    0x09, 0x02,             /* Usage 2 Mouse */
    0xa1, 0x01,             /* Collection appliction */
        0x09, 0x01,         /* Usage 1 pointer */
        0xa1, 0x00,         /* Collection physical */
            0x85, HOGP_REPORT_ID_MOUSE, //BTA_HD_REPT_ID_MOUSE,     /* report id 2 */
            0x05, 0x09,     /* Usage page button */
            0x19, 0x01,     /* Usage minimum 1 */
            0x29, 0x03,     /* Usage maximum 3 */
            0x15, 0x00,     /* Logical mimumum 0 */
            0x25, 0x01,     /* Logical Maximum 1 */
            0x95, 0x03,     /* Report Count 3 */
            0x75, 0x01,     /* Report size 1 */
            0x81, 0x02,     /* Input Variable Abs */
            0x95, 0x01,     /* Report Count 1 */
            0x75, 0x05,     /* Report size 5 */
            0x81, 0x03,     /* Input const var Abs */

            0x05, 0x01,     /* Usage page Desktop 01 */
            0x09, 0x30,     /* Usage X */
            0x09, 0x31,     /* Usage Y */
            0x09, 0x38,     /* Usage Wheel */
            0x15, 0x81,     /* Logical mimumum -127 */
            0x25, 0x7f,     /* Logical Maximum 127 */
            0x75, 0x08,     /* Report size 8 */
            0x95, 0x03,     /* Report Count 3 */            
            0x81, 0x06,     /* Input Variable relative */ 
        0xc0,
    0xc0,

    0x05, 0x06,                    /* USAGE_PAGE (Generic device) */ 
    0x09, 0x3b,                    /*   USAGE (Battery Strength) */ 
    0xa1, 0x01,                    /*   COLLECTION (Application) */
    0x85, HOGP_REPORT_ID_BATTERY,  /*   REPORT_ID (0x13) */ 
    0x75, 0x08,                    /*   REPORT_SIZE (8) */ 
    0x95, 0x01,                    /*   REPORT_COUNT (1) */ 
    0x09, 0x3b,                    /*   USAGE (Battery Strength) */ 
    0x15, 0x00,                    /*   LOGICAL_MINIMUM (0) */ 
    0x25, 0x64,                    /*   LOGICAL_MAXIMUM (100) */ 
    0x81, 0x02,                    /*   INPUT (Data,Var,Abs) */ 
    0xc0,                          /* END_COLLECTION  */  
    
    0x05, 0x06,                    /* USAGE_PAGE (Generic device) */ 
    0x09, 0x3b,                    /*   USAGE (Battery Strength) */ 
    0xa1, 0x01,                    /*   COLLECTION (Application) */ 
    0x85, HOGP_REPORT_ID_SCAN_REF, /*   REPORT_ID (0x14) */ 
    0x75, 0x08,                    /*   REPORT_SIZE (8) */ 
    0x95, 0x01,                    /*   REPORT_COUNT (1) */ 
    0x09, 0x3b,                    /*   USAGE (Battery Strength) */ 
    0x15, 0x00,                    /*   LOGICAL_MINIMUM (0) */ 
    0x25, 0x64,                    /*   LOGICAL_MAXIMUM (100) */ 
    0x81, 0x02,                    /*   INPUT (Data,Var,Abs) */ 
    0xc0,                          /* END_COLLECTION  */

};


/* Primary service - HID Service 
 ********** Start **************
 */
static tgatt_uuid16_service_record HIDS_SrvcRecord =
{ /* SERVICE DEFINITION */
  NULL,                                                                  ///< Link pointer for all_service_q 
  {&BAS_SrvcINCL, GATT_PRIM_SERVICE_RECORD_UUID16, GATT_START_HANDLE},   ///< record header.next_ptr is set to address of next record of the service 
  19,                                                                    ///< Service End handle, set this value to total number of attributes associated with this service excluding service record itself 
  UUID_DECL_PRIMARY_SERVICE,                                             ///< 0x2800 for «Primary Service» 
  hidd_gatts_callback,                                                   ///< Service Callback
  UUID_SERVICE_HUMAN_INTERFACE_DEVICE                                    ///< 0x1812 - for Bluetooth HID service 
};

static tgatt_uuid16_include_decl_record BAS_SrvcINCL =
{ /* INCLUDE Battery SERVICE */
  {&SCPS_SrvcINCL, GATT_INCLUDE_DECL_RECORD_INT_UUID16, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_INCLUDE,                ///< 0x2802 for «Include Service» 
  0,                                ///< Start Handle 
  0,                                ///< End Handle 
  UUID_SERVICE_BATTERY              ///< 0x180F - for Bluetooth Battery service 
};
 
 static tgatt_uuid16_include_decl_record SCPS_SrvcINCL =
 { /* INCLUDE SCAN PARAMETERS SERVICE */
   {&CharDecl_PrtlMode, GATT_INCLUDE_DECL_RECORD_INT_UUID16, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
   UUID_DECL_INCLUDE,                ///< 0x2802 for «Include Service» 
   0,                                ///< Start Handle 
   0,                                ///< End Handle 
   UUID_SERVICE_SCAN_PARAMETERS      ///< 0x1813 - for Bluetooth SCAN Parameter service 
 };

 /* PROTOCOL MODE CHARACTERISTIC */
static tgatt_uuid16_char_decl_record CharDecl_PrtlMode =
{  /* Characteristics Declaration */
  {&CharVal_PrtlModeVal, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                         ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ | GATT_CH_PROP_WRITE_NORESP,    ///< Read & Write Without Response Permitted
  GATT_NEXT_HANDLE,                                 ///< Set to Next attribute record
  GATT_UUID_HID_PROTO_MODE                          ///< UUID for Protocol Mode 
};
 
static tHOGP_PRTLMode_CharValDecl CharVal_PrtlModeVal =
{ /* NOTE**: Need to Initialize all the Client Instances; Currently MAX_CONN_PER_DEV=1, hence initialized Default PRTL Mode value once to Zero*/ 
/* Characteristics Value Declaration */
  {{&CharDecl_MouseIN/*&CharDecl_KBIN*/, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_PROTO_MODE,                                                    ///< UUID for Protocol Mode 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  {0x1} ///< NOTE: Defult Protocol Mode is REPORT PROTOCOL MODE
};

/* REPORT CHARACTERISTICS */
#if 0
    /*KEYBOARD CHAR - REPORT TYPE INPUT*/
static tgatt_uuid16_char_decl_record CharDecl_KBIN =
{  /* Characteristics Declaration */
  {&CharVal_KBIN, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},      ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                             ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_WRITE|GATT_CH_PROP_NOTIFY,             ///< Read, Write & Notify Permitted  
  GATT_NEXT_HANDLE,                                                     ///< Set to Nextl attribute record
  GATT_UUID_HID_REPORT                                                  ///< UUID for HID Report 
};
 
static tgatt_uuid16_charval_decl_record_std CharVal_KBIN =
{  /* Characteristics Value Declaration */
  {&KBIN_CCC, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},   ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_REPORT,                                                 ///< UUID for HID Report 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_NEED_AUTHORIZATION_FLAG
};
//  0 /* Defult Value */
//};
 
static tHOGP_KBIN_CCC KBIN_CCC =
{ /* Need to Initialize all the Client Configuration Instances; Currently MAX_CONN_PER_DEV=1, hence initialized CCC value once to Zero*/
  /* Characteristic Descriptor Declaration */
  {
  {&KBIN_RPT_REF, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION,                    ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_NEED_AUTHORIZATION_FLAG ,
  },
  {0x00}    ///< defualt value 
};
 
static tHOGP_KBIN_RptRef KBIN_RPT_REF =
{ /* Characteristic Descriptor Declaration */
  {{&CharDecl_KBOUT, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE}, ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_REPORT_REFERENCE,                                       ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC,
  },  
  KBIN_RPT_REF_VAL        ///< defualt value: Byte0 - Report ID, Byte1 - Report Type
};

    /*KEYBOARD CHAR - REPORT TYPE OUTPUT*/
static tgatt_uuid16_char_decl_record CharDecl_KBOUT =
{  /* Characteristics Declaration */
  {&CharVal_KBOUT, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                            ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_WRITE|GATT_CH_PROP_WRITE_NORESP,      ///< Read, Write/Wirte without Resp permitted  
  GATT_NEXT_HANDLE,                                                    ///< Set to Next attribute record
  GATT_UUID_HID_REPORT                                                 ///< UUID for HID Report 
};
 
static tHOGP_KBOut CharVal_KBOUT =
{  /* Characteristics Value Declaration */
  {{&KBOUT_RPT_REF, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_REPORT,                                                       ///< UUID for HID Report 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC|GATT_PERM_WRITE_AUTH_SECURITY_LEVEL_WITH_ENC},
  0x00
};
 
static tHOGP_KBOUT_RptRef KBOUT_RPT_REF =
{  /* Characteristic Descriptor Declaration */
  {{&CharDecl_MouseIN, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_REPORT_REFERENCE,                                            ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  KBOUT_RPT_REF_VAL            ///< defualt value: Byte0 - Report ID, Byte1 - Report Type
};

#endif //Keyboard features commented

    /*MOUSE REPORT CHAR - REPORT TYPE INPUT*/
static tgatt_uuid16_char_decl_record CharDecl_MouseIN =
{  /* Characteristics Declaration */
  {&CharVal_MouseIN, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                    ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_NOTIFY,                       ///< Read & Notify Permitted 
  GATT_NEXT_HANDLE,                                            ///< Set to Next attribute record
  GATT_UUID_HID_REPORT                                         ///< UUID for HID Report 
};
 
static tgatt_uuid16_charval_decl_record_std CharVal_MouseIN =
{  /* Characteristics Value Declaration */
  {&MouseIN_CCC, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_REPORT,                                                     ///< UUID for HID Report 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};
//  0 /* Defult Value */
//};
 
static tHOGP_MouseIN_CCC MouseIN_CCC =
{/* Need to Initialize all the Client Configuration Instances; Currently MAX_CONN_PER_DEV=1, hence initialized CCC value once to Zero*/
  /* Characteristic Descriptor Declaration */
  {{&MouseIN_RPT_REF, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION,                            ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC,
  },
  {0x00}                 ///< defualt value 
};
 
static tHOGP_MouseIN_RptRef MouseIN_RPT_REF =
{  /* Characteristic Descriptor Declaration */
  {{&CharDecl_ReportMap, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_REPORT_REFERENCE,                                              ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  MouseIN_RPT_REF_VAL    ///< defualt value: Byte0 - Report ID, Byte1 - Report Type
};
    
    /* REPORT MAP CHARACTERISTIC */
static tgatt_uuid16_char_decl_record CharDecl_ReportMap =
{  /* Characteristics Declaration */
  {&CharVal_ReportMap, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                  ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                                         ///< Read Permitted
  GATT_NEXT_HANDLE,                                          ///< Set to Next attribute record
  GATT_UUID_HID_REPORT_MAP                                   ///< UUID for Report Map 
};
 
static tgatt_uuid16_charval_decl_record_std CharVal_ReportMap =
{  /* Characteristics Value Declaration */
  {&CharDescERR_ReportMap_BAL, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_REPORT_MAP,                                                               ///< UUID for Report Map
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};

static tHOGP_ReportMap_ERR CharDescERR_ReportMap_BAL =
{  /* Characteristics Descriptor Declaration */
  {{&CharDescERR_ReportMap_ScRef, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_EXTERNAL_REPORT_REFERENCE,                                                  ///< UUID for Battery Level 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC},
  GATT_UUID_BATTERY_LEVEL 
};

static tHOGP_ReportMap_ERR CharDescERR_ReportMap_ScRef =
{  /* Characteristics Descriptor Declaration */
  {{&CharDecl_BT_KBIN, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_EXTERNAL_REPORT_REFERENCE,                                   ///< UUID for Report Reference 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC},
  GATT_UUID_SCAN_REFRESH
};
#if 0
    /* BOOT KEYBOARD CHAR - REPORT TYPE INPUT*/
static tgatt_uuid16_char_decl_record CharDecl_BT_KBIN =
{  /* Characteristics Declaration */
  {&CharVal_BT_KBIN, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},           ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_NOTIFY,                   ///< Read & Notify permitted 
  GATT_NEXT_HANDLE,                                        ///< Set to Next attribute record
  GATT_UUID_HID_BT_KB_INPUT                                ///< UUID for Keyboard Input 
};
 
static tgatt_uuid16_charval_decl_record_std CharVal_BT_KBIN =
{  /* Characteristics Value Declaration */
  {&BT_KBIN_CCC, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_BT_KB_INPUT,                                                ///< UUID for KB Input 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};
//  0 /* Defult Value */
//};
 
static tHOGP_BT_KBIN_CCC BT_KBIN_CCC =
{/* Need to Initialize all the Client Configuration Instances; Currently MAX_CONN_PER_DEV=1, hence initialized CCC value once to Zero*/
  /* Characteristic Descriptor Declaration */
  {{&CharDecl_BT_KBOUT, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION,                              ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  {0x00}        ///< defualt value 
};

    /* BOOT KEYBOARD CHAR - REPORT TYPE OUTPUT*/
static tgatt_uuid16_char_decl_record CharDecl_BT_KBOUT =
{  /* Characteristics Declaration */
  {&CharVal_BT_KBOUT, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},           ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                              ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_WRITE|GATT_CH_PROP_WRITE_NORESP,        ///< Read, Write/Write without Response permitted  
  GATT_NEXT_HANDLE,                                                      ///< It will be set to Next attribute record
  GATT_UUID_HID_BT_KB_OUTPUT                                             ///< UUID for KB output
};
 
static tHOGP_KBOut CharVal_BT_KBOUT =
{  /* Characteristics Value Declaration */
  {{&CharDecl_BT_MouseIN, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_BT_KB_OUTPUT,                                                        ///< UUID for KB Output 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC|GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC|GATT_PERM_ENC_KEY_SIZE_16BYTES },
  0x00
};
#endif //Keyboard features commented

#if 0
    /* BOOT MOUSE REPORT CHAR - REPORT TYPE INPUT*/
static tgatt_uuid16_char_decl_record CharDecl_BT_MouseIN =
{  /* Characteristics Declaration */
  {&CharVal_BT_MouseIN, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                  ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_NOTIFY,                     ///< Read & Notify Permitted 
  GATT_NEXT_HANDLE,                                          ///< It will be set to Next attribute record
  GATT_UUID_HID_BT_MOUSE_INPUT                               ///< UUID for Mouse Input 
};

#endif
 
static tgatt_uuid16_charval_decl_record_std CharVal_BT_MouseIN =
{  /* Characteristics Value Declaration */
  {&BT_MouseIN_CCC, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_BT_MOUSE_INPUT,                                                ///< UUID for Mouse Input 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};
//  0 /* Defult Value */
//};
 
static tHOGP_BT_MouseIN_CCC BT_MouseIN_CCC =
{/* Need to Initialize all the Client Configuration Instances; Currently MAX_CONN_PER_DEV=1, hence initialized CCC value once to Zero*/
  /* Characteristic Descriptor Declaration */
  {{&CharDecl_HIDInfo, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION,                            ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  {0x00}                 ///< defualt value 
};

    /* HID INFORMATION CHAR */
static tgatt_uuid16_char_decl_record CharDecl_HIDInfo =
{  /* Characteristics Declaration */
  {&CharVal_HIDInfo, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                  ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                                         ///< Read Permitted
  GATT_NEXT_HANDLE,                                          ///< It will be set to Next attribute record
  GATT_UUID_HID_INFORMATION                                  ///< UUID for HID Information 
};
     
static tgatt_uuid16_charval_decl_record_std CharVal_HIDInfo =
{  /* Characteristics Value Declaration */
  {&CharDecl_HID_CtrlPt, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_INFORMATION,                                                    ///< UUID for HID Information
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};

    /* HID CONTROL POINT CHAR */
static tgatt_uuid16_char_decl_record CharDecl_HID_CtrlPt =
{  /* Characteristics Declaration */
  {&CharVal_CtrlPt, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                  ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_WRITE_NORESP,                                 ///< Wirte without Response permitted 
  GATT_NEXT_HANDLE,                                          ///< It will be set to Next attribute record
  GATT_UUID_HID_CONTROL_POINT                                ///< UUID for HID Control Point 
};
   
static tHOGP_CtrlPoint CharVal_CtrlPt =
{  /* Characteristics Value Declaration */
  {{NULL, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_CONTROL_POINT,                                      ///< UUID for HID Control Point 
  GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC},
  0x0
};

#if 0  
static tHOGP_CtrlPoint CharVal_CtrlPt =
{  /* Characteristics Value Declaration */
  {{&CharDecl_KB_Feature1, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_CONTROL_POINT,                                      ///< UUID for HID Control Point 
  GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC},
  0x0
};
    /*KEYBOARD Feature1 - REPORT TYPE FEATURE */
static tgatt_uuid16_char_decl_record CharDecl_KB_Feature1 =
{  /* Characteristics Declaration */
  {&CharVal_KB_Feature1, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},      ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                                             ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_WRITE,                                ///< Read & Write Permitted  
  GATT_NEXT_HANDLE,                                                     ///< Set to Nextl attribute record
  GATT_UUID_HID_REPORT                                                  ///< UUID for HID Report 
};
 
static tHOGP_KB_FEAT1_Val CharVal_KB_Feature1 =
{  /* Characteristics Value Declaration */
  {{&KB_Feature1_RPT_REF, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},   ///< record header.next_ptr is set to address of next record of the service 
  GATT_UUID_HID_REPORT,                                                 ///< UUID for HID Report 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC|GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC},
  0x00,
};

static tHOGP_KBIN_RptRef KB_Feature1_RPT_REF =
{ /* Characteristic Descriptor Declaration */
  {{NULL, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE}, ///< record header.next_ptr is set to address of next record of the service 
  UUID_DESCPTR_REPORT_REFERENCE,                                       ///< UUID for CCC 
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC,
  },  
  KB_Feature1_RPT_REF_VAL        ///< defualt value: Byte0 - Report ID, Byte1 - Report Type
};
#endif

/* Primary service - HID Service 
********** End **************
*/


/*!
 * @brief  Service Request Handler Registered with Gatt Server
 *            This Callback funtion handles all the Gatt Server Requests & Events
 *
 * @param conn_id Gatt Server Connection Id
 * @param req_type Gatt Server's request
 * @param params Request/Event Parameters
 *
 * @return Void Returns None
 */
tgatts_cb_result hidd_gatts_callback(tconn_id connid, tgatt_event_ID req_type , tgatts_event_param *params)
{
    uint8 need_resp = HOGP_SRVC_ACT_IGNORE;
    tgatts_cb_result result;
    result.status = ATT_SUCCESS;

    #if (defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE)
    HID_DEBUG("hidd_gatts_callback: Req_Type=%d, connID=%p",req_type,connid);
    #endif
 
    if ((connid == NULL  && req_type != GATT_EVENT_SERVICE_ADDED) || params == NULL)
        return result;

    switch(req_type)
    {
        case GATT_EVENT_CONNECTED:
        case GATT_EVENT_DISCONNECTED:
        {
            thidd_state_event evt;
            tgatt_conn_disc_param *p_connDisconParam = (tgatt_conn_disc_param *)params;
            uint8 ii=0; 
            
            evt.event_id  = ((req_type == GATT_EVENT_CONNECTED)? HIDD_INT_OPEN_EVT : HIDD_INT_CLOSE_EVT);
            evt.pdata      = connid;
            evt.data_len  = sizeof(void *);            

            if(req_type == GATT_EVENT_CONNECTED)
            {    // ToDo: if Device Initiated conn is supported, then check for the stored DEVID during connReq and Send CONNECTED Evt to particular app. Currently sending CONN EVT to all the registered App

            #if (defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE)
                HID_DEBUG("hidd_gatts_callback: GATT EVENT CONNECTED, Reason=%d",p_connDisconParam->reason);
            #endif
                if(!p_connDisconParam->reason)
                {
                    for(ii=0; ii<MAX_APP_ID; ii++)
                    {
                        if(hidd_ctrlblk.p_cback[ii] != NULL && hidd_ctrlblk.connContext[ii].cur_state == HIDD_IDLE_ST)
                        {    
                            hidd_ctrlblk.connContext[ii].devid = p_connDisconParam->device_id;
                            hidd_ctrlblk.connContext[ii].connID = connid;
                            
                            evt.appid = ii;
                            state_event_handler(&evt);
                        }
                    }
                }
            
            }
            else
            {    //ToDo: Check for all the APPIDs based on disconnected evt DevID & disconnect all matching DevID connections. Currently sending DISCONNECTED Evt to all the Connected Apps
		#if (defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE)
                HID_DEBUG("hidd_gatts_callback: GATT EVENT DISCONNECTED, Reason=%d",p_connDisconParam->reason);
                #endif

//                uint16 result;
//                tHIDS_nvm_data nvm_data;
                uint8 cccid = fetch_cccIdx(connid);
#if 0
                /* Store  the NV data into persistent storage, if this device id is in Paired device List */
                nvm_data.valid_data = TRUE;
                nvm_data.PrtlMode_Val = CharVal_PrtlModeVal.PrtlMode_Val[cccid];
                nvm_data.kbin_nty_Ind_ccc = KBIN_CCC.nty_Ind[cccid];
                nvm_data.mouse_nty_Ind_ccc = MouseIN_CCC.nty_Ind[cccid];
                nvm_data.bt_kbin_nty_Ind_ccc = BT_KBIN_CCC.nty_Ind[cccid];
                nvm_data.bt_mouse_nty_Ind_ccc = BT_MouseIN_CCC.nty_Ind[cccid];
                
                /* Store  the NV data into persistent storage, if this device id is in Paired device List */
                if((result = NVM_mgt_write(p_connDisconParam->device_id, HIDS_SIGN, sizeof(tHIDS_nvm_data), (void *)&nvm_data)) < 0)
                {
                    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
                    HID_DEBUG("hidd_gatts_callback: Client Configuration Context NOT SAVED!!, Reason=%d",result);
                    #endif
                }
                
                #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
                HID_DEBUG("Loaded NV data is :\nValidData = %d\nPrtlMode = %d\nKBIN_CCC = %d\nMIN_CCC = %d\nBT_KBIN = %d\nBT_MIN = %d\n",
                        nvm_data.valid_data,
                        CharVal_PrtlModeVal.PrtlMode_Val[cccid],
                        KBIN_CCC.nty_Ind[cccid],
                        MouseIN_CCC.nty_Ind[cccid],
                        BT_KBIN_CCC.nty_Ind[cccid],
                        BT_MouseIN_CCC.nty_Ind[cccid]);
                #endif                
#endif
                /* Delete Previous CCC Context & Set the default values */
                CharVal_PrtlModeVal.PrtlMode_Val[cccid] = 0x1;
                KBIN_CCC.nty_Ind[cccid]                 = 0;
                MouseIN_CCC.nty_Ind[cccid]              = 0;
                BT_KBIN_CCC.nty_Ind[cccid]              = 0;
                BT_MouseIN_CCC.nty_Ind[cccid]           = 0;                

                for(ii=0; ii<MAX_APP_ID; ii++)
                {            
                    if(hidd_ctrlblk.connContext[ii].connID == connid)
                    {
                        oss_memset(&(hidd_ctrlblk.connContext[ii].devid),0,sizeof(tbt_device_id));
                    }
                    evt.appid = ii;
                    state_event_handler(&evt);                    
                }
            }
        }
        break;
        case GATT_EVENT_TRANSACTION_TIMEOUT:
            HID_DEBUG(" Transaction has timed out");
            break;
        case GATT_EVENT_MTU_INFO:
           HID_DEBUG("Agreed MTU : %d \n", ((tgatt_mtu_info_param *)params)->mtu);
           break;
        case GATT_EVENT_READ_REQ:
        {
            tgatts_req *req_data = (tgatts_req *)params;
            tgatts_rsp read_rsp;
            
            HID_DEBUG("req_data->need_authorization=%d",req_data->need_authorization);
            if(req_data->need_authorization && (!isAuthorized()))
            {
               result.status = ATT_INSUFF_AUTHOR;
               HID_DEBUG("status=%d",result.status);
            }
            
               HID_DEBUG("status=%d",result.status);
            attr_val.p_rptMap = NULL;
            attr_val.len = 0;
            oss_memset(attr_val.value,0,MAX_ATT_VALUE); //memset attr_val

            read_rsp.rsp_of = GATT_EVENT_READ_REQ ;
            read_rsp.handle = req_data->handle;
            
            if(result.status != ATT_INSUFF_AUTHOR)
            {
               hidd_read_attr_value(connid, req_data, &attr_val, &result.status);
               read_rsp.p_val    = ((attr_val.p_rptMap == NULL)? attr_val.value : attr_val.p_rptMap); //The attr_val.p_rptMap will be NULL, if req handle is not for REPORT MAP
               read_rsp.val_len= attr_val.len;
			   HID_DEBUG("val_len = %d",read_rsp.val_len);
            }
            else
            {
               read_rsp.val_len = 0;
				HID_DEBUG("val_len = 0");
            }

            GATTS_Send_Rsp(connid, result.status, (void *)&read_rsp); 
        }
        break;

        case GATT_EVENT_WRITE_REQ:
            need_resp = HOGP_SRVC_ACT_RSP;
        case GATT_EVENT_WRITE_CMD:
        {
            tgatts_req *req_data = (tgatts_req *)params;			
            tgatts_write_rsp write_rsp;
            
            write_rsp.rsp_of = ((need_resp)? GATT_EVENT_WRITE_REQ : GATT_EVENT_WRITE_CMD) ;
            write_rsp.handle = req_data->handle;

            if(req_data->need_authorization && (!isAuthorized()))
            {
               result.status = ATT_INSUFF_AUTHOR;
            }
            if(result.status != ATT_INSUFF_AUTHOR)
               hidd_write_attr_value(connid, req_data, &result.status);
            if(need_resp)
                GATTS_Send_Rsp(connid, result.status, (void *)&write_rsp); 
       	}
        break;        

        case GATT_EVENT_IND_CONF:
        {
            uint8 ccc_idx = fetch_cccIdx(connid);
            indicate_ack[ccc_idx] = TRUE;
        }
        break;

        case GATT_EVENT_SERVICE_ADDED:
        {
          uint8 ii=0;
          thidd_state_event evt;
          
          for(ii=0; hidd_ctrlblk.p_cback[ii] == NULL; ii++); // Get the APPID for the first application that is registering with hogp
          if(ii < MAX_APP_ID)
          {
              evt.appid     = ii;
              evt.event_id  = HIDD_API_REGISTER_APP_EVT; //For the First APP reg, Need to send APP_REGISTERED_EVT to APP & Transit to Next state.
              evt.pdata     = NULL;
              evt.data_len  = 0;
                  
              state_event_handler(&evt);
          }
          #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
          else
              HID_DEBUG("None of the APP has Registred Cback - FAILED!! Appid=%d > MaxAppid=%d",ii,MAX_APP_ID);
          #endif	
        }
        break;

        default:
        break;            
    }

    return result;
}


/*!
 * @brief  HOGP Register Service Action Function
 *            Action Function to Add HOGP Gatt Services & Register to Gatt Server.
 *
 * @param Void Takes No Parameters
 *
 * @return Void Returns None
 */
void hogp_reg_service_act(void)
{
//    uint8 ii=0;
//    thidd_state_event evt;

    /* Create HIDS Service */
    GATTS_Add_Service((void *) &HIDS_SrvcRecord);        

    /* Update Adv Data & Start Advertising */
    /* Set Advertisement Data */
    //Start_Advertise(FALSE);    
}


/*!
 * @brief  Function API to Update Included Services Handle Range
 *            This funtion is to update included services handle range, after initializing Included 
 *            services and App registration with HOGP Device profile(Hidd_app_reg_api)
 *
 * @param ext_service_id External service Id to be included as declared in hidd_le.h file
 * @param hndl_range External Service Handle Range
 *
 * @return Void Returns None
 */
void hogp_service_include(tHogp_external_service_id ext_service_id, thandle_range hndl_range)
{
    tgatt_uuid16_include_decl_record *p_include_decl_record = NULL;   
    switch(ext_service_id)   
        {      
        case INCL_SERVICE_BAS_ID:        
            p_include_decl_record = &BAS_SrvcINCL;        
            break;              
        case INCL_SERVICE_SCPS_ID:        
            p_include_decl_record = &SCPS_SrvcINCL;        
            break;   
        }   
    p_include_decl_record->attr_val_incl_s_hndl = hndl_range.s_handle;   
    p_include_decl_record->attr_val_incl_e_hndl = hndl_range.e_handle;
}


/*!
 * @brief  Function to Register Application Callback
 *            Action Function to Register Application Callback.
 *
 * @param p_appCback Pointer to Application Callback function
 *
 * @return appID Returns Application Id, which is mapping index of Application Callback Array
 */
appID hogp_reg_app_cb(tHIDD_CBACK *p_appCback)
{
    uint8 ii=0;

    for(ii=0; ii < MAX_APP_ID; ii++)
        if(hidd_ctrlblk.p_cback[ii] == NULL)
        {
            hidd_ctrlblk.p_cback[ii] = p_appCback;
            break;
        }

    if(ii >= MAX_APP_ID)
    {
        #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("APP Registration FAILED!! - Exceeded MAX APP REG, MAX_APP_REGISTERED=%d",ii);
        #endif
        return APP_ID_ERR;
    }
    return ii;
}


/*!
 * @brief  Action Function that sends REGISTERED EVENT to Register App
 *
 * @param appID Application Id, which is mapping index of Application Callback Array
 *
 * @return Void Returns None
 */
void hogp_reg_app_act(appID appid)
{
    tHIDD_STATUS status = HIDD_SUCCESS;

    //Send Registered event status to Application.
    hidd_ctrlblk.p_cback[appid](HIDD_INT_APP_REGISTERED_EVT, ((tHIDD_CBACK_EVT *)(&status)));
}


/*!
 * @brief  Action Function to DeRegister Application
 *
 * @param p_appid Pointer to Application Id, which is mapping index of Application Callback Array
 *
 * @return Void Returns None
 */
void hogp_dereg_app_act(appID *p_appid)
{
    uint8 ii=0;
    tHIDD_STATUS status = HIDD_SUCCESS;    

    for(ii=0; ii != *p_appid && ii < MAX_APP_ID; ii++);

    if(ii >= MAX_APP_ID)
    {
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("APP DeRegistration FAILED!! - Invalid AppID, AppID=%d",*p_appid);
    #endif
        //Send DeRegistered event with status fail/Not allowed to Application.
        status = HIDD_ERR_INVALID_PARAM;
        hidd_ctrlblk.p_cback[*p_appid](HIDD_INT_APP_UNREGISTERED_EVT, ((tHIDD_CBACK_EVT *)(&status)));
        return;
    }
    
    if(hidd_ctrlblk.p_cback[ii] != NULL)
    {
        hidd_ctrlblk.p_cback[ii] = NULL;
    }
    
    //Send status SUCCESS to Application
    hidd_ctrlblk.p_cback[*p_appid](HIDD_INT_APP_UNREGISTERED_EVT, ((tHIDD_CBACK_EVT *)(&status)));
}

#if 0 //Currently Not being used
/*******************************************************************************
**
** Function      hogp_conn_act
**
** Description   Action Function to Initiate ReConnection/Start Directed Advertising.
**
** Parameters:   Takes tConn_params * as input argument and Return Void.
**
*******************************************************************************/
void hogp_conn_act(tConn_params *connParam)
{
/*ToDo: If Device Initiated connection is supported, then check for BDADDR if connection with that dev is already present trigger open_evt
else create connection with that device and save the DEVID. Currently, supporting only Re-Connection by doing Directed Adv & followed by Undirected Adv*/
    
}
#endif

/*!
 * @brief  Action Function to Disconnect Connection
 *
 * @param disConnParam Pointer to Disconnection Parameters
 *
 * @return Void Returns None
 */
void hogp_disconn_act(tDisConn_params *disConnParam)
{
  tbt_device_id disconn_devid = hidd_ctrlblk.connContext[disConnParam->appid].devid;
  GATT_Disconnect(&disconn_devid, 0);
}


/*!
 * @brief  Action Function that handles Incoming Connection event & Sends Connection open EVT to APP
 *
 * @param conn_id Gatt Server Connection Id
 * @param appID Application Id, which is mapping index of Application Callback Array
 *
 * @return Void Returns None
 */
void hogp_open_act(tconn_id connID, appID appid)
{
    tHIDS_nvm_data nvm_data;
    tHIDD_ConnInfo conninfo;
    uint8 cccid;

    conninfo.status = HIDD_SUCCESS;
    conninfo.connID = connID;
    conninfo.dev_id = hidd_ctrlblk.connContext[appid].devid;

       /* Load the NV data if available for this device id */
//    if(( NVM_mgt_read(hidd_ctrlblk.connContext[appid].devid, HIDS_SIGN, sizeof(tHIDS_nvm_data), (void *)&nvm_data) ) < 0)
        nvm_data.valid_data = FALSE;    

    //for(ii=0; ii<MAX_APP_ID; ii++)
        if(hidd_ctrlblk.p_cback[appid])
        {
            hidd_ctrlblk.connID_cccIndex_Map[appid] = connID; // This is to check for the ccc Index. If multiple connections per app is not supported then the connContext can be used directly. This field is not needed.

            cccid = fetch_cccIdx(connID);
            /* Load the NV data if available for this device id */
            if(nvm_data.valid_data == TRUE)
            {
              CharVal_PrtlModeVal.PrtlMode_Val[cccid] = nvm_data.PrtlMode_Val;
              KBIN_CCC.nty_Ind[cccid]                 = nvm_data.kbin_nty_Ind_ccc;
              MouseIN_CCC.nty_Ind[cccid]              = nvm_data.mouse_nty_Ind_ccc;
              BT_KBIN_CCC.nty_Ind[cccid]              = nvm_data.bt_kbin_nty_Ind_ccc;
              BT_MouseIN_CCC.nty_Ind[cccid]           = nvm_data.bt_mouse_nty_Ind_ccc;

              #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
              HID_DEBUG("Loaded NV data is :\nPrtlMode = %d\nKBIN_CCC = %d\nMIN_CCC = %d\nBT_KBIN = %d\nBT_MIN = %d\n",
                        CharVal_PrtlModeVal.PrtlMode_Val[cccid],
                        KBIN_CCC.nty_Ind[cccid],
                        MouseIN_CCC.nty_Ind[cccid],
                        BT_KBIN_CCC.nty_Ind[cccid],
                        BT_MouseIN_CCC.nty_Ind[cccid]);
              #endif
            }
            
            //Send Connected Event to  Registered Applications.
            hidd_ctrlblk.p_cback[appid](HIDD_INT_APP_OPEN_EVT, ((tHIDD_CBACK_EVT *)(&conninfo)));
        }
}


/*!
 * @brief  Action Function that handles Incoming Disconnection event from remote device
 *            & Sends Connection close EVT to APP
 *
 * @param conn_id Gatt Server Connection Id
 *
 * @return Void Returns None
 */
void hogp_close_act(tconn_id connID)
{
    uint8 ii=0;
    tHIDD_STATUS status = HIDD_SUCCESS;
    appID appid = fetch_appId(connID);

    for(ii=0; ii<MAX_APP_ID; ii++)//Currently MAX_APP_ID and MAXCONN_PER_DEV are SAME, hence check is handled in single loop, else need to check in different loops.
        if(hidd_ctrlblk.connContext[ii].connID == connID || hidd_ctrlblk.connID_cccIndex_Map[ii] == connID)
        {
            hidd_ctrlblk.connContext[ii].connID = NULL; //
            hidd_ctrlblk.connID_cccIndex_Map[ii] = NULL; // This is to check for the ccc Index. If multiple connections per app is not supported then the connContext can be used directly. This field is not needed.
            //Send Disconnected Event to all the Registered Applications.
            hidd_ctrlblk.p_cback[appid](HIDD_INT_APP_CLOSE_EVT, ((tHIDD_CBACK_EVT *)(&status)));
        }    
}


/*!
 * @brief  Action Function to Send Input Report to Remote Gatt Client.
 *
 * @param sendReport_params Pointer to Report Data parameters
 *
 * @return Void Returns None
 */
void hogp_send_report_act(tSendReport_params* sendReport_params)
{
    uint8 ccc_idx = fetch_cccIdx(sendReport_params->connID);

    boolean indicate = FALSE, resp = HOGP_SRVC_ACT_RSP;
    static uint8 prev_rptId = 0, prev_rptType = 0, prev_prtlMode = 0;
    static uint16 attr_id = 0;

    prev_prtlMode = CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx];
    
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
    HID_DEBUG("Send HOGP Report, connID=%p, ReportID=%d, ReportType=%d, attr_id=%d",sendReport_params->connID,sendReport_params->rpt_ID,sendReport_params->rpt_type,attr_id);
    #endif        

    if(prev_prtlMode != CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx] || prev_rptId != sendReport_params->rpt_ID
        || prev_rptType != sendReport_params->rpt_type)
    {
        attr_id = get_attr_id(sendReport_params->connID, sendReport_params->rpt_ID, sendReport_params->rpt_type);
        prev_prtlMode = CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx];
        prev_rptId = sendReport_params->rpt_ID;
        prev_rptType = sendReport_params->rpt_type;

        #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("Change in Attr ID: Prtl_Mode=%d, ReportID=%d, ReportType=%d, attr_id=%d",CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx],sendReport_params->rpt_ID,sendReport_params->rpt_type,attr_id);
        #endif        
    }

    /* Check if Notification/Indication is enabled by Gatt Client */
    switch(CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx])
    {
        case HOGP_PROTOCOL_MODE_BOOT:
        {
            if(prev_rptId == HOGP_REPORT_ID_KB && prev_rptType == HOGP_REPORT_TYPE_INPUT)
            {
                if(BT_KBIN_CCC.nty_Ind[ccc_idx] & TO_INDICATE)
                    indicate = TRUE;
                else if(!(BT_KBIN_CCC.nty_Ind[ccc_idx] & TO_NOTIFY))
                    resp = HOGP_SRVC_ACT_IGNORE;                    
            }
            else if(prev_rptId == HOGP_REPORT_ID_MOUSE && prev_rptType == HOGP_REPORT_TYPE_INPUT)
            {
                if(BT_MouseIN_CCC.nty_Ind[ccc_idx] & TO_INDICATE)
                    indicate = TRUE;
                else if(!(BT_MouseIN_CCC.nty_Ind[ccc_idx] & TO_NOTIFY))
                    resp = HOGP_SRVC_ACT_IGNORE;
            }
        }
        break;
    
        case HOGP_PROTOCOL_MODE_REPORT:
        {
            if(prev_rptId == HOGP_REPORT_ID_KB && prev_rptType == HOGP_REPORT_TYPE_INPUT)
            {
                if(KBIN_CCC.nty_Ind[ccc_idx] & TO_INDICATE)
                    indicate = TRUE;
                else if(!(KBIN_CCC.nty_Ind[ccc_idx] & TO_NOTIFY))
                    resp = HOGP_SRVC_ACT_IGNORE;                    
            }
            else if(prev_rptId == HOGP_REPORT_ID_MOUSE && prev_rptType == HOGP_REPORT_TYPE_INPUT)
            {
                if(MouseIN_CCC.nty_Ind[ccc_idx] & TO_INDICATE)
                    indicate = TRUE;
                else if(!(MouseIN_CCC.nty_Ind[ccc_idx] & TO_NOTIFY))
                    resp = HOGP_SRVC_ACT_IGNORE;                    
            }/*
            else if(prev_rptId == HOGP_REPORT_ID_BATTERY && prev_rptType == HOGP_REPORT_TYPE_INPUT)
            {
                if(BALCCC.nty_Ind[ccc_idx] & TO_INDICATE)
                    indicate = TRUE;
                else if(!(BALCCC.nty_Ind[ccc_idx] & TO_NOTIFY))
                    resp = HOGP_SRVC_ACT_IGNORE;                    
            }
            else if(prev_rptId == HOGP_REPORT_ID_SCAN_REF && prev_rptType == HOGP_REPORT_TYPE_INPUT)
            {
                if(ScRef_CCC.nty_Ind[ccc_idx] & TO_INDICATE)
                    indicate = TRUE;
                else if(!(ScRef_CCC.nty_Ind[ccc_idx] & TO_NOTIFY))
                    resp = HOGP_SRVC_ACT_IGNORE;                    
            }*/
        }
        break;

        default:
            #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
            HID_DEBUG("INCORRECT Protocol Mode VALUE: Prtl_Mode=%d",CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx]);
            #endif
            resp = HOGP_SRVC_ACT_IGNORE;
        break;
    }
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("resp = %u, attr_id = %u", resp, attr_id);
    #endif

    // TO-DO: Need to handle the INDICATIONS case - Next indication should be sent only after getting the handle confirmation for previously sent indication.
    if(attr_id != 0 && resp && indicate_ack[ccc_idx])
    {
        GATTS_Send_Handlevalue(sendReport_params->connID, attr_id, indicate, 
                                            sendReport_params->rpt_len, sendReport_params->p_data);
    }
    
    if(indicate)//&& bt_result //ToDo: Need to include this check to set the indicate_ack only if Gatt is sending this Indication
        indicate_ack[ccc_idx] = FALSE;
    
}


/*!
 * @brief  Internal Function to Read attribute values to respond to Read Req from GATT Client
 *
 * @param conn_id Gatt Server Connection Id
 * @param read_req Gatt Server Read Request data parameters
 * @param p_attr_value Pointer to attribute value that will be sent in response to read req
 * @param p_status Status of the read request
 *
 * @return Void Returns None
 */
static void hidd_read_attr_value(tconn_id connID, tgatts_req *read_req, tHOGP_AttrValue *p_attr_value, tgatt_status *p_status)
{
    tgatt_status st = ATT_ATTR_NOT_FOUND;
    uint8 ccc_idx = fetch_cccIdx(connID);
    uint8 *tmp_attr_val = p_attr_value->value;
    
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("read_req.handle = %u, offset = %u", read_req->handle, read_req->offset);
    #endif    
#if 0 //Currently Read Long is not beeing handled.
    if ((read_req->handle != CharVal_ReportMap->header.handle)&&
               read_req->is_long == TRUE)
    {
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("read_req.handle != ReportMapHdl && is_long = TRUE", read_req->handle, read_req->offset);
    #endif    
        st = ATT_ATTR_NOT_LONG;
        return;
    }
#endif

    if(read_req->handle == CharVal_PrtlModeVal.prtlMode_CharValDecl.header.handle)
       {
         p_attr_value->value[0] = CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx];
         p_attr_value->len = 0x01;
         st = ATT_SUCCESS;
       }
    else if(read_req->handle == CharVal_KBIN.header.handle) // Keyboard Input Report
    {
        p_attr_value->value[0] = 0x0;
        p_attr_value->len = 0x08;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == KBIN_CCC.kbin_ccc.header.handle)
    {
        UINT16_TO_LE_ENDIAN(KBIN_CCC.nty_Ind[ccc_idx],tmp_attr_val);//p_attr_value->value[1] - other bytes are already initialized to Zero
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == KBIN_RPT_REF.kbin_RptRef.header.handle)
    {
        UINT16_TO_LE_ENDIAN(KBIN_RPT_REF.RptRef_Val,tmp_attr_val);
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == CharVal_KBOUT.KBOut.header.handle) // Keyboard Output Report
    {
		p_attr_value->value[0] = CharVal_KBOUT.KBOut_Val;
        p_attr_value->len = 0x01;	
        st = ATT_SUCCESS;
    }
	else if(read_req->handle == CharVal_BT_KBOUT.KBOut.header.handle)
    {
		p_attr_value->value[0] = CharVal_BT_KBOUT.KBOut_Val;
        p_attr_value->len = 0x01;	
        st = ATT_SUCCESS;
    } 
    /* There is no CCC for Keyboard Output Report, So proceed further */
    else if(read_req->handle == KBOUT_RPT_REF.kbout_RptRef.header.handle)
    {
        UINT16_TO_LE_ENDIAN(KBOUT_RPT_REF.RptRef_Val,tmp_attr_val);
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == CharVal_MouseIN.header.handle) // Mouse Input Report
    {
        p_attr_value->value[0] = 0x0;
        p_attr_value->len = 0x04;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == MouseIN_CCC.mouse_ccc.header.handle)
    {
        UINT16_TO_LE_ENDIAN(MouseIN_CCC.nty_Ind[ccc_idx],tmp_attr_val);//p_attr_value->value[1] - other bytes are already initialized to Zero
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == MouseIN_RPT_REF.mouse_RptRef.header.handle)
    {
        UINT16_TO_LE_ENDIAN(MouseIN_RPT_REF.RptRef_Val,tmp_attr_val);
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == CharVal_ReportMap.header.handle)
    {
        if(reportDescriptor != NULL)
            p_attr_value->len = sizeof(reportDescriptor);
        else
            p_attr_value->len = 0;
        
           if (read_req->offset > p_attr_value->len)
        {
           st = ATT_INVALID_OFFSET;
        }
        else
        {
               p_attr_value->len -= read_req->offset;
            p_attr_value->p_rptMap = (uint8 *)(reportDescriptor + read_req->offset);
            
            st = ATT_SUCCESS;            
        }
        
        *p_status = st;
        return;        
    }
    //External Report Ref - Battery Level Characteristic
    else if(read_req->handle == CharDescERR_ReportMap_BAL.Err.header.handle)
    {
        UINT16_TO_LE_ENDIAN(CharDescERR_ReportMap_BAL.Err_Val,tmp_attr_val);
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }    
    //External Report Ref - Scan Refresh Characteristic
    else if(read_req->handle == CharDescERR_ReportMap_ScRef.Err.header.handle)
    {
        UINT16_TO_LE_ENDIAN(CharDescERR_ReportMap_ScRef.Err_Val,tmp_attr_val);
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == CharVal_BT_KBIN.header.handle)
    {
        p_attr_value->value[0] = 0x0;
        p_attr_value->len = 0x08;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == BT_KBIN_CCC.bt_kbin_ccc.header.handle)
    {
        UINT16_TO_LE_ENDIAN(BT_KBIN_CCC.nty_Ind[ccc_idx],tmp_attr_val);//p_attr_value->value[1] - other bytes are already initialized to Zero
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    
    else if(read_req->handle == CharVal_BT_MouseIN.header.handle)
    {
        p_attr_value->value[0] = 0x0;
        p_attr_value->len = 0x04;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == BT_MouseIN_CCC.bt_mouse_ccc.header.handle)
    {
        UINT16_TO_LE_ENDIAN(BT_MouseIN_CCC.nty_Ind[ccc_idx],tmp_attr_val);//p_attr_value->value[1] - other bytes are already initialized to Zero
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == CharVal_HIDInfo.header.handle)
    {
        p_attr_value->value[0] = 0x11;        // Lower Byte - Version of USB HID Spec = 0x0111
        p_attr_value->value[1] = 0x01;        // Upper Byte - Version of USB HID Spec = 0x0111
        p_attr_value->value[2] = 0x00;        // HW Localization; For Not Localized = 0x00
        p_attr_value->value[3] = 0x02;        // BIT 0 - Remote Wakeup; BIT 1 - Normally Connectable
        p_attr_value->len = 0x04;

        st = ATT_SUCCESS;
    }
    else if(read_req->handle == CharVal_CtrlPt.CtrlPt.header.handle)
    {   st = ATT_READ_NOT_PERMIT;}       /*HID Control Point - Read Not Permitted*/
    else if(read_req->handle == CharVal_KB_Feature1.kb_feature1.header.handle) // Keyboard Input Report
    {/* Keyboard Feature Report */
        UINT8_TO_LE_ENDIAN(CharVal_KB_Feature1.kb_feature1_val,tmp_attr_val);
        p_attr_value->len = 0x01;
        st = ATT_SUCCESS;
    }
    else if(read_req->handle == KB_Feature1_RPT_REF.kbin_RptRef.header.handle)
    {
        UINT16_TO_LE_ENDIAN(KB_Feature1_RPT_REF.RptRef_Val,tmp_attr_val);
        p_attr_value->len = 0x02;
        st = ATT_SUCCESS;
    }	
    else
        st = ATT_INVALID_HANDLE;

    *p_status = st;
    return;

}


/*!
 * @brief  Internal Function to Save attribute values received in Write Req from GATT Client.
 *
 * @param conn_id Gatt Server Connection Id
 * @param write_req Gatt Server Write Request data parameters
 * @param p_status Status of the read request
 *
 * @return Void Returns None
 */
void hidd_write_attr_value(tconn_id connID, tgatts_req *write_req, tgatt_status *p_status)
{
    tgatt_status st = ATT_ATTR_NOT_FOUND;
    uint8 ccc_idx = fetch_cccIdx(connID);
    
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("write_req.handle = %u, offset = %u, Len = %u ", write_req->handle, write_req->offset, write_req->val_len);
		HID_DEBUG("feature handle = %u", CharVal_KB_Feature1.kb_feature1.header.handle);
    #endif
    
    if(write_req->handle == CharVal_PrtlModeVal.prtlMode_CharValDecl.header.handle)
        {
         CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx] = write_req->p_value[0];
         st = ATT_SUCCESS;
        }
    else if(write_req->handle == KBIN_CCC.kbin_ccc.header.handle)
        {
        KBIN_CCC.nty_Ind[ccc_idx] = write_req->p_value[0];
        st = ATT_SUCCESS;
        }
    else if(write_req->handle == CharVal_KBOUT.KBOut.header.handle) // Keyboard Output Report
        {
		CharVal_KBOUT.KBOut_Val = write_req->p_value[0];	
        st = ATT_SUCCESS; //ToDo: Not using the Keyboard Output Report
        } 
	else if (write_req->handle == CharVal_BT_KBOUT.KBOut.header.handle) 
		{
		CharVal_BT_KBOUT.KBOut_Val = write_req->p_value[0];
        st = ATT_SUCCESS;
		}
    else if(write_req->handle == MouseIN_CCC.mouse_ccc.header.handle)
        {
        MouseIN_CCC.nty_Ind[ccc_idx] = write_req->p_value[0];
        st = ATT_SUCCESS;
        }
    else if(write_req->handle == BT_KBIN_CCC.bt_kbin_ccc.header.handle)
        {
        BT_KBIN_CCC.nty_Ind[ccc_idx] = write_req->p_value[0];
        st = ATT_SUCCESS;
        }
    else if(write_req->handle == BT_MouseIN_CCC.bt_mouse_ccc.header.handle)
        {
        BT_MouseIN_CCC.nty_Ind[ccc_idx] = write_req->p_value[0];
        st = ATT_SUCCESS;
        }
    else if(write_req->handle == CharVal_CtrlPt.CtrlPt.header.handle)
        { 
        CharVal_CtrlPt.CtrlPt_Val = write_req->p_value[0];
        st = ATT_SUCCESS;        
        }
    else if(write_req->handle == CharVal_KB_Feature1.kb_feature1.header.handle) // Keyboard Input Report
        {/* Keyboard Feature Report */
		HID_DEBUG("CharVal_KB_Feature1.kb_feature1.header.handle");
        CharVal_KB_Feature1.kb_feature1_val = write_req->p_value[0];
        st = ATT_SUCCESS;        
        }
    /* There is no CCC for Keyboard Output Report, So proceed further */            
    else if( (write_req->handle == KBOUT_RPT_REF.kbout_RptRef.header.handle) ||
            (write_req->handle == CharVal_MouseIN.header.handle) ||// Mouse Input Report
            (write_req->handle == KBIN_RPT_REF.kbin_RptRef.header.handle) ||
            (write_req->handle == CharVal_KBIN.header.handle) ||// Keyboard Input Report
            (write_req->handle == MouseIN_RPT_REF.mouse_RptRef.header.handle) ||
            (write_req->handle == CharVal_ReportMap.header.handle) ||
            (write_req->handle == CharDescERR_ReportMap_BAL.Err.header.handle) || 
            (write_req->handle == CharDescERR_ReportMap_ScRef.Err.header.handle) ||
            (write_req->handle == CharVal_BT_KBIN.header.handle) ||
            (write_req->handle == CharVal_BT_MouseIN.header.handle) ||
            (write_req->handle == CharVal_HIDInfo.header.handle) )
        {
		HID_DEBUG("ATT_WRITE_NOT_PERMIT");
        st = ATT_WRITE_NOT_PERMIT;
        }
    else
        st = ATT_INVALID_HANDLE;

    *p_status = st;
    return;

}


/*!
 * @brief  Internal Function to get the Attribute Handle for the Report to be sent.
 *
 * @param conn_id Gatt Server Connection Id
 * @param rpt_ID HOGP report id
 * @param rpt_type HOGP report type
 *
 * @return uint16 Returns If requested rpt_id & rpt_type are present then returns its 
 *                       Attribute Handle else Zero is returned
 */
static uint16 get_attr_id(tconn_id connID, uint8 rpt_ID, uint8 rpt_type)
{
    uint8 ccc_idx = fetch_cccIdx(connID);

    switch(CharVal_PrtlModeVal.PrtlMode_Val[ccc_idx])
    {
        case HOGP_PROTOCOL_MODE_BOOT:
        {
            if(rpt_ID == HOGP_REPORT_ID_KB && rpt_type == HOGP_REPORT_TYPE_INPUT)
                return (CharVal_BT_KBIN.header.handle);
            /*else if(rpt_ID == HOGP_REPORT_ID_KB && rpt_type == HOGP_REPORT_TYPE_OUTPUT)
                return (CharVal_BT_KBOUT.header.handle);*/
            else if(rpt_ID == HOGP_REPORT_ID_MOUSE && rpt_type == HOGP_REPORT_TYPE_INPUT)
                return (CharVal_BT_MouseIN.header.handle);    
        }
        break;

        case HOGP_PROTOCOL_MODE_REPORT:
        {
            if(rpt_ID == HOGP_REPORT_ID_KB && rpt_type == HOGP_REPORT_TYPE_INPUT)
                return (CharVal_KBIN.header.handle);
            /*else if(rpt_ID == HOGP_REPORT_ID_KB && rpt_type == HOGP_REPORT_TYPE_OUTPUT)
                return (CharVal_KBOUT.header.handle);*/
            else if(rpt_ID == HOGP_REPORT_ID_MOUSE && rpt_type == HOGP_REPORT_TYPE_INPUT)
                return (CharVal_MouseIN.header.handle);
            /*else if(rpt_ID == HOGP_REPORT_ID_BATTERY && rpt_type == HOGP_REPORT_TYPE_INPUT)
                return (CharVal_BALevel.bal_CharValDecl.header.handle);    
            else if(rpt_ID == HOGP_REPORT_ID_SCAN_REF && rpt_type == HOGP_REPORT_TYPE_INPUT)
                return (CharVal_ScRefresh.ScRefresh_CharValDecl.header.handle);        */
        }
        break;

        default:
            break;
    }
    return 0; // Return attr_id = 0, if requested rpt_id & rpt_type are not matching.

}


/*!
 * @brief  Internal Function to Fetch the CCCID corresponding to Connection ID.
 *
 * @param conn_id Gatt Server Connection Id
 *
 * @return uint8 Returns Index value for Particular Gatt connection Handle, which is mapped to its CCC.
 */
inline static uint8 fetch_cccIdx(tconn_id connID)
{
    uint8 idx=0;
    for(; idx<MAX_CONN_PER_DEV && connID != hidd_ctrlblk.connID_cccIndex_Map[idx]; idx++);
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
    if(idx >= MAX_CONN_PER_DEV)
        HID_DEBUG("fetch_cccIdx: INVALID Conn Handle !!  connID=%p, CCCids=%d",connID, idx);
    #endif    
    return idx;
}


/*!
 * @brief  Internal Function to Fetch the AppID corresponding to Connection ID.
 *
 * @param conn_id Gatt Server Connection Id
 *
 * @return uint8 Returns Index value for Particular Gatt connection Handle, which is mapped to its AppID.
 */
inline static uint8 fetch_appId(tconn_id connID)
{
    uint8 idx=0;
    for(; idx<MAX_APP_ID && connID != hidd_ctrlblk.connContext[idx].connID; idx++);
    #if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
    if(idx >= MAX_CONN_PER_DEV)
        HID_DEBUG("fetch_cccIdx: INVALID Conn Handle !!  connID=%p, CCCids=%d",connID, idx);
    #endif    
    return idx;
}

#endif // #if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)

