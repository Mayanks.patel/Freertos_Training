
/*
 * Copyright (C) 2008-2015, Marvell International Ltd.
 * All Rights Reserved.
 */

#ifndef _BLE_H_
#define _BLE_H_

void wmsdk_ble_init();

#endif /* _BLE_H_ */
