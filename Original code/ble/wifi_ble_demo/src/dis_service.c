/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   dis_service.c
 * \brief  This file contains the implementation of the Device Information Service Handling
 * \author Marvell Semiconductor
 */

#include "utils.h"
#include "service_conf.h"
#include "dis_service.h"
#include "main_gatt_api.h"

#define DIS_SERVICE_VERSION "MRVL-LE-DIS-11.0.0.0"

#define MANU_NAME_STR "Marvell Technology"
#define MODEL_NUMBER_STR "MRVL-LE-DIS-11.0.0.0"
#define SERIAL_NUMBER_STR	"MRVL-11.0.0.0"
#define	HW_REVISION_STR	"HW-w8897d"
#define FW_REVISION_STR	"FW-15_28_4_p95"
#define SW_REVISION_STR	"SW-Release_1215"


static tgatt_uuid16_char_decl_record CharDecl_Manu_Name_Str;
static tgatt_uuid16_char_decl_record CharDecl_Model_Number_Str;
static tgatt_uuid16_char_decl_record CharDecl_Serial_Number_Str;
static tgatt_uuid16_char_decl_record CharDecl_Hardware_Revision_Str;
static tgatt_uuid16_char_decl_record CharDecl_Firmware_Revision_Str;
static tgatt_uuid16_char_decl_record CharDecl_Software_Revision_Str;
static tgatt_uuid16_char_decl_record CharDecl_SystemID;
static tgatt_uuid16_charval_decl_record_std CharVal_Manu_Name_Str;
static tgatt_uuid16_charval_decl_record_std CharVal_Model_Number_Str;
static tgatt_uuid16_charval_decl_record_std CharVal_Serial_Number_Str;
static tgatt_uuid16_charval_decl_record_std CharVal_Hardware_Revision_Str;
static tgatt_uuid16_charval_decl_record_std CharVal_Firmware_Revision_Str;
static tgatt_uuid16_charval_decl_record_std CharVal_Softmware_Revision_Str;
static tDIS_SYSID_CharValDecl CharVal_SystemID;

uint8	manu_name[] = MANU_NAME_STR;
uint8	model_numb_str[] = MODEL_NUMBER_STR;
uint8	serial_numb_str[] = SERIAL_NUMBER_STR;
uint8	hw_revision_str[] = HW_REVISION_STR;
uint8	fw_revision_str[] = FW_REVISION_STR;
uint8	sw_revision_str[] = SW_REVISION_STR;

uint8   manuf_def_id[] = {0xaa, 0xbb, 0xcc, 0xdd, 0xee};   //0xeedddccbbaa
uint8   org_uinq_id[] = {0x11, 0x12, 0x13};   //0x131211

/***********************************************************************   
  Sturcture & Function Declarations   
 ************************************************************************/
static tgatt_uuid16_service_record DIS_SrvcRecord;
static tgatt_uuid16_char_decl_record CharDecl_PNPID;
static tDIS_PNPID_CharValDecl CharVal_PNPID;
static tgatts_cb_result dis_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data);


/***********************************************************************
  Global Variables & Data Sturctures    
 ************************************************************************/
static uint8 attr_val[MAX_SRVC_DATA] = {0};
static boolean dis_srvc_initialized = FALSE;


/* Primary service - DIS Service 
 ********** Start **************
 */
static tgatt_uuid16_service_record DIS_SrvcRecord =
{
  /* SERVICE DEFINITION */
  NULL,                                                                    ///< Link pointer for all_service_q 
  {&CharDecl_PNPID, GATT_PRIM_SERVICE_RECORD_UUID16, GATT_START_HANDLE},   ///< record header.next_ptr is set to address of next record of the service 
   16,                                ///< Service End handle, set this value to total number of attributes associated with this service excluding service record itself 
  UUID_DECL_PRIMARY_SERVICE,        ///< 0x2800 for «Primary Service» 
  dis_serv_req_handler,             ///< Service Callback
  UUID_SERVICE_DEVICE_INFO,         ///< 0x180A - for Bluetooth DI service 
};

static tgatt_uuid16_char_decl_record CharDecl_PNPID =
{
  /* Characteristics Declaration */
  {&CharVal_PNPID, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,         ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                ///< Read 
  GATT_NEXT_HANDLE,                 ///< Set to Next attribute record
  GATT_UUID_PNP_ID                  ///< UUID for PnP ID 
};

static tDIS_PNPID_CharValDecl CharVal_PNPID =
{
  /* Characteristics Value Declaration */
  {
 {&CharDecl_Manu_Name_Str, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
	GATT_UUID_PNP_ID,                                                     ///< UUID for PnP ID
	GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  {VENDORID_SRC,VENDORID,PRODUCTID,PRODUCT_VER}
};


static tgatt_uuid16_char_decl_record CharDecl_Manu_Name_Str =
{
  /* Characteristics Declaration */
  {&CharVal_Manu_Name_Str, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,         ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                ///< Read 
  GATT_NEXT_HANDLE,                 ///< Set to Next attribute record
  GATT_UUID_MANU_NAME              ///< UUID for Manufacturer Name 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Manu_Name_Str =
{
  /* Characteristics Value Declaration */
    {&CharDecl_Model_Number_Str, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
    GATT_UUID_MANU_NAME,                                                     ///< UUID for Manufacturer Name
    GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};

static tgatt_uuid16_char_decl_record CharDecl_Model_Number_Str =
{
  /* Characteristics Declaration */
  {&CharVal_Model_Number_Str, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},   ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,         ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                ///< Read 
  GATT_NEXT_HANDLE,                 ///< Set to Next attribute record
  GATT_UUID_MODEL_NUMBER_STR        ///< UUID for Model number string 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Model_Number_Str =
{
  /* Characteristics Value Declaration */
    {&CharDecl_Serial_Number_Str, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
    GATT_UUID_MODEL_NUMBER_STR,                                           ///< UUID for Model number string
    GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};

static tgatt_uuid16_char_decl_record CharDecl_Serial_Number_Str =
{
 /* Characteristic Declaration */
 {&CharVal_Serial_Number_Str, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
 UUID_DECL_CHARACTERISTIC,
 GATT_CH_PROP_READ,          ///< Read Property
 GATT_NEXT_HANDLE,
 GATT_UUID_SERIAL_NUMBER_STR ///< UUID for Serial number String 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Serial_Number_Str = 
{
  /* Characteristic Value Declaration */
    {&CharDecl_Hardware_Revision_Str, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE}, 
    GATT_UUID_SERIAL_NUMBER_STR,									///< UUID for Serial number String 
    GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
};

static tgatt_uuid16_char_decl_record CharDecl_Hardware_Revision_Str =
{
	/* Characteristic Declaration for Hardware Revision String */
 {&CharVal_Hardware_Revision_Str, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
 UUID_DECL_CHARACTERISTIC,
 GATT_CH_PROP_READ,          ///< Read Property 
 GATT_NEXT_HANDLE,
 GATT_UUID_HW_VERSION_STR   ///< UUID for Hardware Revision String 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Hardware_Revision_Str = 
{
	/* Characteristic Value Declatation for Hardware Revision String */
   {&CharDecl_Firmware_Revision_Str, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},
   GATT_UUID_HW_VERSION_STR,
   GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
}; 

static tgatt_uuid16_char_decl_record CharDecl_Firmware_Revision_Str = 
{
	/* Characteristic Declaration for Firmware Revision String */
 {&CharVal_Firmware_Revision_Str, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
 UUID_DECL_CHARACTERISTIC,
 GATT_CH_PROP_READ, 		 ///< Read Property  
 GATT_NEXT_HANDLE,
 GATT_UUID_FW_VERSION_STR   ///< UUID for Firmware Revision String 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Firmware_Revision_Str = 
{
   /* Characteristic Value Declaration for Firmware Revision String */
  {&CharDecl_Software_Revision_Str, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},
  GATT_UUID_FW_VERSION_STR,
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC 
};

static tgatt_uuid16_char_decl_record CharDecl_Software_Revision_Str = 
{
	/* Characteristic Declaration for Software Revision String */
 {&CharVal_Softmware_Revision_Str, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
 UUID_DECL_CHARACTERISTIC,
 GATT_CH_PROP_READ,         ///< Read Property 
 GATT_NEXT_HANDLE,
 GATT_UUID_SW_VERSION_STR  ///< UUID for Software Revision String 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Softmware_Revision_Str = 
{
	/* Characteristic Value Declaration for Software Revision String */
  {&CharDecl_SystemID, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},
  GATT_UUID_SW_VERSION_STR,
  GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC 
};

static tgatt_uuid16_char_decl_record CharDecl_SystemID =
{
	/* Characteristic Declaration for System ID */
 {&CharVal_SystemID, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},
 UUID_DECL_CHARACTERISTIC,
 GATT_CH_PROP_READ,       ///< Read Property 
 GATT_NEXT_HANDLE,
 GATT_UUID_SYSTEM_ID	 ///< UUID for System ID  
};

static tDIS_SYSID_CharValDecl CharVal_SystemID =
{
  {
   {NULL, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},
   GATT_UUID_SYSTEM_ID,
   GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
 {manuf_def_id, org_uinq_id}
}; 
/* Primary service - DIS Service 
 ********** End **************
 */
 

/*!
 * @brief  Device Information Service Init API for APP/Profile.
 *            This API Call Instantiates a Device Information Service Instance.
 *
 * @param Void Takes No Parameters
 *
 * @return Void Returns None
 */
void dis_serv_init(void)
{
  /* Add the DIS service record to GATT Only once, as there can be only single instance for Device Info Service*/
  if(!dis_srvc_initialized)
  {
	/* Create DIS Service */
	GATTS_Add_Service((void *) &DIS_SrvcRecord);
	dis_srvc_initialized = TRUE;
  }
  return;
}


/*!
 * @brief  Service Request Handler Registered with Local Gatt Server
 *            This Callback funtion handles all the Remote Gatt Client Requests & Events of this service
 *
 * @param conn_id Gatt Server Connection Id
 * @param req_type Gatt Server's request
 * @param p_req_data Request/Event Parameters
 *
 * @return Void Returns None
 */
static tgatts_cb_result dis_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data)
{
  tgatts_cb_result result;
  static tgatts_rsp read_rsp;  
  read_rsp.p_val = attr_val;
  uint8 *tmp_p_val_ptr;

  result.status = ATT_SUCCESS; /*Default return status value*/


  if(req_type == GATT_EVENT_READ_REQ) 
  {
	if(((tgatts_req *)p_req_data)->handle == CharVal_PNPID.pnpid_CharValDecl.header.handle)
	{
      tmp_p_val_ptr = read_rsp.p_val;
	  UINT8_TO_LE_ENDIAN(CharVal_PNPID.pnpidVal.vendorIDsrc,tmp_p_val_ptr);
	  UINT16_TO_LE_ENDIAN(CharVal_PNPID.pnpidVal.vendorID,tmp_p_val_ptr);
	  UINT16_TO_LE_ENDIAN(CharVal_PNPID.pnpidVal.productID,tmp_p_val_ptr);
	  UINT16_TO_LE_ENDIAN(CharVal_PNPID.pnpidVal.productVersion,tmp_p_val_ptr);

	  read_rsp.val_len = 0x07;
	}
    else if (((tgatts_req *)p_req_data)->handle == CharVal_Manu_Name_Str.header.handle)
	{ 
        read_rsp.p_val = manu_name; 
 		read_rsp.val_len = sizeof(MANU_NAME_STR);
	}
	else if(((tgatts_req *)p_req_data)->handle == CharVal_Model_Number_Str.header.handle)
	{
		read_rsp.p_val =  model_numb_str;
        read_rsp.val_len = sizeof(MODEL_NUMBER_STR); 
	}
    else if (((tgatts_req *)p_req_data)->handle == CharVal_Serial_Number_Str.header.handle)
	{
		read_rsp.p_val = serial_numb_str;
        read_rsp.val_len = sizeof(SERIAL_NUMBER_STR);
	}
    else if (((tgatts_req *)p_req_data)->handle == CharVal_Hardware_Revision_Str.header.handle)
	{
		read_rsp.p_val = hw_revision_str;
        read_rsp.val_len = sizeof(HW_REVISION_STR);
	}
	else if (((tgatts_req *)p_req_data)->handle == CharVal_Firmware_Revision_Str.header.handle)
	{
		read_rsp.p_val = fw_revision_str;
        read_rsp.val_len = sizeof(FW_REVISION_STR);
	}
	else if (((tgatts_req *)p_req_data)->handle == CharVal_Softmware_Revision_Str.header.handle)
	{
		read_rsp.p_val = sw_revision_str;
        read_rsp.val_len = sizeof(SW_REVISION_STR);
	}
	else if (((tgatts_req *)p_req_data)->handle == CharVal_SystemID.sysid_CharValDecl.header.handle) 
	{ 
		tmp_p_val_ptr =  read_rsp.p_val;
		ARRAY_TO_STREAM(tmp_p_val_ptr, CharVal_SystemID.sysidVal.manufDefID, sizeof(manuf_def_id));
		ARRAY_TO_STREAM(tmp_p_val_ptr, CharVal_SystemID.sysidVal.orgUniqID, sizeof(org_uinq_id));
        read_rsp.val_len = 0x08;
	}
	else
	  result.status = ATT_INVALID_HANDLE;

	read_rsp.rsp_of = GATT_EVENT_READ_REQ;
	read_rsp.handle = ((tgatts_req *)p_req_data)->handle ;

	GATTS_Send_Rsp(conn_id, result.status, (tgatts_rsp *)&read_rsp); 
  }

  return result;
}


