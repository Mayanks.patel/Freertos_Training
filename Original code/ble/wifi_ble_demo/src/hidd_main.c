/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file     hidd_main.c
 * \brief   This file contains the implementation of the State Events Handling
 *        for the functionality of the HOGP Device Role.
 * \author Marvell Semiconductor
 */

#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)

#include "hidd_main.h"
#include "hidd_le.h"
#include "types.h"


/***********************************************************************

  Constants & Data Types

 ************************************************************************/
tHIDD_CtrlBlk hidd_ctrlblk = {{0}};

/* EVENTS - State Machine*/    
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
char *HID_EVT_DEBUG[] = {
  "HIDD_API_INIT_EVT",                //HIDD_API_INIT_EVT
  "HIDD_API_REGISTER_APP_EVT",         //HIDD_API_REGISTER_APP_EVT
  "HIDD_API_UNREGISTER_APP_EVT",        //HIDD_API_UNREGISTER_APP_EVT
  "HIDD_API_CONNECT_EVT",                //HIDD_API_CONNECT_EVT
  "HIDD_API_DISCONNECT_EVT",            //HIDD_API_DISCONNECT_EVT
  "HIDD_API_SEND_REPORT_EVT",            //HIDD_API_SEND_REPORT_EVT
  //    "HIDD_API_REPORT_ERROR_EVT",    
  //    "HIDD_INT_APP_REGISTERED_EVT",
  //    "HIDD_INT_APP_UNREGISTERED_EVT",
  "HIDD_INT_OPEN_EVT",                //HIDD_INT_OPEN_EVT
  "HIDD_INT_CLOSE_EVT",                //HIDD_INT_CLOSE_EVT
  //    "HIDD_INT_SUSPEND_EVT",
  //    "HIDD_INT_EXIT_SUSPEND_EVT",
  "HIDD_IGNORE",
};
#endif


/*******************************************************************
 ********************** EVENT - STATE MAPPING ************************
 ********************************************************************
 HIDD_INIT_ST
 Event                            Action                    Next state 
 HIDD_API_INIT_EVT                hogp_reg_service_act        HIDD_INIT_ST
 HIDD_API_REGISTER_APP_EVT         HIDD_REGISTER_ACT        HIDD_IDLE_ST
 HIDD_API_UNREGISTER_APP_EVT       HIDD_IGNORE            HIDD_INIT_ST 
 HIDD_API_CONNECT_EVT                  HIDD_IGNORE                     HIDD_INIT_ST 
 HIDD_API_DISCONNECT_EVT           HIDD_IGNORE                     HIDD_INIT_ST 
 HIDD_API_SEND_REPORT_EVT          HIDD_IGNORE                     HIDD_INIT_ST 
 HIDD_API_REPORT_ERROR_EVT         HIDD_IGNORE                     HIDD_INIT_ST 
 HIDD_INT_APP_REGISTERED_EVT                                          
 HIDD_INT_APP_UNREGISTERED_EVT                                        
 HIDD_INT_OPEN_EVT                     HIDD_IGNORE                     HIDD_INIT_ST 
 HIDD_INT_CLOSE_EVT                    HIDD_IGNORE                     HIDD_INIT_ST 
 HIDD_INT_SUSPEND_EVT                  HIDD_IGNORE                     HIDD_INIT_ST 
 HIDD_INT_EXIT_SUSPEND_EVT         HIDD_IGNORE                     HIDD_INIT_ST 

 HIDD_IDLE_ST
 Event                            Action                    Next state 
 HIDD_API_REGISTER_APP_EVT         HIDD_IGNORE                     HIDD_IDLE_ST 
 HIDD_API_UNREGISTER_APP_EVT       HIDD_UNREGISTER_ACT    HIDD_INIT_ST 
 HIDD_API_CONNECT_EVT                  HIDD_CONNECT_ACT            HIDD_IDLE_ST 
 HIDD_API_DISCONNECT_EVT           HIDD_IGNORE                     HIDD_IDLE_ST 
 HIDD_API_SEND_REPORT_EVT          HIDD_CONNECT_ACT        HIDD_IDLE_ST 
 HIDD_API_REPORT_ERROR_EVT         HIDD_IGNORE                     HIDD_IDLE_ST 
 HIDD_INT_APP_REGISTERED_EVT                                             
 HIDD_INT_APP_UNREGISTERED_EVT                                         
 HIDD_INT_OPEN_EVT                     HIDD_OPEN_ACT               HIDD_CONN_ST 
 HIDD_INT_CLOSE_EVT                    HIDD_IGNORE                     HIDD_IDLE_ST 
 HIDD_INT_SUSPEND_EVT                  HIDD_IGNORE                     HIDD_IDLE_ST 
 HIDD_INT_EXIT_SUSPEND_EVT         HIDD_IGNORE                     HIDD_IDLE_ST 


 HIDD_CONN_ST
 Event                            Action                    Next state 
 HIDD_API_REGISTER_APP_EVT         HIDD_IGNORE                     HIDD_CONN_ST 
 HIDD_API_UNREGISTER_APP_EVT       HIDD_DISCONNECT_ACT     HIDD_W4UNREG_ST
 HIDD_API_CONNECT_EVT                  HIDD_IGNORE                     HIDD_CONN_ST 
 HIDD_API_DISCONNECT_EVT           HIDD_DISCONNECT_ACT     HIDD_CONN_ST 
 HIDD_API_SEND_REPORT_EVT          HIDD_SEND_REPORT_ACT       HIDD_CONN_ST 
 HIDD_API_REPORT_ERROR_EVT         HIDD_REPORT_ERROR_ACT    HIDD_CONN_ST 
 HIDD_INT_APP_REGISTERED_EVT                                           
 HIDD_INT_APP_UNREGISTERED_EVT                                         
 HIDD_INT_OPEN_EVT                     HIDD_IGNORE                     HIDD_CONN_ST 
 HIDD_INT_CLOSE_EVT                    HIDD_CLOSE_ACT              HIDD_IDLE_ST 
 HIDD_INT_SUSPEND_EVT                  HIDD_SUSPEND_ACT            HIDD_CONN_ST 
 HIDD_INT_EXIT_SUSPEND_EVT         HIDD_EXIT_SUSPEND_ACT     HIDD_CONN_ST 


 HIDD_W4_UNREG_ST
 Event                            Action                    Next state 
 HIDD_API_REGISTER_APP_EVT         HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_API_UNREGISTER_APP_EVT       HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_API_CONNECT_EVT                  HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_API_DISCONNECT_EVT           HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_API_SEND_REPORT_EVT          HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_API_REPORT_ERROR_EVT         HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_INT_APP_REGISTERED_EVT                                             
 HIDD_INT_APP_UNREGISTERED_EVT                                        
 HIDD_INT_OPEN_EVT                     HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_INT_CLOSE_EVT                    HIDD_UNREGISTER2_ACT    HIDD_INIT_ST 
 HIDD_INT_SUSPEND_EVT                  HIDD_IGNORE                     HIDD_W4UNREG_ST 
 HIDD_INT_EXIT_SUSPEND_EVT         HIDD_IGNORE                     HIDD_W4UNREG_ST 

 *******************************************************************************/

/*!
 * @brief  HOGP Device profile state events handler.
 *            This function Handle events of the state machine. The following state machine is implemented
 *            according to the above EVENT-STATE Mapping. Any Changes made in state 
 *            machine shall also be modified in above Mapping for better code readability.
 *
 * @param event Pointer to State-Event parameter structure
 *
 * @return Void Returns None
 */
void state_event_handler(thidd_state_event *evt)
{
  switch(hidd_ctrlblk.connContext[evt->appid].cur_state)
  {
    case HIDD_INIT_ST:
      {    
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("[HIDD_INIT_ST STATE] [%s]", HID_EVT_DEBUG[evt->event_id]);
#endif    
        switch(evt->event_id)
        {
          case HIDD_API_INIT_EVT:
            hogp_reg_service_act();//Register HOGP services to Gatt; No state change
            break;
          case HIDD_API_REGISTER_APP_EVT:
            {
              hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_IDLE_ST;
              hogp_reg_app_act(evt->appid);//HIDD_REGISTER_ACT
            }
            break;

          default:
            /* Apart from above Handled EVTs, IGNORE other Events - Current State doesn't change */
            break;
        }                

      }
      break;

    case HIDD_IDLE_ST:
      {
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("[HIDD_IDLE_ST] [%s]", HID_EVT_DEBUG[evt->event_id]);
#endif    
        switch(evt->event_id)
        {
          case HIDD_API_UNREGISTER_APP_EVT:
            {
              hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_INIT_ST;
              hogp_dereg_app_act(&evt->appid); //HIDD_UNREGISTER_ACT            
            }
            break;
#if 0 //Currently Not being used
          case HIDD_API_CONNECT_EVT:
            {    
              hogp_conn_act((tConn_params *)evt->pdata); //HIDD_CONNECT_ACT
              //hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_IDLE_ST;
            }
            break;
#endif			
          case HIDD_API_SEND_REPORT_EVT:
            {    //Now App will handle this case of doing Directed Advertisement to Last Connected Device
              //tConn_params connParam = {evt->appid, hidd_ctrlblk.reconn_devid.reConnDevId };
              //hogp_conn_act(&connParam); //HIDD_CONNECT_ACT
            }
            break;
          case HIDD_INT_OPEN_EVT:
            {    
              hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_CONN_ST;
              hogp_open_act(evt->pdata,evt->appid); //HIDD_OPEN_ACT
            }
            break;
          default:
            /* Apart from above Handled EVTs, IGNORE other Events - Current State doesn't change */
            break;
        }        
      }
      break;

    case HIDD_CONN_ST:
      {
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("[HIDD_CONN_ST] [%s]", HID_EVT_DEBUG[evt->event_id]);
#endif    

        switch(evt->event_id)
        {
          case HIDD_API_UNREGISTER_APP_EVT:
            {
              hogp_disconn_act((tDisConn_params *)evt->pdata); //HIDD_DISCONNECT_ACT
              hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_W4_UNREG_ST;
            }
            break;
          case HIDD_API_DISCONNECT_EVT:
            {    
              hogp_disconn_act((tDisConn_params *)evt->pdata); //HIDD_DISCONNECT_ACT
              //hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_CONN_ST; WAIT untill HIDD_INT_CLOSE_EVT, which indicates
              //disconnection is complete, then move to HIDD_IDLE_ST
            }
            break;
          case HIDD_API_SEND_REPORT_EVT:
            {                  
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
              HID_DEBUG("IN Case CONN_ST: appid=%d, connHdl=%p",evt->appid,((tSendReport_params*)(evt->pdata))->connID);    
#endif
              hogp_send_report_act((tSendReport_params*)(evt->pdata)); //HIDD_SEND_REPORT_ACT
              //hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_CONN_ST;
            }
            break;
          case HIDD_INT_CLOSE_EVT:
            {    
              hogp_close_act(evt->pdata); //HIDD_CLOSE_ACT
              hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_IDLE_ST;
            }
            break;
          default:
            /* Apart from above Handled EVTs, IGNORE other Events - Current State doesn't change */
            break;
        }
      }
      break;

    case HIDD_W4_UNREG_ST:
      {        
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
        HID_DEBUG("[HIDD_W4_UNREG_ST] [%s]", HID_EVT_DEBUG[evt->event_id]);
#endif    
        switch(evt->event_id)
        {
          case HIDD_INT_CLOSE_EVT:
            {    
              //HIDD_UNREGISTER2_ACT
              hidd_ctrlblk.connContext[evt->appid].cur_state = HIDD_INIT_ST;
            }
            break;
          default:
            /* Apart from above Handled EVTs, IGNORE other Events - Current State doesn't change */              
            break;
        }
      }
      break;

    default:
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
      HID_DEBUG("INVALID Current STATE!! Cur_State=%d, AppID=%d", hidd_ctrlblk.connContext[evt->appid].cur_state, evt->appid);
#endif
      break;

  }
}


#endif //If (HID_DEVICE_ENABLED)
