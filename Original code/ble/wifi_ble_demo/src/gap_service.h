/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   gap_service.h
 * \brief  This file contains the data structures and function API declarations 
 *        that would be used by different Gap Service.
 * \author Marvell Semiconductor
 */


#ifndef _GAP_SERVICE_H
#define _GAP_SERVICE_H

#include "main_gap_declares.h"
#include "main_gap_api.h"


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/
typedef uint8 tgap_proc_req_code;

/*!
* \name GAPS Request Opcodes
*/
//@{
#define GAP_READ_REMOTE_NAME_REQ            0x01
#define GAP_START_SERVICE_SEARCH_REQ        0x02
//@}


/**************************************************************************************************************
  
  Structure Definitions
  
****************************************************************************************************************/
/*!
* \brief GAPS Request Api Parameters
*/
typedef union {
  
  tBT_UUID service_uuid;
  
} tgaps_req_api_params;

/*!
* \brief GAPS Request Api Parameters
*/
typedef struct {
  
  uint8 *p_local_dev_name;
  uint8 local_dev_name_len;
  uint16 local_device_appearance;
  
} tgap_serv_read_local_char_params;


/**************************************************************************************************************
  
  Function Definitions
  
****************************************************************************************************************/
void gap_serv_init(void);
void Gap_Serv_Read_Local_Char_Api(tgap_serv_read_local_char_params *p_char_value_param);
extern bt_result Gaps_Client_Request(tgap_proc_req_code req_code, tbt_device_id *device_id,tgaps_req_api_params *params, bt_gap_proc_status_callback req_cb);

#endif //_GAP_SERVICE_H
