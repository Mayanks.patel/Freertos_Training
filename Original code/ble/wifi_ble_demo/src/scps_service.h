/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   scps_service.h
 * \brief  This file contains the data structures and function API declarations 
 *        that would be used by different SCPS Service.
 * \author Marvell Semiconductor
 */


#ifndef _SCPS_SERVICE_H
#define _SCPS_SERVICE_H

#include "mrvlstack_config.h"
#include "main_gatt_api.h"
#include "service_conf.h"


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/

/*!
* \brief Scan Interval Window Characteristic Value
*/
typedef struct {
  uint16    le_scan_int;  ///< Remote Client's LE SCAN INTERVAL VALUE
  uint16    le_scan_win;  ///< Remote Client's LE SCAN WINDOW VALUE
}ScIntWin_Char_Val;

/*!
* \brief Scan Interval Window Characteristic
*/
typedef struct {
  tgatt_uuid16_charval_decl_record_std ScIntWin_CharValDecl;
  ScIntWin_Char_Val ScIntWinVal[MAX_CONN_PER_DEV];  ///< Value, maintained per Connection/Client
}tSCPS_ScIntWin_CharValDecl;

/*!
* \brief Scan Refresh Characteristic Value
*/
typedef struct {
  tgatt_uuid16_charval_decl_record_std ScRefresh_CharValDecl; ///< Scan Refresh Characteristic Attribute Record
  uint8 Sc_Refresh; ///< Scan Refresh Characteristic Value
}tSCPS_ScRef_CharValDecl;

/*!
* \brief Scan Refresh Client Characteristic Configuration Descriptor
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    ScRef_ccc;  ///< Scan Refresh Client Characteristic Configuration Attribute Record
  uint16                            nty_Ind[MAX_CONN_PER_DEV];    ///< CCC Value, maintained per Connection/Client[0x01 - Notify; 0x02 - Indicate]
}tSCPS_ScRef_CCC;

/*!
* \brief SCPS Service data to be saved to NVM(Non Volatile Memory)
*/
typedef struct {
  boolean valid_data;          ///< Set TRUE while storing the data, so that while loading only valid data can be loaded, else dont load.
  uint16 scnRef_nty_Ind_ccc; ///< 0x01 - Notify; 0x02 - Indicate  
  ScIntWin_Char_Val ScIntWinVal;  ///< Remote Client's Scan Interval & Scan Window Value
}tSCPS_nvm_data;


/***********************************************************************

  Function Declarations

 ************************************************************************/
void scps_serv_init(void);
thandle_range scps_service_to_be_included(void);    //Returns the Scan Parameter Service Handle Range.
void scps_srvc_send_notify(tconn_id conn_id, uint16 val_len, uint8 * pval);
void scps_srvc_read_ScanIntWin_char(tconn_id conn_id, ScIntWin_Char_Val *p_char_value_param);

#endif //_SCPS_SERVICE_H
