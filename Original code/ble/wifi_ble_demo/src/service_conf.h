/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
  * \file   service_conf.h
 * \brief  This file contains the Common Macros, data structures and function API declarations 
 *        that would be used by different Gatt Services(ex: bas_service, scps service, etc).
 * \author Marvell Semiconductor
 */


#ifndef _SERVICE_CONF_H
#define _SERVICE_CONF_H

#include "mrvlstack_config.h"
#include "main_gap_declares.h"
#include "main_gatt_api.h"
#include "log.h"


/* Trace Statements */
#if ((defined MRVL_STACK_DEBUG_ENABLED) && (MRVL_STACK_DEBUG_ENABLED == TRUE)) && ((defined SRVC_DEBUG_ENABLED) && (SRVC_DEBUG_ENABLED ==TRUE))
extern uint8 SRVC_MSG_level; 

#define SRVC_ERROR(m, arg...)                    {if (SRVC_MSG_level >= ERROR) print(m, ##arg);}
#define SRVC_WARNING(m, arg...)                  {if (SRVC_MSG_level >= WARNING) print(m, ##arg );}
#define SRVC_DEBUG(m, arg...)                    {if (SRVC_MSG_level >= DEBUG) print(m, ##arg);}
#define SRVC_ALL(m, arg...)                      {if (SRVC_MSG_level >= ALL) print(m, ##arg);}
#else
#define SRVC_ERROR(m, arg...)
#define SRVC_WARNING(m, arg...)
#define SRVC_DEBUG(m, arg...)
#define SRVC_ALL(m, arg...)
#endif


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/
#define TO_NOTIFY                (0x0001)
#define TO_INDICATE              (0x0002)

#define MAX_CONN_PER_DEV    1 ///<Maximum number of Connections supported by device/UUT
#define MAX_SRVC_DATA            10         ///< Maximum Gatt Server Attribute Value that will be READ from or WRITTEN into server DB.

/*!
* \brief Service Device/Connection Context
*/
typedef struct {
  tbt_device_id dev_id;  ///< Remote Device Id
  tconn_id conn_id;  ///< Gatt Server Connection Id for the above Device Id
}tSRVC_ConnContext;


/***********************************************************************

  Function Declarations

 ************************************************************************/


#endif //_SERVICE_CONF_H
