/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file    hidd_main.h
 * \brief  This file contains the declarations of States, Events & State Event Handler
 *        used by the Hidd_main.c file.
 * \author Marvell Semiconductor
 */

#ifndef _HID_MAIN_H
#define _HID_MAIN_H

#define HID_DEVICE_ENABLED TRUE

#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)

#include "types.h"
#include "timer.h"
#include "log.h"
#include "hidd_api.h"
#include "service_conf.h"


/* Trace Statements */
#if ((defined MRVL_STACK_DEBUG_ENABLED) && (MRVL_STACK_DEBUG_ENABLED == TRUE)) && (defined(HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
extern uint32 HID_MSG_level; 

#define HID_ERROR(m, arg...)                    {if (HID_MSG_level >= ERROR) print(m, ##arg);}
#define HID_WARNING(m, arg...)                  {if (HID_MSG_level >= WARNING) print(m, ##arg );}
#define HID_DEBUG(m, arg...)                    {if (HID_MSG_level >= DEBUG) print(m, ##arg);}
#define HID_ALL(m, arg...)                      {if (HID_MSG_level >= ALL) print(m, ##arg);}
#else
#define HID_ERROR(m, arg...)
#define HID_WARNING(m, arg...)
#define HID_DEBUG(m, arg...)
#define HID_ALL(m, arg...)
#endif


/***************************************************************************

  Constants and Types                               

 ***************************************************************************/
 
/*!
* \brief HOGP Device STATES - State Machine
*/
typedef enum{
  HIDD_INIT_ST = 1,
  HIDD_IDLE_ST,
  HIDD_CONN_ST,
  HIDD_W4_UNREG_ST
}hidd_state; // Initialization of states shall start from 1, as default value for state would be set to ZERO and which is used to check if any of the application has already Initialized the 
// HOGP module. If already been initialized by other app, then second app INIT would directly return without doing init again.
typedef uint8 tHIDD_STATE;


/*!
* \brief HOGP Device STATES EVENTS - State Machine
*/
typedef enum{
  HIDD_API_INIT_EVT = 0,
  HIDD_API_REGISTER_APP_EVT,
  HIDD_API_UNREGISTER_APP_EVT,
//  HIDD_API_CONNECT_EVT,
  HIDD_API_DISCONNECT_EVT,
  HIDD_API_SEND_REPORT_EVT,
  //    HIDD_API_REPORT_ERROR_EVT,
  //    HIDD_INT_APP_REGISTERED_EVT,
  //    HIDD_INT_APP_UNREGISTERED_EVT,
  HIDD_INT_OPEN_EVT,
  HIDD_INT_CLOSE_EVT,
  //    HIDD_INT_SUSPEND_EVT,        
  //    HIDD_INT_EXIT_SUSPEND_EVT,
  HIDD_IGNORE
}hidd_event;//tHIDD_EVT


/*****************************************************************************

  Structure Declarations.

 *****************************************************************************/
/*!
* \brief Parameters to State Event Handler Function
*/
typedef struct {
  appID        appid;
  tHIDD_EVT    event_id;
  void        *pdata;
  uint16    data_len;  
}thidd_state_event;


/*!
* \brief Connection Info
*/
typedef struct
{
  tconn_id        connID;
  tbt_device_id    devid;  //currently Device initiated connection is not supported. Hence not required
  tHIDD_STATE    cur_state;
}tHIDD_ConnContext;


/*!
* \brief HIDD Control Block 
*/
typedef struct
{
  tHIDD_CBACK            *p_cback[MAX_APP_ID]; ///< Array of Callback Functions per Application Id, Mapping is done by using Appid as Index of this Array
  tHIDD_ConnContext    connContext[MAX_APP_ID]; ///< Connection Context Array per Application Id, Mapping is done by using Appid as Index of this Array    
  tconn_id            connID_cccIndex_Map[MAX_CONN_PER_DEV]; ///< Each Connection Handle is Mapped to the Gatt Client Instance 
  //for Client Characteristic Configuration.

  // This is to check for the ccc Index. If multiple connections per app is not supported then the connContext can be used directly. This field is not needed.                                                                
  //    tHIDD_ConnContext    connContext[MAX_APP_ID][MAX_CONN_PER_APP];
  //    tHIDD_STATE            app_cur_state[MAX_APP_ID];
}tHIDD_CtrlBlk;

extern tHIDD_CtrlBlk hidd_ctrlblk;


/*****************************************************************************************************

  HIDD State Machine Function Declarations

 *****************************************************************************************************/

/*!
 * @brief  HOGP Device profile state events handler.
 *            This function Handle events of the state machine. The following state machine is implemented
 *            according to the above EVENT-STATE Mapping. Any Changes made in state 
 *            machine shall also be modified in above Mapping for better code readability.
 *
 * @param event Pointer to State-Event parameter structure
 *
 * @return Void Returns None
 */
void state_event_handler(thidd_state_event *evt);


#endif //#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)
#endif // _HID_MAIN_H 
