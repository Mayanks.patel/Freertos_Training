/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   bas_service.c
 * \brief  This file contains the implementation of the Battery Service Handling
 * \author Marvell Semiconductor
 */

#include "utils.h"
#include "service_conf.h"
#include "bas_service.h"
#include "main_gatt_api.h"
#include "memory_manager.h"
//#include "nvm_mgt.h"
#include "oss.h"


#define BAS_SERVICE_VERSION  "MRVL-LE-BAS-10.0.0.0"

/***********************************************************************   
  Sturcture & Function Declarations   
 ************************************************************************/
static tgatt_uuid16_service_record BAS_SrvcRecord;
static tgatt_uuid16_char_decl_record CharDecl_BALevel;
static tBAS_BAL_CharValDecl CharVal_BALevel;
static tBAS_BAL_CCC BALCCC;
static tgatts_cb_result bas_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data);
static void bas_load_client_context(tconn_id conn_id, tbt_device_id *p_peer_device_id);
static inline uint8 bas_fetch_cccid(tconn_id conn_id);


/***********************************************************************
  Global Variables & Data Sturctures    
 ************************************************************************/
static uint8 attr_val[MAX_SRVC_DATA] = {0};
static boolean bas_srvc_initialized = FALSE;
static tSRVC_ConnContext bas_cccIdx_connId[MAX_CONN_PER_DEV] = {{{{0}}}};


/* Primary service - BAS Service 
 ********** Start **************
 */
static tgatt_uuid16_service_record BAS_SrvcRecord =
{
  /* SERVICE DEFINITION */
  NULL,                                                                       ///< Link pointer for all_service_q 
  {&CharDecl_BALevel, GATT_PRIM_SERVICE_RECORD_UUID16, GATT_START_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  3,                                ///< Service End handle, set this value to total number of attributes associated with this service excluding service record itself 
  UUID_DECL_PRIMARY_SERVICE,        ///< 0x2800 for «Primary Service» 
  bas_serv_req_handler,             ///< Service Callback
  UUID_SERVICE_BATTERY,             ///< 0x180F - for Bluetooth BAS service 
};

static tgatt_uuid16_char_decl_record CharDecl_BALevel =
{      /* Characteristics Declaration */
  {&CharVal_BALevel, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                 ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ|GATT_CH_PROP_NOTIFY,    ///< Read & Notify 
  GATT_NEXT_HANDLE,                         ///< Set to Next attribute record
  GATT_UUID_BATTERY_LEVEL                   ///< UUID for Battery Level 
};

static tBAS_BAL_CharValDecl CharVal_BALevel =
{
  /* Characteristics Value Declaration */
  {
    {&BALCCC, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
    GATT_UUID_BATTERY_LEVEL,                                                 ///< UUID for Battery Level 
    GATT_PERM_READ_AUTH_SECURITY_LEVEL_WITH_ENC
  },
  50            ///< Initial Battery Level  TODO: Fetch the BAL value & Update this Value
};

static tBAS_BAL_CCC BALCCC =
{/* Need to Initialize all the Client Configuration Instances; Currently MAX_CONN_PER_DEV=1, hence initialized CCC value once to Zero*/
  /* Characteristics Value Declaration */
  {{NULL, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},                  ///< record header.next_ptr is set to address of next record of the service 
    UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION,                        ///< UUID for CCC 
    GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC|GATT_PERM_ENC_KEY_SIZE_16BYTES
  },
  {0x00}            ///< defualt value 
};

/* Primary service - BAS Service 
 ********** End **************
 */


/*!
 * @brief  Battery Service Init API for APP/Profile.
 *            This API Call Instantiates a Single Battery Service Instance
 *            NOTE: To Add Multiple Battery Service Instances, Need to create Copy File of this file and
 *                      Modify the names of APIs, Global Variables and Callback function names.
 *
 * @param Void Takes No Parameters
 *
 * @return Void Returns None
 */
void bas_serv_init(void)
{
  /* Add the Battery service record to GATT Only once, as bas_serv_init can be used to have only single instance for Battery Service*/
  if(!bas_srvc_initialized)
  {
    /* Create BAS Service */
    GATTS_Add_Service((void *) &BAS_SrvcRecord);
    bas_srvc_initialized = TRUE;
  }
  return;
}


/*!
 * @brief  Service Request Handler Registered with Local Gatt Server
 *            This Callback funtion handles all the Remote Gatt Client Requests & Events of this service
 *
 * @param conn_id Gatt Server Connection Id
 * @param req_type Gatt Server's request
 * @param p_req_data Request/Event Parameters
 *
 * @return Void Returns None
 */
static tgatts_cb_result bas_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data)
{
  tgatts_cb_result result; 
  static tgatts_rsp read_rsp;
  static tgatts_write_rsp write_rsp;
  read_rsp.p_val = attr_val;
  uint8 ccc_idx;
  uint8 *tmp_p_val_ptr;

  result.status = ATT_SUCCESS; /*Default return status value*/

  if(conn_id)
  {
    ccc_idx = bas_fetch_cccid(conn_id); //Get the ccc index from the conn_id
   
    #ifdef SRVC_DEBUG_ENABLED
    SRVC_DEBUG("bas_serv_req_handler: ConnID=%d, ccc_idx=%d",conn_id,ccc_idx);
    #endif
  }

  if(req_type == GATT_EVENT_CONNECTED)
  {
    /* Save the ConnID and corresponding DeviceID, if it was not stored earlier */
    bas_load_client_context(conn_id, (&((tgatt_conn_disc_param *)p_req_data)->device_id));
  }
  else if(req_type == GATT_EVENT_DISCONNECTED)
  {  
//    uint16 result;
    tBAS_nvm_data nvm_data;
    
    /* Store	the NV data into persistent storage, if this device id is in Paired device List */
    nvm_data.valid_data = TRUE;
    nvm_data.bal_nty_Ind_ccc = BALCCC.nty_Ind[ccc_idx];
#if 0  			
    /* Store	the NV data into persistent storage, if this device id is in Paired device List */
    if((result = NVM_mgt_write(bas_cccIdx_connId[ccc_idx].dev_id, BAS_SIGN, sizeof(tBAS_nvm_data), (void *)&nvm_data)) < 0)
    {
      #if ((defined SRVC_DEBUG_ENABLED) && (SRVC_DEBUG_ENABLED == TRUE))
        SRVC_DEBUG("%s: Client Configuration Context NOT SAVED!!, Reason=%d",__FUNCTION__,result);
      #endif
    }
#endif  			
    #if ((defined SRVC_DEBUG_ENABLED) && (SRVC_DEBUG_ENABLED == TRUE))
      SRVC_DEBUG("%s: Loaded NV data is :\nValidData = %d\nBALCCC = %d\n",__FUNCTION__,
                      nvm_data.valid_data,
                      BALCCC.nty_Ind[ccc_idx]);
    #endif

    /* Clean the Connection Context */
    BALCCC.nty_Ind[ccc_idx] = 0;
    bas_cccIdx_connId[ccc_idx].conn_id = NULL;
    //oss_memset(&(bas_cccIdx_connId[cccid].dev_id),0,sizeof(bas_cccIdx_connId[cccid].dev_id));

  }
  else if(req_type == GATT_EVENT_READ_REQ) 
  {
    if(((tgatts_req *)p_req_data)->handle == CharVal_BALevel.bal_CharValDecl.header.handle)
    {
      read_rsp.p_val[0] = CharVal_BALevel.BALVal;  //0x32; //TODO: Hardcoding value .. Need to get value dynamically regHID_Srvc.BAS_Srvc.ba_level.baLvlVal;        
      read_rsp.val_len = 0x01;
    }else if(((tgatts_req *)p_req_data)->handle == BALCCC.baLevel_ccc.header.handle)
    {
      tmp_p_val_ptr = read_rsp.p_val;
      UINT16_TO_LE_ENDIAN(BALCCC.nty_Ind[ccc_idx],tmp_p_val_ptr);
      read_rsp.val_len = 0x02;
    }
    else
    {
      result.status = ATT_INVALID_HANDLE;
    }

    read_rsp.rsp_of = GATT_EVENT_READ_REQ;
    read_rsp.handle = ((tgatts_req *)p_req_data)->handle ;

    GATTS_Send_Rsp(conn_id, result.status, (tgatts_rsp *)&read_rsp); 
  }
  else if(req_type == GATT_EVENT_WRITE_CMD || req_type == GATT_EVENT_WRITE_REQ)
  {
    if(((tgatts_req *)p_req_data)->handle == BALCCC.baLevel_ccc.header.handle)
    {
      BALCCC.nty_Ind[ccc_idx] = ((tgatts_req *)p_req_data)->p_value[0];
    }
    else
      result.status = ATT_INVALID_HANDLE;

    if(req_type == GATT_EVENT_WRITE_REQ)
    {
      write_rsp.rsp_of = GATT_EVENT_WRITE_REQ;
      write_rsp.handle = ((tgatts_req *)p_req_data)->handle ;
      GATTS_Send_Rsp(conn_id, result.status, (void *)&write_rsp); 
    }
  }

  return result;
}


/*!
 * @brief  Function API to send Notification to Gatt Client
 *            This funtion sends Notification to remote gatt client, if the remote gatt client has 
 *            configured/enabled this Characteristic Value Notifications
 *
 * @param conn_id Gatt Server Connection Id
 * @param val_len Characteristic Value length
 * @param pval Pointer to Characteristic Value
 *
 * @return Void Returns None
 */
void bas_srvc_send_notify(tconn_id conn_id, uint16 val_len, uint8 * pval)
{
  uint16 attr_handle = CharVal_BALevel.bal_CharValDecl.header.handle;
  uint8 ccc_idx;

  if(!conn_id || !pval)
    return;

  ccc_idx = bas_fetch_cccid(conn_id); //Get the ccc index from the conn_id

#ifdef SRVC_DEBUG_ENABLED
  SRVC_DEBUG("bas_srvc_send_notify: ConnID=%p, CCC_ID=%d, Nty_Ind=%d",conn_id,ccc_idx,BALCCC.nty_Ind[ccc_idx]);
#endif    

  if(BALCCC.nty_Ind[ccc_idx] & TO_NOTIFY)
  {
    /* Send Notification to Gatt Client */
#ifdef SRVC_DEBUG_ENABLED
    SRVC_DEBUG("BAS Notification Sent, ConnID=%p, AttrHdl=%d",conn_id,attr_handle);
#endif
    GATTS_Send_Handlevalue(conn_id, attr_handle, FALSE, val_len, pval);
  }
}


/*!
 * @brief  Function API that returns Service Handle Range
 *            This funtion is used by other services, to include this service. In order to include this 
 *            service, the including service shall have to update its included service handle range, for which
 *            this function api is used.
 *            Note: This api shall only be called after adding this service/service init.
 *
 * @param conn_id Gatt Server Connection Id
 *
 * @return thandle_range Returns Service Handle Range(Start and End Handle)
 */
thandle_range bas_service_to_be_included(void)
{
  thandle_range handle_range = {0};

  handle_range.s_handle = BAS_SrvcRecord.header.handle;
  handle_range.e_handle = BAS_SrvcRecord.end_handle;

  return handle_range;
}


/*!
 * @brief  Function to save Service Connection Context
 *            This funtion is to save Connection ID & corresponding Device ID. Also if present, 
 *            NVM data is loaded.
 *
 * @param conn_id Gatt Server Connection Id
 * @param p_peer_device_id Pointer to Remote Device Id
 *
 * @return Void Returns None
 */
static void bas_load_client_context(tconn_id conn_id, tbt_device_id *p_peer_device_id)
{
  uint8 ii;
  tBAS_nvm_data nvm_data;
  
  /* Now check for Max Connections */
  for(ii=0; !(bas_cccIdx_connId[ii].conn_id == NULL) && ii<MAX_CONN_PER_DEV ; ii++);
  
  if(ii<MAX_CONN_PER_DEV)
  {/* NOT exceeded MAX Number of Connections */

    /* Load the NV data if available for this device id */
//    if(( NVM_mgt_read(*p_peer_device_id, BAS_SIGN, sizeof(tBAS_nvm_data), (void *)&nvm_data) ) < 0)
      nvm_data.valid_data = FALSE;

    if(nvm_data.valid_data == TRUE)
      BALCCC.nty_Ind[ii] = nvm_data.bal_nty_Ind_ccc;

    #if ((defined SRVC_DEBUG_ENABLED) && (SRVC_DEBUG_ENABLED == TRUE))
      SRVC_DEBUG("%s: Stored NV data is :\nValidData = %d\nBALCCC = %d\n",__FUNCTION__,
      nvm_data.valid_data,
      nvm_data.bal_nty_Ind_ccc);
  
      SRVC_DEBUG("bas_load_client_context: Save the Connid=%p & BD_Addr= [%02X %02X %02X %02X %02X %02X]\n",
                 conn_id,(*p_peer_device_id).address[0],(*p_peer_device_id).address[1],(*p_peer_device_id).address[2],
                 (*p_peer_device_id).address[3],(*p_peer_device_id).address[4],(*p_peer_device_id).address[5]);
    #endif

    bas_cccIdx_connId[ii].conn_id = conn_id;
    bas_cccIdx_connId[ii].dev_id = *p_peer_device_id;
  }
  else
  {/* Exceeded MAX_CONN_PER_DEV, TODO: Disconnect the client */
    #ifdef SRVC_DEBUG_ENABLED
    SRVC_DEBUG("bas_load_client_context: Exceeded MAX_CONN_PER_DEV=%d, Hence Disconnecting the client \n",MAX_CONN_PER_DEV);
    #endif
  }
}


/*!
 * @brief  Function to Fetch Client Characteristic Configuration Index
 *            This Function is for Fetching the CCC index corresponding to Connection ID.
 *
 * @param conn_id Gatt Server Connection Id
 *
 * @return uint8 Returns CCC Index value for Particular Gatt connection Handle, which is mapped to its CCC.
 */
static inline uint8 bas_fetch_cccid(tconn_id conn_id)
{
  uint8 idx;

  for(idx=0; idx<MAX_CONN_PER_DEV && conn_id != bas_cccIdx_connId[idx].conn_id; idx++);

#ifdef SRVC_DEBUG_ENABLED
  if(idx >= MAX_CONN_PER_DEV)
    SRVC_DEBUG("bas_fetch_cccid: ConnID=%p, MAX_CONN_PER_DEV=%d, CCC_ID=%d\n",conn_id,MAX_CONN_PER_DEV,idx);
#endif

  return idx;

}

