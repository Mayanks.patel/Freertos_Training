/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   hidd_le.h
 * \brief  This file contains the data structures and action function declarations 
 *        that would be used by state machine based on events and api calls.
 * \author Marvell Semiconductor
 */


#ifndef _HIDD_LE_H
#define _HIDD_LE_H

#define HID_DEVICE_ENABLED TRUE

#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)

#include "mrvlstack_config.h"
#include "main_gatt_api.h"
#include "hidd_api.h"
#include "service_conf.h"


/***********************************************************************

  Macro Definitions & Data Sturctures

 ************************************************************************/
/*!
* \name HOGP Device REPORT ID Definitions
*/
//@{
#define HOGP_REPORT_ID_KB           0x01
#define HOGP_REPORT_ID_MOUSE        0x02
#define HOGP_REPORT_ID_BATTERY      0x13
#define HOGP_REPORT_ID_SCAN_REF     0x14
//@}

/*!
* \name HOGP Device REPORT TYPES
*/
//@{
#define HOGP_REPORT_TYPE_INPUT      0x01
#define HOGP_REPORT_TYPE_OUTPUT     0x02
#define HOGP_REPORT_TYPE_FEATURE    0x03
//@}

/*!
* \name HOGP Device PROTOCOL MODE
*/
//@{
#define HOGP_PROTOCOL_MODE_BOOT     0x00
#define HOGP_PROTOCOL_MODE_REPORT   0x01    ///< Default Value to be set is Report Mode.
//@}

#define HOGP_SRVC_ACT_IGNORE    0
#define HOGP_SRVC_ACT_RSP       1

#define TO_NOTIFY               (0x0001)
#define TO_INDICATE             (0x0002)

#define MAX_ATT_VALUE           10         ///< Maximum Gatt Server Attribute Value that will be READ from or WRITTEN into server DB except for REPORT MAP Value.


/*!
* \brief INCLUDED Services ID by HOGP Profile
*/
typedef enum{
  INCL_SERVICE_BAS_ID = 0,
  INCL_SERVICE_SCPS_ID,
}tHogp_external_service_id;//HIDS service includes Battery Service & Scan Parameter Service.

/*!
* \brief HID Protocol Mode Value
*/
typedef struct {
  tgatt_uuid16_charval_decl_record_std prtlMode_CharValDecl;  ///< Protocol Mode Characteristic Attribute Record
  uint8    PrtlMode_Val[MAX_CONN_PER_DEV];  ///< Protocol Mode Characteristic Value
}tHOGP_PRTLMode_CharValDecl;

/*!
* \brief Keyboard Input Report Client Characteristic Configuration Descriptor
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    kbin_ccc;  ///< Keyboard Input Report Client Characteristic Configuration Attribute Record
  uint16                             nty_Ind[MAX_CONN_PER_DEV];    ///< CCC Value, maintained per Connection/Client[0x01 - Notify; 0x02 - Indicate]
}tHOGP_KBIN_CCC;

/*!
* \brief Keyboard Input Report Reference Value
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    kbin_RptRef;  ///< Report Reference of Keyboard Input Report Attribute Record
  uint16                            RptRef_Val;  ///< Report Reference Value, Byte0 - Report ID, Byte1 - Report Type
}tHOGP_KBIN_RptRef;

/*!
* \brief Keyboard Output Report Reference Value
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    kbout_RptRef;  ///< Report Reference of Keyboard Output Report Attribute Record
  uint16                            RptRef_Val;  ///< Report Reference Value, Byte0 - Report ID, Byte1 - Report Type
}tHOGP_KBOUT_RptRef;

/*!
* \brief Keyboard Feature Report Value
*/
typedef struct {
  tgatt_uuid16_charval_decl_record_std kb_feature1;  ///< Keyboard Report Feature Attribute Record
  uint8                            kb_feature1_val;  ///< Keyboard Report Feature Value
}tHOGP_KB_FEAT1_Val;

/*!
* \brief Mouse Input Report Client Characteristic Configuration Descriptor
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    mouse_ccc;  ///< Mouse Input Report Client Characteristic Configuration Attribute Record
  uint16                             nty_Ind[MAX_CONN_PER_DEV];  ///< CCC Value, maintained per Connection/Client[0x01 - Notify; 0x02 - Indicate]
}tHOGP_MouseIN_CCC;

/*!
* \brief Mouse Input Report Reference Value
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    mouse_RptRef;  ///< Report Reference of Mouse Input Report Attribute Record
  uint16                            RptRef_Val;  ///< Report Reference Value, Byte0 - Report ID, Byte1 - Report Type
}tHOGP_MouseIN_RptRef;

/*!
* \brief Boot Keyboard Input Report Client Characteristic Configuration Descriptor
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    bt_kbin_ccc; ///< Boot Keyboard Input Report Client Characteristic Configuration Attribute Record
  uint16                             nty_Ind[MAX_CONN_PER_DEV]; ///< CCC Value, maintained per Connection/Client[0x01 - Notify; 0x02 - Indicate]
}tHOGP_BT_KBIN_CCC;

/*!
* \brief Boot Mouse Input Report Client Characteristic Configuration Descriptor
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    bt_mouse_ccc;  ///< Boot Mouse Input Report Client Characteristic Configuration Attribute Record
  uint16                             nty_Ind[MAX_CONN_PER_DEV];  ///< CCC Value, maintained per Connection/Client[0x01 - Notify; 0x02 - Indicate]
}tHOGP_BT_MouseIN_CCC;

/*!
* \brief External Report Reference Characteristic Descriptor
*/
typedef struct {
  tgatt_char_desc_CCC_record_std    Err;  ///< External Report Reference Attribute Record, specified in HID Report Map
  uint16                            Err_Val; ///< External Report Reference Value, holds one of Included service's Characteristic UUID, whose reports can be received.
}tHOGP_ReportMap_ERR;

/*!
* \brief HOGP Attribute Value
*/
typedef struct {
  uint16    len; ///< Attribute Value Length
  uint8    value[MAX_ATT_VALUE]; ///< This array should be of size MAXIMUM MTU SIZE
  uint8    *p_rptMap;            ///< For Report Map, just need to return the pointer as instead of copying the whole memory.
}tHOGP_AttrValue;

/*!
* \brief HOGP Device Control Point Characteristic Value
*/
typedef struct{
  tgatt_uuid16_charval_decl_record_std CtrlPt; ///< HID Control Point Characteristic Attribute Record
  uint8 CtrlPt_Val;  ///< HID Control Point Characteristic Value
}tHOGP_CtrlPoint;

/*!
* \brief HOGP Device Keyboard Output Report Characteristic Value
*/
typedef struct{
  tgatt_uuid16_charval_decl_record_std KBOut; ///< HID Keyboard Output Report Characteristic Attribute Record
  uint8 KBOut_Val; ///< HID Keyboard Output Report Characteristic Value
}tHOGP_KBOut;

/*!
* \brief HOGP Device Data to be saved to NVM(Non Volatile Memory)
*/
typedef struct {
  boolean valid_data;         ///< Set TRUE while storing the data, so that while loading only valid data can be loaded, else dont load.
  uint8  PrtlMode_Val;        ///< Configured Protocol Mode Value
  uint16 kbin_nty_Ind_ccc;    ///< Configured CCC Value for Keyboard Input Report
  uint16 mouse_nty_Ind_ccc;   ///< Configured CCC Value for Mouse Input Report
  uint16 bt_kbin_nty_Ind_ccc; ///< Configured CCC Value for Boot Keyboard Input Report
  uint16 bt_mouse_nty_Ind_ccc;///< Configured CCC Value for Boot Mouse Input Report  
}tHIDS_nvm_data;


/***********************************************************************

  Function Declarations

 ************************************************************************/
appID hogp_reg_app_cb(tHIDD_CBACK *p_appCback);
void hogp_reg_service_act(void);
void hogp_reg_app_act(appID appid);
void hogp_dereg_app_act(appID *p_appid);
//void hogp_conn_act(tConn_params *connParam);
void hogp_disconn_act(tDisConn_params *disConnParam);
void hogp_send_report_act(tSendReport_params* sendReport_params);
void hogp_open_act(tconn_id connHdl, appID appid);
void hogp_close_act(tconn_id connHdl);
tgatts_cb_result hidd_gatts_callback(tconn_id connid, tgatt_event_ID req_type , tgatts_event_param *req_data);
void hogp_service_include(tHogp_external_service_id ext_service_id, thandle_range hndl_range);

#endif //#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)
#endif //_HIDD_LE_H
