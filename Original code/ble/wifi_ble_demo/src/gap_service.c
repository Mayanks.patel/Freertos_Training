/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   gap_service.c
 * \brief  This file contains the implementation of the GAP Service Handling
 * \author Marvell Semiconductor
 *         
 */

#include "mrvlstack_config.h"
#include "utils.h"
#include "main_gatt_api.h"
#include "gap_service.h"
#include "oss.h"
#include <string.h>

#if defined(GATT_SERVER_SUPPORT) && (GATT_SERVER_SUPPORT == ENABLED)  

#define GAP_SERVICE_VERSION "MRVL-LE-GAPS-40_0_0_0"

#define GATT_DEVICE_NAME "MRVL SDK"

/* Defualt PPCP value */
#define CONN_INTER_MIN  0x08,0x00
#define CONN_INTER_MAX  0x18, 0x00
#define CONN_SLAVE_LATENCY 0x14, 0x00
#define CONN_SVN_TOUT_MUL 0xF4,0x01

#define PPCP_VAL_LEN 8
#define APPEARANCE_VAL_LEN 0x02

uint8 name[] = GATT_DEVICE_NAME;           /* ToDo : Device name should be fetched using GAP API */
uint8 PPCP_value[PPCP_VAL_LEN] = {CONN_INTER_MIN, CONN_INTER_MAX, CONN_SLAVE_LATENCY, CONN_SVN_TOUT_MUL};
uint8 appearance_val[APPEARANCE_VAL_LEN] = {0xC0,0x03};

static tgatts_rsp read_rsp;
static tgatts_write_rsp write_rsp;

static boolean gap_srvc_initialized = FALSE;


static tgatt_uuid16_char_decl_record CharDecl_Devicename;
static tgatt_uuid16_charval_decl_record_std CharVal_Devicename;
static tgatt_uuid16_char_decl_record CharDecl_PPCP;
static tgatt_uuid16_charval_decl_record_std CharVal_PPCP;
static tgatt_uuid16_char_decl_record CharDecl_Appearance;
static tgatt_uuid16_charval_decl_record_std CharVal_Appearance;

static tgatts_cb_result gap_srvc_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data);


/* Primary service - GAP Service 
 ********** Start **************
 */
tgatt_uuid16_service_record GAPServiceRecord =
{
  /* SERVICE DEFINITION */
  NULL,                                                   ///< Link pointer for all_service_q 
  {&CharDecl_Devicename, GATT_PRIM_SERVICE_RECORD_UUID16, GATT_START_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  6,                                                      ///< Service End handle, set this value to total number of attributes associated with this service excluding service record itself 
  UUID_DECL_PRIMARY_SERVICE,                              ///< 0x2800 for «Primary Service» 
  gap_srvc_req_handler,                                   ///< Service Callback
  UUID_SERVICE_GAP,                                       ///< 0x1800 - for Bluetooth GAP service 
};

static tgatt_uuid16_char_decl_record CharDecl_Devicename =
{
  /* Characteristics Declaration */
  {&CharVal_Devicename, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                               ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                                      ///< Read 
  GATT_NEXT_HANDLE,                                       ///< It will be set to handle of CharVal_BatteryLevel attribute record
  UUID_CIC_DEVICE_NAME                                    ///< UUID for Device Name 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Devicename =
{
  /* Characteristics Value Declaration */
  {&CharDecl_PPCP, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},     ///< record header.next_ptr is set to address of next record of the service 
  UUID_CIC_DEVICE_NAME,                                                        ///< UUID for Device Name
  GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL
};

static tgatt_uuid16_char_decl_record CharDecl_PPCP =
{
  /* Characteristics Declaration */
  {&CharVal_PPCP, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},            ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                               ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                                      ///< Read
  GATT_NEXT_HANDLE,                                       ///< It will be set to handle of CharVal_BatteryLevel attribute record
  UUID_CIC_PPCP                                           ///< UUID for PPCP 
};

static tgatt_uuid16_charval_decl_record_std CharVal_PPCP =
{
  /* Characteristics Value Declaration */
  {&CharDecl_Appearance, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
  UUID_CIC_PPCP,                                                       ///< UUID for PPCP
  GATT_PERM_READ_NO_SECURITY_LEVEL
};

static tgatt_uuid16_char_decl_record CharDecl_Appearance=
{
  /* Characteristics Declaration */
  {&CharVal_Appearance, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},            ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,                               ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_READ,                                      ///< Read
  GATT_NEXT_HANDLE,                                       ///< It will be set to handle of CharVal_BatteryLevel attribute record
  UUID_CIC_APPEARANCE                                     ///< UUID for Appearance 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Appearance=
{
  /* Characteristics Value Declaration */
  {NULL, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
  UUID_CIC_APPEARANCE,                                                 ///< UUID for Appearance
  GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL
};


/* Primary service - GAP Service 
 ********** End **************
 */


/*!
 * @brief  GAP Service Init API for APP/Profile.
 *            This API Call Instantiates a Single GAP Service Instance
 *
 * @param Void Takes No Parameters
 *
 * @return Void Returns None
 */
void gap_serv_init(void)
{
  /* Add the GAP service record to GATT Only once, as there can be only single instance for Generic Access Service*/
  if(!gap_srvc_initialized)
  {
    /* Create GAP Service */
    GATTS_Add_Service((tgatt_service_record_common *) &GAPServiceRecord);
    gap_srvc_initialized = TRUE;
  }
  return;
}

/*!
 * @brief  Internal Function to save the write request values from remote Gatt Client
 *
 * @param write_req Gatt Client's Write request parameters
 * @param p_status Pointer to Status of this function call
 *
 * @return Void Returns None
 */
void gap_srvc_write_attr_value(tgatts_req *write_req, tgatt_status *p_status)
{
    tgatt_status st = ATT_ATTR_NOT_FOUND;
    
    if (write_req->handle == CharVal_Devicename.header.handle)
    {
        uint8 temp_name_size = 0;
        st = ATT_SUCCESS;

        if(write_req->val_len <= strlen((const char *)name) )
          temp_name_size = write_req->val_len;
        else
          temp_name_size = strlen((const char *)name);
        oss_memcopy(name,write_req->p_value,temp_name_size);
		name[temp_name_size] = '\0';
    }
    else if (write_req->handle == CharVal_Appearance.header.handle)
    {
        st = ATT_SUCCESS;
        oss_memcopy(&appearance_val,write_req->p_value,write_req->val_len);
    }
    
    *p_status = st;
    
}

/*!
 * @brief  API Function to read the updated Local GAP Service Characteristic Values
 *            Mainly Local Device Name & Appearance, which can be updated by Remote Client.
 *
 * @param p_char_value_param Pointer to Gap Local Device Characteristic Values. Updated values are copied to this memory, which can be used by applications.
 *
 * @return Void Returns None
 */
void Gap_Serv_Read_Local_Char_Api(tgap_serv_read_local_char_params *p_char_value_param)
{
  uint8 *p_appearance_val = appearance_val;

  p_char_value_param->p_local_dev_name = name;
  p_char_value_param->local_dev_name_len = strlen((const char *)name); //Subtract the NULL Character
  LE_ENDIAN_TO_UINT16(p_appearance_val,(p_char_value_param->local_device_appearance));

  return;
}

/*!
 * @brief  Service Request Handler Registered with Local Gatt Server
 *            This Callback funtion handles all the Remote Gatt Client Requests & Events of this service
 *
 * @param conn_id Gatt Server Connection Id
 * @param req_type Gatt Server's request
 * @param p_req_data Request/Event Parameters
 *
 * @return Void Returns None
 */
static tgatts_cb_result gap_srvc_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data)
{
  tgatts_cb_result result;
  
  result.status = ATT_SUCCESS;

  if(req_type == GATT_EVENT_READ_REQ) 
  {
    if(p_req_data->request_param.handle == CharVal_Devicename.header.handle)
    {
      read_rsp.val_len = sizeof(GATT_DEVICE_NAME);
      read_rsp.p_val = name;
    }else if(p_req_data->request_param.handle == CharVal_PPCP.header.handle)
    {
      read_rsp.val_len = PPCP_VAL_LEN;
      read_rsp.p_val = PPCP_value;
    }
    else if(p_req_data->request_param.handle == CharVal_Appearance.header.handle)
    {
      read_rsp.val_len = APPEARANCE_VAL_LEN;
      read_rsp.p_val = appearance_val;
    }
    else
    {
      result.status = ATT_ATTR_NOT_FOUND;
    }
    
    read_rsp.rsp_of = GATT_EVENT_READ_REQ;
    read_rsp.handle = p_req_data->request_param.handle;
    
    GATTS_Send_Rsp(conn_id, result.status, (tgatts_rsp *)&read_rsp); 
    
  }
  else if ( ( req_type == GATT_EVENT_WRITE_REQ ) || ( req_type == GATT_EVENT_WRITE_CMD ))
  {
      tgatts_req *req_data = (tgatts_req *)p_req_data;
      
      write_rsp.rsp_of = req_type;
      
      write_rsp.handle = req_data->handle;
      
      gap_srvc_write_attr_value(req_data, &result.status);
      
      if(req_type == GATT_EVENT_WRITE_REQ)
          GATTS_Send_Rsp(conn_id, result.status, (void *)&write_rsp); 
  }

  return result;
  
}

#endif
