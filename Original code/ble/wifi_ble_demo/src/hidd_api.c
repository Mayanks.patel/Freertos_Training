/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   hidd_api.c
 * \brief  This file contains the handling of API calls from the Application & calls
 *        the appropriate action function based on states event handling. 
 * \author Marvell Semiconductor
 */

#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)

#include "hidd_api.h"
#include "hidd_main.h"
#include "hidd_le.h"
#include "oss.h"


/***********************************************************************

  Api Function Definitions

 ************************************************************************/

/*!
 * @brief  Function API for Application to Register with HOGP.
 *
 * @param app_reg_params Application Callback function pointer
 *
 * @return tHIDD_AppInfo Returns Appid and this api status.
 */
tHIDD_AppInfo Hidd_app_reg_api(tHIDD_CBACK *app_reg_params)
{
  uint8 ii=0;
  tHIDD_AppInfo ret;
  thidd_state_event evt;

  if((ret.appid = hogp_reg_app_cb(app_reg_params)) == APP_ID_ERR)
  {
    ret.status = BT_RESULT_FAIL;
    return ret;
  }

  if(hidd_ctrlblk.connContext[ret.appid].cur_state == 0)//Check if this is the first Init for HOGP
  {
    for(ii=0; ii<MAX_APP_ID; ii++)
      hidd_ctrlblk.connContext[ii].cur_state = HIDD_INIT_ST; //Since INIT will be done only once, update all the APP connContext's current state to HIDD_INIT_ST.

    // Initialize Profile Data

    //Register Services with Gatt
    evt.appid    = ret.appid;
    evt.event_id = HIDD_API_INIT_EVT;
    evt.pdata    = NULL;
    evt.data_len = 0;

    state_event_handler(&evt);
    ret.status = BT_RESULT_STARTED;
  }
  else if(hidd_ctrlblk.connContext[ret.appid].cur_state == HIDD_INIT_ST)
  {
    evt.appid    = ret.appid;
    evt.event_id = HIDD_API_REGISTER_APP_EVT;
    evt.pdata    = NULL;
    evt.data_len = 0;

    state_event_handler(&evt);
    ret.status = BT_RESULT_STARTED;  
  }
  else
  {
    ret.status = BT_RESULT_FAIL;
#if ((defined HID_DEVICE_DEBUG_ENABLED) && (HID_DEVICE_DEBUG_ENABLED == TRUE))
    HID_DEBUG("Something wrong with STATE handling!! STATE=%d, AppID=%d", hidd_ctrlblk.connContext[ret.appid].cur_state, ret.appid);
#endif  
  }

  return ret;    
}


/*!
 * @brief  Function API for Application to DeRegister with HOGP.
 *
 * @param app_dereg_params Pointer to Application Id returned while app registration
 *
 * @return tHIDD_STATUS Returns api status.
 */
tHIDD_STATUS Hidd_app_dereg_api(appID *app_dereg_params)
{
  thidd_state_event evt;

  evt.appid      = *app_dereg_params;
  evt.event_id   = HIDD_API_UNREGISTER_APP_EVT;
  evt.pdata      = app_dereg_params;
  evt.data_len   = sizeof(appID);

  state_event_handler(&evt);
  return BT_RESULT_STARTED;   
}

#if 0
/*******************************************************************************
 **
 ** Function        Hidd_connect_api
 **
 ** Description     Connect API for APP.
 **
 ** Parameters:     Takes tConn_params as input argument and Return tHIDD_STATUS.
 **
 *******************************************************************************/
tHIDD_STATUS Hidd_connect_api(tConn_params *conn_params)
{
  thidd_state_event evt;

  evt.appid    = conn_params->appid;
  evt.event_id = HIDD_API_CONNECT_EVT;
  evt.pdata    = conn_params;
  evt.data_len = sizeof(tConn_params);

  state_event_handler(&evt);
  // ** Need to Check operation **
  return BT_RESULT_STARTED;
}
#endif

/*!
 * @brief  Function API for Disconnect for given connection id
 *
 * @param disconn_params Pointer to Disconnection parameters
 *
 * @return tHIDD_STATUS Returns api status.
 */
tHIDD_STATUS Hidd_disconnect_api(tDisConn_params *disconn_params)
{
  thidd_state_event evt;

  evt.appid    = disconn_params->appid;
  evt.event_id = HIDD_API_DISCONNECT_EVT;
  evt.pdata    = disconn_params;
  evt.data_len = sizeof(tDisConn_params);

  state_event_handler(&evt);
  return BT_RESULT_STARTED;    
}


/*!
 * @brief  Function API for  Send Report API
 *
 * @param sendReport_params Pointer to Report parameters to be sent to remote
 *
 * @return tHIDD_STATUS Returns api status.
 */
tHIDD_STATUS Hidd_send_report_api(tSendReport_params *sendReport_params)
{
  thidd_state_event evt;

  evt.appid    = sendReport_params->appid;
  evt.event_id = HIDD_API_SEND_REPORT_EVT;
  evt.pdata    = sendReport_params;
  evt.data_len = sizeof(tSendReport_params);

  state_event_handler(&evt);
  return BT_RESULT_STARTED;  
}

#endif //#if defined(HID_DEVICE_ENABLED) && (HID_DEVICE_ENABLED == TRUE)

