
/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */


#include <wmstdio.h>
#include <wm_os.h>
#include <mdev.h>
#include <mdev_uart.h>
#include <mdev_sdio.h>
#include <cli.h>
#include <wlan.h>
#include <partition.h>
#include <flash.h>
#include <wmstdio.h>
#include <stdlib.h>

#include "mrvlstack_config.h"
#include "main_gap_api.h"
#include "memory_manager.h"
#include "sample_ble_poolconfig.h"

#include "hidd_api.h"
#include "hidd_le.h"
#include "timer.h"
#include "gap_service.h"
#include "dis_service.h"
#include "bas_service.h"
#include "scps_service.h"


#include "main_gap_api.h"
#include "main_gap_declares.h"
#include "main_gatt_api.h"
#include "service_conf.h"
#include "bas_service.h"
#include "dis_service.h"
#include "scps_service.h"
#include "gap_service.h"
#include "timer.h"

#include "hidd_api.h"
#include "hidd_le.h"

#if ((defined MRVL_STACK_DEBUG_ENABLED) && (MRVL_STACK_DEBUG_ENABLED == TRUE))
/* Define DEBUG Level */
extern uint8 SRVC_MSG_level = ALL;
uint32 HID_MSG_level = ALL; /* For Debug Purpose */
#endif

/// MAX ATT MTU size. Application can choose one of the block pool to configure MAX MTU Size.
#define ATT_MAX_MTU_SIZE     (POOL_ID0_BUF1_SZ - L2CAP_MIN_OFFSET - sizeof(Msg_Hdr))

#define HOG_MOUSE_CIRCLE_RADIUS 20 // 20 Units
#define HOG_MOUSE_CIRCLE_NUM_OF_LOOP 4

/* Memory chunk given to BLE Stack for buffer management */
__attribute__ ((aligned(4))) static uint8  stackBufferPool[POOL_ID0_SZ + POOL_ID0_OVERHEAD];
static uint32 stackTrackBuf[POOL_ID0_BUF0_CNT + POOL_ID0_BUF1_CNT + POOL_ID0_BUF2_CNT + POOL_ID0_BUF3_CNT];

/* Reconnection Structure */
typedef struct
{
  tbt_device_id        reConnDevId;                            //Last connected Device is used for Re-connection
  boolean              is_connecting;                          // TRUE: ReConnection Already Initiated  FALSE: ReConnection Not initiated
  Timer                *dirAdvTimerId;                         // Timer for DIRECTED HIGH/LOW Duty Cycle Advertising
}tReconn_Info;

typedef struct 
{
  
  tbt_device_id  device_id;
  
  tbt_oob_data_flags oob_data_flags;
  
  uint8 oob_tmpk[MAX_KEYS_SIZE];
  
  struct {
      uint8 confirm[MAX_KEYS_SIZE];
      uint8 rand[MAX_KEYS_SIZE];
  } sc_oob_data;
  
} tapp_oob_data;

static void hogp_cback(tHIDD_EVT event, tHIDD_CBACK_EVT *p_data);
void draw_mouse_circle(void);

void sendMINRpt(int x, int y);


appID appid;
tSendReport_params sendReport_params;
tconn_id connID=NULL;
tReconn_Info reconn_devid = {{{0}}};
uint8 MINrpt[4] = {0};

tbt_discovery_params disc_params;
t_device_config_data config;

tbt_sec_events_resp_parms resp_params;

static tconn_id cl_conn_id =0;
boolean authorization_flag = TRUE;
static uint8 ccc_index;
#define MAX_NUM_CCC 10
static uint16 ccc_handle[MAX_NUM_CCC]; /* To store all found Characteristics CCCs */

static void gatt_services_prof_init(void)
{    
  tHIDD_AppInfo appinfo;

  /* GAP Service Init */
  gap_serv_init();

  /* Device Information Service Init */
  dis_serv_init();

  /* Battery Service Init */
  bas_serv_init();

  /* Scan Parameters Service Init */
  scps_serv_init();

  /* INIT & Register with HOGP Service */
  appinfo = Hidd_app_reg_api(hogp_cback);
  appid = appinfo.appid;


  return;
}

static void hogp_cback(tHIDD_EVT event, tHIDD_CBACK_EVT *p_data)
{
  //wmprintf("hogp_app_cback: event = %d""\r\n", event);
  switch(event)
  {
    case HIDD_INT_APP_REGISTERED_EVT:
    {
      /* Include External Services into Profile's Services */
      hogp_service_include(INCL_SERVICE_BAS_ID, bas_service_to_be_included());
      hogp_service_include(INCL_SERVICE_SCPS_ID, scps_service_to_be_included());   
    }
    break;

    case HIDD_INT_APP_UNREGISTERED_EVT:
      break;

    case HIDD_INT_APP_OPEN_EVT:
      connID = ((tHIDD_ConnInfo *)p_data)->connID;

      //Todo: Save the Last connected BDADDR for Re-Connection
      reconn_devid.reConnDevId = ((tHIDD_ConnInfo *)p_data)->dev_id;

      if(reconn_devid.is_connecting == TRUE)    //This Flag was set TRUE during the start of DIR_ADV.
      {
        reconn_devid.is_connecting = FALSE;
        //Stop DIR_ADV TIMER
        if(reconn_devid.dirAdvTimerId)
        {
          wmprintf("[HOGP TEST APP]Stop Directed ADV Timer, Timer ID = %p""\r\n",reconn_devid.dirAdvTimerId);

          stop_timer(reconn_devid.dirAdvTimerId);
          reconn_devid.dirAdvTimerId = NULL;
        }
      }            

      break;

    case HIDD_INT_APP_CLOSE_EVT:
      wmprintf("[HOGP TEST APP][CLOSE_EVT]Stop Idle Timer, ID ""\r\n"); 
      connID = NULL;
      
      break;        
    default:
      break;
  }
  return;
}

static void ble_app_cmd_menu(int argc, char *argv[])
{

   int opt = 0;

   if (argc == 2) {
      opt = atoi(argv[1]);
   } else {
	wmprintf("Please enter valid option \r\n");
	wmprintf("Usage: ble-opt <number>\r\n");
	wmprintf(" where number = 1 - Start advertising(broadcaster role)\r\n");
	wmprintf("       number = 2 - Stop advertising\r\n");
	wmprintf("       number = 3 - Start Discovery(observer role)\r\n");
        wmprintf(" 	 number = 4 - Send Mouse reports(broadcaster role)\r\n"); 
      return;
   }

  switch(opt)
  {

  case 1:
     {
        
        //Enable Advertising
        uint8 adv_data[] =  { 0x02,0x01,0x00, 0x02,0x0A,0xFF,0x07,0x03,0x00,0x18,0x01,0x18,0x12,0x18, 0x02, 0x1C, 0x01};
        uint8 scan_data[] = { 0x03, 0x03, 0x01, 0x18, 0x02, 0x1C, 0x01, 0x0b,0x09,'M','R','V','L','-','W','M','S','D','K'};
        
        uint8 buffer[64];
        
       
        tbt_advertising_data_params *gap_adv_data = (tbt_advertising_data_params *)buffer ;
        
        tble_advertising_params default_advt_params = { GAP_BLE_DEFAULT_ADVERTISING_INT_MIN,GAP_BLE_DEFAULT_ADVERTISING_INT_MAX,
          GAP_BLE_DEFAULT_ADVERTISING_TYPE,GAP_BLE_DEFAULT_OWN_ADDRESS_TYPE,0,
          {0,0,0,0,0,0},GAP_BLE_DEFAULT_ADVERTISING_CHANNEL_MAP,GAP_BLE_DEFAULT_ADVERTISING_FILTER_POLICY };

        wmprintf(" Start Advertising\r\n");         
       
        gap_adv_data->length = sizeof(adv_data);
        
        gap_adv_data->scan_rsp_data = FALSE;
         
        memcpy(gap_adv_data->data, adv_data, sizeof(adv_data));
        
        config.type = BT_CONFIG_T_ADVERTISING_DATA;
        
        config.len = sizeof(tbt_advertising_data_params) + gap_adv_data->length ;
        
        config.p.adv_data = *gap_adv_data;
        
        GAP_Config_Device(&config);
              
        gap_adv_data->length = sizeof( scan_data);
        
        memcpy(gap_adv_data->data, scan_data, sizeof( scan_data));
        
        gap_adv_data->scan_rsp_data = TRUE;
        
        config.type = BT_CONFIG_T_ADVERTISING_DATA;
        
        config.len = sizeof(tbt_advertising_data_params) + gap_adv_data->length ;
        
        config.p.adv_data = *gap_adv_data;
        
        GAP_Config_Device(&config);
        
        config.len = sizeof(tble_advertising_params);
        
        config.type = BT_CONFIG_T_ADVERTISING_PARAMS;
        
        //default_advt_params.interval_max = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MAX;
        
        //default_advt_params.interval_min = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MIN;
        
        config.p.adv_params= default_advt_params;
        
        GAP_Config_Device(&config);
        
        config.len = 0x02;
        
        config.type = BT_CONFIG_T_BLE_DEVICE_MODE;
        
        //config.p.mode = GAP_BLE_GENERAL_DISCOVERABLE | GAP_BLE_NON_CONNECTABLE_BROADCASTING;
        
        config.p.mode = GAP_BLE_GENERAL_DISCOVERABLE | GAP_BLE_CONNECTABLE;
        
        GAP_Config_Device(&config);
        
        //Enable Advertising~~   
     }
     break;
  case 2:
     {
        
        //Disable Advertising     
     
        tble_advertising_params default_advt_params = { GAP_BLE_DEFAULT_ADVERTISING_INT_MIN,GAP_BLE_DEFAULT_ADVERTISING_INT_MAX,
          GAP_BLE_DEFAULT_ADVERTISING_TYPE,GAP_BLE_DEFAULT_OWN_ADDRESS_TYPE,0,
          {0,0,0,0,0,0},GAP_BLE_DEFAULT_ADVERTISING_CHANNEL_MAP,GAP_BLE_DEFAULT_ADVERTISING_FILTER_POLICY };

        wmprintf(" Stop Advertising\r\n");     
        
        config.len = sizeof(tble_advertising_params);
        config.type = BT_CONFIG_T_ADVERTISING_PARAMS;

        default_advt_params.interval_max = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MAX;
        default_advt_params.interval_min = GAP_BLE_DEFAULT_NC_ADVERTISING_INT_MIN;
       
        config.p.adv_params= default_advt_params;
  
        GAP_Config_Device(&config);
  
        config.len = 0x02;
  
        config.type = BT_CONFIG_T_BLE_DEVICE_MODE;
        config.p.mode = GAP_BLE_NON_DISCOVERABLE | GAP_BLE_NON_CONNECTABLE;
  
  
        GAP_Config_Device(&config);
     }
     break;
  case 3:
     {
        wmprintf(" Start Discovery \r\n");     
        
        disc_params.ble_scan_params.duplicate_filters  = 0x01;

        disc_params.ble_scan_params.scan_address_type = 0x00;

        disc_params.ble_scan_params.scan_interval = 18;

        disc_params.ble_scan_params.scan_window = 18;

        disc_params.ble_scan_params.scan_type = 0x01;

        disc_params.ble_scan_params.scan_filter_policy = 0x00;

        disc_params.discovery_duration = 10000;

        disc_params.discovery_mode = 0x04;

        GAP_Start_Discovery(&disc_params);
     }

     break;
  case 4:
	// For mouse
        if(!reconn_devid.is_connecting && connID == NULL )
        {
          wmprintf("No Connection Available""\r\n");

          reconn_devid.is_connecting = TRUE;
        }
	else
		draw_mouse_circle();

        break;


  default:
     break;
  }
   
}

static void utils_skip_delimits(uint8 **ptr, uint8 delimits)
{
  
  while(**ptr == delimits)(*ptr)++;
  
}

static void utils_get_device_addr(uint8** params, tbt_device_id *device_id)
{
  
  uint8 *endptr;
  uint8 i;
  
  utils_skip_delimits(params,' ');
  
  for(i=0;i<BD_ADDR_SIZE;i++)
  {
    device_id->address[i] = strtol(*params,&endptr,16);
    *params = endptr + 1;
    
  }
  
}

static void ble_create_bond(int argc, char *argv[])
{


    tbt_bonding_params bonding_params;
    tbt_remote_device remote_dev;

  if (argc == 3) {
     utils_get_device_addr(&argv[1], &remote_dev.device_id);
     remote_dev.device_id.device_type = BT_DEVICE_TYPE_BLE;
     remote_dev.device_id.address_type = atoi(argv[2]);
     
     bonding_params.device_id = remote_dev.device_id;
     bonding_params.add_remove = TRUE;
     bonding_params.bond_mode = BT_LE_GENERAL_BONDING_MODE;
     GAP_Create_Bond(&bonding_params);
  }
  else {
      wmprintf("Usage: ble-create-bond <BLE device address> <adddress type>\r\n");
      wmprintf("Take <BLE device address> string from Discovery result, eg. AA:BB:CC:DD:EE:FF\r\n");
      wmprintf("<adddress type>  Set 0 for Public, 1 for random\r\n");
      
      return;
  }
  
}


void ble_gattc_handler(tconn_id conn_id, tgatt_event_ID event_type , tgattc_event_param *p_req_param)
{


    tgattc_req_param req_param;
    
    req_param.disc_param.s_handle = 1;
    req_param.disc_param.e_handle = 0xFFFF;
   
    switch(event_type)
    {
        case GATT_EVENT_CONNECTED:
        {
            wmprintf("Connected with GATT Server \r\n");

            //memset(ccc_handle, 0, MAX_NUM_CCC*(sizeof(uint16)));
            
            cl_conn_id = conn_id;
            GATTC_Submit_Req(cl_conn_id, GATTC_REQ_DISCOVERY, GATTC_DISC_PRI_SRV_ALL, &req_param);
             wmprintf("---Primary Service discovery Started---\r\n\n\n");
        }
        break;

        case GATT_EVENT_DISCONNECTED:
            wmprintf("Disconnected with GATT server \r\n");
            cl_conn_id = 0;
            break;
 
        case GATTC_EVENT_DIS_RSP:
        {
            switch(p_req_param->rsp_param.sub_type)
            {
                int i = 0;
                case GATTC_DISC_PRI_SRV_ALL:
                    wmprintf("Discover Primary Service Response: \r\n");

                    if(p_req_param->rsp_param.u.disc_rsp_param.isComplete) {
                        wmprintf("---Primary Service discovery Completed---\r\n\n\n");
                        
                        /* Go for included service discovery */ 
                        GATTC_Submit_Req(cl_conn_id, GATTC_REQ_DISCOVERY, GATTC_DISC_INC_SRV, &req_param);
                        wmprintf("---Included Service discovery Started---\r\n\n\n");
                    } else {
                        /* Print the response */
                        for(i = 0; i < p_req_param->rsp_param.u.disc_rsp_param.numberOfPairs ; i++) {
                            wmprintf("Service UUID : 0x%x\r\n", ((p_req_param->rsp_param.u.disc_rsp_param.u.pri_srv_pair[i].uuid.len) == 2) ?
                                                (p_req_param->rsp_param.u.disc_rsp_param.u.pri_srv_pair[i].uuid.u.uuid16): 0x0000);
                            wmprintf("Start Handle : 0x%x\r\n", p_req_param->rsp_param.u.disc_rsp_param.u.pri_srv_pair[i].startHandle);
                            wmprintf("End Handle: 0x%x \r\n\n", p_req_param->rsp_param.u.disc_rsp_param.u.pri_srv_pair[i].endHandle);
                        }
                    }
                break;
                case GATTC_DISC_INC_SRV:
                {
                    wmprintf("Discover Included Service Response: \r\n");
                    if(p_req_param->rsp_param.u.disc_rsp_param.isComplete) {
                    
                        wmprintf("---Included Service discovery Completed---\r\n\n\n");
                        
                        /* Go for characteristics discovery */ 
                        GATTC_Submit_Req(conn_id, GATTC_REQ_DISCOVERY, GATTC_DISC_CHAR, &req_param);
                        wmprintf("---Characteristics discovery Started---\r\n\n\n");
                    } else {
                        /* Print the response */
                        for(i = 0; i < p_req_param->rsp_param.u.disc_rsp_param.numberOfPairs ; i++) {
                            wmprintf("Record Handle : 0x%x\r\n", p_req_param->rsp_param.u.disc_rsp_param.u.included_srv_pair[i].handle);
                            wmprintf("includedServiceHandle : 0x%x\r\n", 
                                        p_req_param->rsp_param.u.disc_rsp_param.u.included_srv_pair[i].includedServiceHandle);
                            wmprintf("End Handle: 0x%x\r\n", p_req_param->rsp_param.u.disc_rsp_param.u.included_srv_pair[i].endHandle);
                            wmprintf("Service UUID : 0x%x\r\n\n", ((p_req_param->rsp_param.u.disc_rsp_param.u.included_srv_pair[i].serviceUUID.len) ==
                                         2) ?(p_req_param->rsp_param.u.disc_rsp_param.u.included_srv_pair[i].serviceUUID.u.uuid16): 0x0000);
                        }
                    }
                }
                break;
                case GATTC_DISC_CHAR:
                {
                    wmprintf("Discover Characteristic Response: \r\n");
                    if(p_req_param->rsp_param.u.disc_rsp_param.isComplete) {
                         wmprintf("---Characteristics discovery Completed---\r\n\n\n");
                         

                         /* Go for characteristics descriptors discovery */ 
                         ccc_index = 0;
                         GATTC_Submit_Req(conn_id, GATTC_REQ_DISCOVERY, GATTC_DISC_CHAR_DSCPT, &req_param);
                         wmprintf("---Characteristics descriptors discovery Started---\r\n\n\n");
                        
                    } else {
                        /* Print the response */
                        for(i = 0; i < p_req_param->rsp_param.u.disc_rsp_param.numberOfPairs ; i++) {
                            wmprintf("Characteristic Handle : 0x%x\r\n", p_req_param->rsp_param.u.disc_rsp_param.u.char_pair[i].handle);
                            wmprintf("Properties: 0x%x\r\n", p_req_param->rsp_param.u.disc_rsp_param.u.char_pair[i].properties);
                            wmprintf("Characteristic Value Handle: 0x%x\r\n", p_req_param->rsp_param.u.disc_rsp_param.u.char_pair[i].charValueHandle);
                            wmprintf("Characteristic UUID : 0x%x\r\n\n", ((p_req_param->rsp_param.u.disc_rsp_param.u.char_pair[i].charUUID.len) == 2) ?
                                                            (p_req_param->rsp_param.u.disc_rsp_param.u.char_pair[i].charUUID.u.uuid16): 0x0000);
                        }
                    }
                }
                break;
                case GATTC_DISC_CHAR_DSCPT:
                {
                    uint8 CCC[2] = {0x01, 0x00}; /* To enable notification */
                    
                    if(p_req_param->rsp_param.u.disc_rsp_param.isComplete) {
                        wmprintf("----Completed All discovery Requests----\r\n\n\n");
                        wmprintf("---Configuring all notifications---\r\n\n\n");


                        for(i = 0; i < MAX_NUM_CCC &&  ccc_handle[i] ; i++){
                            /* Enabling notification */
                            req_param.write_param.handle = 0x13; //ccc_handle[i];
                            req_param.write_param.value_len = 2;
                            req_param.write_param.p_val = CCC;
                            
                            GATTC_Submit_Req(conn_id, GATTC_REQ_WRITE, GATTC_WRITE_NO_RSP, &req_param);
                        }
                            
                    } else{                        

                          /* Print the response */
                          for(i = 0; i < p_req_param->rsp_param.u.disc_rsp_param.numberOfPairs ; i++) {
                             if((p_req_param->rsp_param.u.disc_rsp_param.u.char_des_info_pair[i].UUID.len == 2 ) && \
                                (p_req_param->rsp_param.u.disc_rsp_param.u.char_des_info_pair[i].UUID.u.uuid16 == UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION))
                             {
                                 wmprintf("CCC Handle : 0x%x\r\n", p_req_param->rsp_param.u.disc_rsp_param.u.char_des_info_pair[i].handle);
                                 ccc_handle[ccc_index] =  p_req_param->rsp_param.u.disc_rsp_param.u.char_des_info_pair[i].handle;
                                 ccc_index++;

                             }
                        }
                    }
                }
                break;

                default:
                    break;
            }
        }
        break; //bread for GATTC_EVENT_DIS_RSP case
        
#if 0
        case GATTC_EVENT_WRITE_RSP:
            switch(p_req_param->rsp_param.sub_type)
            {
                case GATTC_WRITE:
                if(p_req_param->rsp_param.status == ATT_SUCCESS) {
                    wmprintf(" Handle =0x%x written successfully \r\n", p_req_param->rsp_param.u.write_rsp_param.handle);
                } else {
                    wmprintf(" Error code = 0x%x handle = 0x%x \r\n", p_req_param->rsp_param.status, p_req_param->rsp_param.u.write_rsp_param.handle);
                }
                break;

                default:
                break;
            }
           break; //GATTC_EVENT_WRITE_RSP
#endif
        case GATTC_EVENT_VALUE_IND_NOTI:
        {
            int i;
            
            if(p_req_param->ind_noti_param.isInd)
               wmprintf("Got Indication with handle = 0X%x\r\nValue is: \n", p_req_param->ind_noti_param.handle);
            else
               wmprintf("Got Notification with handle = 0X%x\r\nValue is: \n", p_req_param->ind_noti_param.handle);
            for(i =0 ; i< p_req_param->ind_noti_param.value_len ; i++) {
                wmprintf(" 0X%x ", *(p_req_param->ind_noti_param.p_value + i));
            }
            wmprintf(" \r\n");

            if(p_req_param->ind_noti_param.isInd)
                GATTC_Send_IndConfirm(conn_id, p_req_param->ind_noti_param.handle);
        }
           break; //GATTC_EVENT_VALUE_IND_NOTI
           
        default:
           break;
    }

}

tbt_remote_device remote_dev;
static void ble_gattc_connect(int argc, char *argv[])
{
    thandle_range handle_range = {0, 0xFFFF}; /* To receive all ind/noti*/

  if (argc == 4) {
     utils_get_device_addr(&argv[1], &remote_dev.device_id);
     remote_dev.device_id.device_type = BT_DEVICE_TYPE_BLE;
     remote_dev.device_id.address_type = atoi(argv[2]);

     GATTC_RegisterNotification(ble_gattc_handler, &remote_dev.device_id, handle_range); /* Register to receive all ind/notifications */
     GATTC_Connect(ble_gattc_handler, &remote_dev.device_id, atoi(argv[3]));
  }
  else {
      wmprintf("Usage: ble-gattc-connect <BLE device address> <adddress type> <connect type>\r\n");
      wmprintf("Take <BLE device address> string from Discovery result\r\n");
      wmprintf("<adddress type>  Set 0 for Public, 1 for random\r\n");
      wmprintf("<connect type>  Set 1for Direct connect, 0 for Indirect connect\r\n");
      return;
  }
}

static void ble_gattc_disconnect(int argc, char *argv[])
{
   if(cl_conn_id)
     GATT_Disconnect(&remote_dev.device_id, cl_conn_id);
}

static struct cli_command commands[] = {
      {"ble-opt", NULL, ble_app_cmd_menu},
      {"ble-create-bond",NULL, ble_create_bond},
      {"ble-gattc-connect",NULL, ble_gattc_connect},
      {"ble-gattc-disconnect",NULL, ble_gattc_disconnect},
};

static int ble_app_cli_init(void)
{
   int i;
   for (i = 0; i < sizeof(commands) / sizeof(struct cli_command); i++)
      if (cli_register_command(&commands[i]))
         return 1;
   return 0;
}



static void ble_app_handler(t_bt_gap_event_id gap_event_id,t_bt_gap_events_data* gap_event_data)
{
  
  //wmprintf("ble demo app handler: event id %d \r\n",gap_event_id);
  
  switch(gap_event_id)
  {
    case BT_ENABLE_COMPLETED_EVENT:
      {
        tbt_security_property_data sec_prop_data;
        
        wmprintf("BT_ENABLE_COMPLETED_EVENT\r\n");

        /* CLI Interface for BLE application */
        ble_app_cli_init();

	gatt_services_prof_init();

        sec_prop_data.bond_mode = BT_LE_GENERAL_BONDING_MODE;  /* No persistance storage */
        sec_prop_data.io_cap = BT_SEC_CAP_NO_IO;               /* To enforce, Just works model */
        sec_prop_data.key_size = 0x10;
        sec_prop_data.sec_level = BT_SEC_UNAUTHENTICATED_SECURITY_LEVEL;

        //Setting Security Properties
        config.len = sizeof(tbt_security_property_data);

        config.type = BT_CONFIG_T_SECURITY_PROPERTY_DATA;

        config.p.sec_prop_data = sec_prop_data;

        GAP_Config_Device(&config);
        return;

      }
      break;
    case BT_DISCOVERY_COMPLETED_EVENT:
      wmprintf("Discovery Completed \r\n");
      
      break;
    case BT_DISCOVERY_RESULTS_EVENT:
      {
        t_bt_device_report_data *disc_result = (t_bt_device_report_data*)gap_event_data;
        
        wmprintf(" BT_DISCOVERY_RESULTS_EVENT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X \r\n",
            disc_result->device_id.address[0],disc_result->device_id.address[1],
            disc_result->device_id.address[2],disc_result->device_id.address[3],
            disc_result->device_id.address[4],disc_result->device_id.address[5]);
        wmprintf(" Device Type %d Address Type %d \r\n",disc_result->device_id.device_type,
            disc_result->device_id.address_type);

        wmprintf(" RSSI value %d dbm \r\n", (int8)disc_result->rssi);
        wmprintf(" Device Name Len %d %s\r\n\n",strlen((const char *)disc_result->device_name), disc_result->device_name);
        
      }
      break;

    case BT_GAP_SEC_START_BOND_REQ_EVT:
      {
        t_bt_security_event_data  *sec_event_data = (t_bt_security_event_data*)gap_event_data;
        tbt_sec_events_resp_parms resp_params;

        wmprintf("ble demo app handler: BT_GAP_SEC_START_BOND_REQ_EVT: sec_event_data->bond_mode = %d \r\n", sec_event_data->bond_mode);
        resp_params.bond_mode = sec_event_data->bond_mode;
        resp_params.confirm = TRUE;
        resp_params.device_id = sec_event_data->device_id;
        resp_params.sec_event_id = sec_event_data->sec_event_id;
        GAP_Sec_Events_Response(&resp_params);
      }
      break;

    case BT_GAP_SEC_PASSKEY_DISPLAY_REQ_EVT:
      {  
        wmprintf("Passkey %ld\n",gap_event_data->sec_event_data.p.passkey);
        /*No Response Required*/
      }
      break;

    case BT_GAP_SEC_BONDING_COMPLETE_EVT:
      {  
        
        t_bt_security_event_data  *sec_event_data = (t_bt_security_event_data*)gap_event_data;
        
        wmprintf(" Bonding is completed Status %d Security Level %d\r\n",sec_event_data->p.sec_cmplt.result,
            sec_event_data->p.sec_cmplt.sec_level);
      }
      break;
    case BT_GAP_CONNECTION_PARAMS_UPDATED_EVT:
      {
        t_bt_ble_connect_updated_params *connParams = (t_bt_ble_connect_updated_params *)gap_event_data;
        
        wmprintf("UPDATED CONNECTION PARAMETERS: ConnINT=%x, ConnLatency=%x, Supervision_TO=%x, BT_RESULT=%d \r\n",connParams->conn_interval,connParams->conn_latency,
            connParams->super_tmo,connParams->result);
        
      }
      break;
    case BT_GAP_LINK_CONNECT_COMPLETE_EVT:
      {
        t_bt_connect_state_update_params *conn_params = (t_bt_connect_state_update_params *)gap_event_data;
        wmprintf("BT_GAP_LINK_CONNECT_COMPLETE_EVT: Status %d \r\n",conn_params->status); 
        wmprintf("GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X \r\n",
            conn_params->device_id.address[0],conn_params->device_id.address[1],
            conn_params->device_id.address[2],conn_params->device_id.address[3],
            conn_params->device_id.address[4],conn_params->device_id.address[5]);
        
        wmprintf("GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device Type %d Address Type %d \r\n",
                             conn_params->device_id.device_type,conn_params->device_id.address_type);



      }
      break;
    case BT_GAP_LINK_DISCONNECT_COMPLETE_EVT:
      {
        t_bt_connect_state_update_params *conn_params = (t_bt_connect_state_update_params *)gap_event_data;
        wmprintf("BT_GAP_LINK_DISCONNECT_COMPLETE_EVT: Status %d\r\n",conn_params->status); 
        wmprintf("GAP_CB:  BT_GAP_LINK_DISCONNECT_COMPLETE_EVT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X \r\n",
            conn_params->device_id.address[0],conn_params->device_id.address[1],
            conn_params->device_id.address[2],conn_params->device_id.address[3],
            conn_params->device_id.address[4],conn_params->device_id.address[5]);
      }
      break;
    case BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT:
      {
         t_bt_encrypt_state_change_params *encrypt_params = (t_bt_encrypt_state_change_params*)gap_event_data;
         wmprintf("BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Status %d \r\n",encrypt_params->status); 
         wmprintf("GAP_CB:  BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X \r\n",
                  encrypt_params->device_id.address[0],encrypt_params->device_id.address[1],
                  encrypt_params->device_id.address[2],encrypt_params->device_id.address[3],
                  encrypt_params->device_id.address[4],encrypt_params->device_id.address[5]);
      }
      break;
 #if 0     
    case BT_GAP_REMOTE_NAME_COMPLETE_EVENT:
      {
        
        t_bt_remote_name_complete_event_params *remote_name_complete_params = (t_bt_remote_name_complete_event_params*)gap_event_data;
        
        wmprintf("gap_test_cb: BT_GAP_REMOTE_NAME_COMPLETE_EVENT: Status %d""\r\n",remote_name_complete_params->status); 
        
        wmprintf("GAP_CB:  BT_GAP_REMOTE_NAME_COMPLETE_EVENT: Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X""\r\n",
                 remote_name_complete_params->device_id.address[0],remote_name_complete_params->device_id.address[1],
                 remote_name_complete_params->device_id.address[2],remote_name_complete_params->device_id.address[3],
                 remote_name_complete_params->device_id.address[4],remote_name_complete_params->device_id.address[5]);
        
        wmprintf("gap_test_cb: Remote Name : %s""\r\n",remote_name_complete_params->device_name);
        
      }
      break;
    case BT_GAP_SERVICE_SEARCH_RESULT_EVENT:
      {
         
         uint8 i;
         t_bt_service_search_result_event_params *service_search_params = (t_bt_service_search_result_event_params*)gap_event_data;
         
         wmprintf("gap_test_cb: BT_GAP_SERVICE_SEARCH_RESULT_EVENT: Num Services %d""\r\n",service_search_params->num_services);
         wmprintf("Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X""\r\n",
                 service_search_params->device_id.address[0],service_search_params->device_id.address[1],
                 service_search_params->device_id.address[2],service_search_params->device_id.address[3],
                 service_search_params->device_id.address[4],service_search_params->device_id.address[5]);
         
         wmprintf("Device Type %d Address Type %d""\r\n",service_search_params->device_id.device_type,
                                                             service_search_params->device_id.address_type);
         for(i=0;i<service_search_params->num_services;i++)
         {
           wmprintf("gap_test_cb: Service UUID : Len %d UUID 0x%x ""\r\n", 
             (service_search_params->service_params[i].uuid.len), (service_search_params->service_params[i].uuid.u.uuid16));
           
           wmprintf("gap_test_cb: Start Handle : 0x%x ""\r\n", service_search_params->service_params[i].startHandle);
           wmprintf("gap_test_cb: End Handle: 0x%x ""\r\n", service_search_params->service_params[i].endHandle);
           
         }
         
      }
      break;
    case BT_GAP_SERVICE_SEARCH_COMPLETE_EVENT:
      {
          t_bt_service_search_result_event_params *service_search_params = (t_bt_service_search_result_event_params*)gap_event_data;
          wmprintf("gap_test_cb: BT_GAP_SERVICE_SEARCH_COMPLETE_EVENT: ""\r\n");
          wmprintf("Device 0x%02X:0x%02X:0x%02X:0x%02X:0x%02X:0x%02X""\r\n",
                  service_search_params->device_id.address[0],service_search_params->device_id.address[1],
                  service_search_params->device_id.address[2],service_search_params->device_id.address[3],
                  service_search_params->device_id.address[4],service_search_params->device_id.address[5]);
          
          wmprintf("Device Type %d Address Type %d""\r\n",service_search_params->device_id.device_type,
                                                              service_search_params->device_id.address_type);
      }
      break;
 #endif 
    default:
      break;
  }
}

void sendMINRpt(int x, int y)
{
  sendReport_params.appid = appid;
  sendReport_params.connID = connID;
  sendReport_params.rpt_ID = HOGP_REPORT_ID_MOUSE;
  sendReport_params.rpt_type = HOGP_REPORT_TYPE_INPUT;
  sendReport_params.p_data = MINrpt;
  sendReport_params.rpt_len= sizeof(MINrpt);        

  MINrpt[1]=x;
  MINrpt[2]=y;

  Hidd_send_report_api(&sendReport_params);
}

void draw_mouse_circle(void)
{

   int8 x=0, y=0, i, j;
   sendMINRpt(x,y);
   os_thread_sleep(os_msec_to_ticks(500));

#if 1
   for(j=0; j<HOG_MOUSE_CIRCLE_NUM_OF_LOOP; j++)
   {
     for(y=0; y <= HOG_MOUSE_CIRCLE_RADIUS; y++)
     {
       x = y-HOG_MOUSE_CIRCLE_RADIUS;
       sendMINRpt(x,y);
       os_thread_sleep(os_msec_to_ticks(50));

     }
     for(x=0; x <= HOG_MOUSE_CIRCLE_RADIUS; x++)
     {
        y = HOG_MOUSE_CIRCLE_RADIUS-x;
	sendMINRpt(x,y);
	os_thread_sleep(os_msec_to_ticks(50));
     }
   
     for(i=0; i <= HOG_MOUSE_CIRCLE_RADIUS; i++)
     {   
	y = -i;
	x = HOG_MOUSE_CIRCLE_RADIUS+y;
	sendMINRpt(x,y);
	os_thread_sleep(os_msec_to_ticks(50));
     }
     for(i=0; i <= HOG_MOUSE_CIRCLE_RADIUS; i++)
     {
	x = -i;
	y = -(HOG_MOUSE_CIRCLE_RADIUS+x);
	sendMINRpt(x,y);
	os_thread_sleep(os_msec_to_ticks(50));
     }
    }
#endif


}

boolean isAuthorized()
{
   wmprintf("isAuthorized : authorization_flag %d \n",authorization_flag);
   if(authorization_flag)
   	return TRUE;
   else
   	return FALSE;
}

void wmsdk_ble_init()
{
	tblock_pool_config pool_config = {
		{{POOL_ID0_BUF0_REAL_SZ, POOL_ID0_BUF0_CNT,},
		{POOL_ID0_BUF1_REAL_SZ, POOL_ID0_BUF1_CNT,},
		{POOL_ID0_BUF2_REAL_SZ, POOL_ID0_BUF2_CNT,},
		{POOL_ID0_BUF3_REAL_SZ, POOL_ID0_BUF3_CNT}}
	};

	Stack_InitBufferPools(stackBufferPool,
		stackTrackBuf, &pool_config, ATT_MAX_MTU_SIZE);

	wmprintf("wmsdk ble init starting bluetooth \r\n");
	
	GAP_Enable_Bluetooth(ble_app_handler);
}
