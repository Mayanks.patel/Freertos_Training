/*
 *                Copyright 2014, Marvell International Ltd.
 * This code contains confidential information of Marvell Semiconductor, Inc.
 * No rights are granted herein under any patent, mask work right or copyright
 * of Marvell or any third party.
 * Marvell reserves the right at its sole discretion to request that this code
 * be immediately returned to Marvell. This code is provided "as is".
 * Marvell makes no warranties, express, implied or otherwise, regarding its
 * accuracy, completeness or performance.
 */

/*!
 * \file   scps_service.c
 * \brief  This file contains the implementation of the Scan Parameter Service Handling
 * \author Marvell Semiconductor
 */

#include "utils.h"
#include "service_conf.h"
#include "scps_service.h"
#include "main_gatt_api.h"
//#include "nvm_mgt.h"
#include "oss.h"

#define SCPS_SERVICE_VERSION  "MRVL-LE-SCPS-10.0.0.0"

#define DEFAULT_LE_SCAN_INTERVAL_VAL 0x10
#define DEFAULT_LE_SCAN_WINDOW_VAL 0x10

/***********************************************************************   
  Sturcture & Function Declarations   
 ************************************************************************/
static tgatt_uuid16_service_record ScPS_SrvcRecord;
static tgatt_uuid16_char_decl_record CharDecl_ScIntWin;
static tSCPS_ScIntWin_CharValDecl CharVal_ScIntWin;
static tgatt_uuid16_char_decl_record CharDecl_ScRefresh; 
static tSCPS_ScRef_CharValDecl CharVal_ScRefresh;
static tSCPS_ScRef_CCC ScRef_CCC;
static tgatts_cb_result scps_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data);
static void scps_load_client_context(tconn_id conn_id, tbt_device_id *p_peer_device_id);
static inline uint8 scps_fetch_cccid(tconn_id conn_id);



/***********************************************************************
  Global Variables & Data Sturctures    
 ************************************************************************/
static uint8 attr_val[MAX_SRVC_DATA] = {0};
static boolean scps_srvc_initialized = FALSE;
static tSRVC_ConnContext scps_cccIdx_connId[MAX_CONN_PER_DEV] = {{{{0}}}};


/* Primary service - Scan Parameter Service 
 ********** Start **************
 */
static tgatt_uuid16_service_record ScPS_SrvcRecord =
{
  /* SERVICE DEFINITION */
  NULL,                                                                        ///< Link pointer for all_service_q 
  {&CharDecl_ScIntWin, GATT_PRIM_SERVICE_RECORD_UUID16, GATT_START_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
  5,                                    ///< Service End handle, set this value to total number of attributes associated with this service excluding service record itself 
  UUID_DECL_PRIMARY_SERVICE,            ///< 0x2800 for «Primary Service» 
  scps_serv_req_handler,                ///< Service Callback
  UUID_SERVICE_SCAN_PARAMETERS,         ///< 0x1813 - for Bluetooth Scan Parameter service 
};

static tgatt_uuid16_char_decl_record CharDecl_ScIntWin =
{
  /* Characteristics Declaration */
  {&CharVal_ScIntWin, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,             ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_WRITE_NORESP,            ///< Write Without Response 
  GATT_NEXT_HANDLE,                     ///< Set to Next attribute record
  GATT_UUID_SCAN_INT_WINDOW             ///< UUID for Scan Interval Window characteristic 
};

static tSCPS_ScIntWin_CharValDecl CharVal_ScIntWin =
{
  /* Characteristics Value Declaration */
  {
    {&CharDecl_ScRefresh, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE}, ///< record header.next_ptr is set to address of next record of the service 
    GATT_UUID_SCAN_INT_WINDOW,                                                    ///< UUID for Scan Int Window 
    GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  {0x0,0x0}
};

static tgatt_uuid16_char_decl_record CharDecl_ScRefresh =
{
  /* Characteristics Declaration */
  {&CharVal_ScRefresh, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},           ///< record header.next_ptr is set to address of next record of the service 
  UUID_DECL_CHARACTERISTIC,             ///< 0x2803 for «Characteristics Declaration» 
  GATT_CH_PROP_NOTIFY,                  ///< Notify 
  GATT_NEXT_HANDLE,                     ///< Set to Next attribute record
  GATT_UUID_SCAN_REFRESH                ///< UUID for Scan Refresh 
};

static tSCPS_ScRef_CharValDecl CharVal_ScRefresh =
{
  /* Characteristics Value Declaration */
  {
    {&ScRef_CCC, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
    GATT_UUID_SCAN_REFRESH,                                                    ///< UUID for Scan Refresh 
    0x0
  },
  0x0
};

static tSCPS_ScRef_CCC ScRef_CCC =
{/* Need to Initialize all the Client Configuration Instances; Currently MAX_CONN_PER_DEV=1, hence initialized CCC value once to Zero*/
  /* Characteristics Value Declaration */
  {{NULL, GATT_CHAR_DESC_CCC_RECORD_STD, GATT_AUTO_HANDLE},                    ///< record header.next_ptr is set to address of next record of the service 
    UUID_DESCPTR_CLIENT_CHARACTERISTIC_CONFIGURATION,                          ///< UUID for CCC 
    GATT_PERM_READ_UNAUTH_SECURITY_LEVEL_WITH_ENC | GATT_PERM_WRITE_UNAUTH_SECURITY_LEVEL_WITH_ENC
  },
  {0x00}          /* defualt value */
};

/* Primary service - Scan Parameter Service 
 ********** End **************
 */


/*!
 * @brief  Scan Parameter Service Init API for APP/Profile.
 *            This API Call Instantiates a Scan Parameter Service Instance
 *
 * @param Void Takes No Parameters
 *
 * @return Void Returns None
 */
void scps_serv_init(void)
{
  uint8 ii;
  /* Add the scps service record to GATT Only once, as there can be only single instance for Scan Parameter Service*/
  if(!scps_srvc_initialized)
  {
    /* Create SCPS Service */
    GATTS_Add_Service((void *) &ScPS_SrvcRecord);
    scps_srvc_initialized = TRUE;

    /*Initialize SCPS Values*/
    for(ii=0; ii<MAX_CONN_PER_DEV; ii++)
    {
      CharVal_ScIntWin.ScIntWinVal[ii].le_scan_int = DEFAULT_LE_SCAN_INTERVAL_VAL;
      CharVal_ScIntWin.ScIntWinVal[ii].le_scan_win = DEFAULT_LE_SCAN_WINDOW_VAL;
	}
  }
  return;
}


/*!
 * @brief  Service Request Handler Registered with Local Gatt Server
 *            This Callback funtion handles all the Remote Gatt Client Requests & Events of this service
 *
 * @param conn_id Gatt Server Connection Id
 * @param req_type Gatt Server's request
 * @param p_req_data Request/Event Parameters
 *
 * @return Void Returns None
 */
static tgatts_cb_result scps_serv_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data)
{
  tgatts_cb_result result;
  static tgatts_rsp read_rsp;
  static tgatts_write_rsp write_rsp;
  uint8 ccc_idx = 0;

  result.status = ATT_SUCCESS;

  if(conn_id)
  {
    ccc_idx = scps_fetch_cccid(conn_id); //Get the ccc index from the conn_id
    
    #ifdef SRVC_DEBUG_ENABLED
      SRVC_DEBUG("scps_serv_req_handler: ConnID=%p, ccc_idx=%d",conn_id,ccc_idx);
    #endif
  }

  if(req_type == GATT_EVENT_CONNECTED)
  {
    /* Save the ConnID and corresponding DeviceID, if it was not stored earlier */
    scps_load_client_context(conn_id, (&((tgatt_conn_disc_param *)p_req_data)->device_id));
  }
  else if(req_type == GATT_EVENT_DISCONNECTED)
  {  
#if 0
    uint16 result;
    tSCPS_nvm_data nvm_data;
  
    /* Store  the NV data into persistent storage, if this device id is in Paired device List */
    nvm_data.valid_data = TRUE;
    nvm_data.scnRef_nty_Ind_ccc = ScRef_CCC.nty_Ind[ccc_idx];
    nvm_data.ScIntWinVal = CharVal_ScIntWin.ScIntWinVal[ccc_idx];

    /* Store  the NV data into persistent storage, if this device id is in Paired device List */
    if((result = NVM_mgt_write(scps_cccIdx_connId[ccc_idx].dev_id, SCPS_SIGN, sizeof(tSCPS_nvm_data), (void *)&nvm_data)) < 0)
    {
      #if ((defined SRVC_DEBUG_ENABLED) && (SRVC_DEBUG_ENABLED == TRUE))
      SRVC_DEBUG("%s: Client Configuration Context NOT SAVED!!, Reason=%d",__FUNCTION__,result);
      #endif
    }
              
    #if ((defined SRVC_DEBUG_ENABLED) && (SRVC_DEBUG_ENABLED == TRUE))
      SRVC_DEBUG("%s: Stored NV data is :\nValidData = %d\nScRef_CCC = %d\nScInt = %d\nScWin = %d\n",__FUNCTION__,
                  nvm_data.valid_data, ScRef_CCC.nty_Ind[ccc_idx], nvm_data.ScIntWinVal.le_scan_int,
                  nvm_data.ScIntWinVal.le_scan_win);
    #endif
#endif

    /* Clean the Connection Context */
    ScRef_CCC.nty_Ind[ccc_idx] = 0;
    CharVal_ScIntWin.ScIntWinVal[ccc_idx].le_scan_int = DEFAULT_LE_SCAN_INTERVAL_VAL;
    CharVal_ScIntWin.ScIntWinVal[ccc_idx].le_scan_win = DEFAULT_LE_SCAN_WINDOW_VAL;
    scps_cccIdx_connId[ccc_idx].conn_id = NULL;
    //oss_memset(&(scps_cccIdx_connId[cccid].dev_id),0,sizeof(scps_cccIdx_connId[cccid].dev_id));

  }
  else if(req_type == GATT_EVENT_READ_REQ) 
  {
    read_rsp.p_val = attr_val;
	uint8 *p_data = read_rsp.p_val;

    if(((tgatts_req *)p_req_data)->handle == ScRef_CCC.ScRef_ccc.header.handle)
    {
      UINT16_TO_LE_ENDIAN(ScRef_CCC.nty_Ind[ccc_idx], p_data);
      read_rsp.val_len = 0x02;//p_attr_value->value[1] - other bytes are already initialized to Zero
    }
    else
    {
      result.status = ATT_INVALID_HANDLE;
    }

    read_rsp.rsp_of = GATT_EVENT_READ_REQ;
    read_rsp.handle = ((tgatts_req *)p_req_data)->handle ;

    GATTS_Send_Rsp(conn_id, result.status, (tgatts_rsp *)&read_rsp); 
  }
  else if(req_type == GATT_EVENT_WRITE_CMD || req_type == GATT_EVENT_WRITE_REQ)
  {
    if(((tgatts_req *)p_req_data)->handle == CharVal_ScIntWin.ScIntWin_CharValDecl.header.handle)
    {
       if(((tgatts_req *)p_req_data)->val_len != 4)
       {
        result.status = ATT_INVALID_ATTR_VALUE_LEN; 
       }
       else
       {
         CharVal_ScIntWin.ScIntWinVal[ccc_idx].le_scan_int = ((((tgatts_req *)p_req_data)->p_value[1] << 8) & ((tgatts_req *)p_req_data)->p_value[0]);
         CharVal_ScIntWin.ScIntWinVal[ccc_idx].le_scan_win = ((((tgatts_req *)p_req_data)->p_value[3] << 8) & ((tgatts_req *)p_req_data)->p_value[2]);
       }
    }
    else if(((tgatts_req *)p_req_data)->handle == ScRef_CCC.ScRef_ccc.header.handle)
    {
      ScRef_CCC.nty_Ind[ccc_idx] = ((tgatts_req *)p_req_data)->p_value[0];
    }
    else
    {
      result.status = ATT_INVALID_HANDLE;
    }

    if(req_type == GATT_EVENT_WRITE_REQ)
    {
      write_rsp.rsp_of = GATT_EVENT_WRITE_REQ;
      write_rsp.handle = ((tgatts_req *)p_req_data)->handle ;
      GATTS_Send_Rsp(conn_id, result.status, (void *)&write_rsp); 
    }
  }

  return result;
}


/*!
 * @brief  Function to read the remote client's updated Scan Interval & Scan Window Values.
 *
 * @param p_char_value_param Pointer to Scan Interval & Scan Window Characteristic Values. Updated values are copied to this memory, which can be used by applications.
 * @param conn_id Gatt Server Connection Id
 *
 * @return Void Returns None
 */
void scps_srvc_read_ScanIntWin_char(tconn_id conn_id, ScIntWin_Char_Val *p_char_value_param)
{
  uint8 ccc_idx;
  
  if(!conn_id || !p_char_value_param)
    return;
  
  ccc_idx = scps_fetch_cccid(conn_id); //Get the ccc index from the conn_id	 

  p_char_value_param->le_scan_int = CharVal_ScIntWin.ScIntWinVal[ccc_idx].le_scan_int;
  p_char_value_param->le_scan_win = CharVal_ScIntWin.ScIntWinVal[ccc_idx].le_scan_win;

  return;
}


/*!
 * @brief  Function API to send Notification to Gatt Client
 *            This funtion sends Notification to remote gatt client, if the remote gatt client has 
 *            configured/enabled this Characteristic Value Notifications
 *
 * @param conn_id Gatt Server Connection Id
 * @param val_len Characteristic Value length
 * @param pval Pointer to Characteristic Value
 *
 * @return Void Returns None
 */
void scps_srvc_send_notify(tconn_id conn_id, uint16 val_len, uint8 * pval)
{
  uint16 attr_handle = CharVal_ScRefresh.ScRefresh_CharValDecl.header.handle;
  uint8 ccc_idx;

  if(!conn_id || !pval)
    return;

  ccc_idx = scps_fetch_cccid(conn_id); //Get the ccc index from the conn_id    

#ifdef SRVC_DEBUG_ENABLED
  SRVC_DEBUG("scps_srvc_send_notify: ConnID=%p, AttrHdl=%d, CCC_ID=%d, Nty_IND=%d",conn_id,attr_handle,ccc_idx,ScRef_CCC.nty_Ind[ccc_idx]);
#endif

  /* Send Notification to Gatt Client */
  if(ScRef_CCC.nty_Ind[ccc_idx] & TO_NOTIFY)
  {
#ifdef SRVC_DEBUG_ENABLED
    SRVC_DEBUG("SCPS Notification Sent, ConnID=%p, AttrHdl=%d",conn_id,attr_handle);
#endif
    GATTS_Send_Handlevalue(conn_id, attr_handle, FALSE, val_len, pval);
  }
}


/*!
 * @brief  Function API that returns Service Handle Range
 *            This funtion is used by other services, to include this service. In order to include this 
 *            service, the including service shall have to update its included service handle range, for which
 *            this function api is used.
 *            Note: This api shall only be called after adding this service/service init.
 *
 * @param conn_id Gatt Server Connection Id
 *
 * @return thandle_range Returns Service Handle Range(Start and End Handle)
 */
thandle_range scps_service_to_be_included(void)
{
  thandle_range handle_range = {0};

  handle_range.s_handle = ScPS_SrvcRecord.header.handle;
  handle_range.e_handle = ScPS_SrvcRecord.end_handle;

  return handle_range;
}


/*!
 * @brief  Function to save Service Connection Context
 *            This funtion is to save Connection ID & corresponding Device ID. Also if present, 
 *            NVM data is loaded.
 *
 * @param conn_id Gatt Server Connection Id 
 * @param p_peer_device_id Pointer to Remote Device Id
 *
 * @return Void Returns None
 */
static void scps_load_client_context(tconn_id conn_id, tbt_device_id *p_peer_device_id)
{
  uint8 ii;
//  tSCPS_nvm_data nvm_data;

  /* Now check for Max Connections */
  for(ii=0; !(scps_cccIdx_connId[ii].conn_id == NULL) && ii<MAX_CONN_PER_DEV ; ii++);

  if(ii<MAX_CONN_PER_DEV)
  {/* NOT exceeded MAX Number of Connections */

#if 0

    /* Load the NV data if available for this device id */
    if(( NVM_mgt_read(*p_peer_device_id, SCPS_SIGN, sizeof(tSCPS_nvm_data), (void *)&nvm_data) ) < 0)
      nvm_data.valid_data = FALSE;

    if(nvm_data.valid_data == TRUE)
    {
      ScRef_CCC.nty_Ind[ii] = nvm_data.scnRef_nty_Ind_ccc;
      CharVal_ScIntWin.ScIntWinVal[ii] = nvm_data.ScIntWinVal;
    }
	
    #if ((defined SRVC_DEBUG_ENABLED) && (SRVC_DEBUG_ENABLED == TRUE))
      SRVC_DEBUG("%s: Stored NV data is :\nValidData = %d\nScRef_CCC = %d\nScInt = %d\nScWin = %d\n",__FUNCTION__,
                      nvm_data.valid_data, nvm_data.scnRef_nty_Ind_ccc, nvm_data.ScIntWinVal.le_scan_int,
                      nvm_data.ScIntWinVal.le_scan_win);

      SRVC_DEBUG("scps_load_client_context: Save the Connid=%p & BD_Addr= [%02X %02X %02X %02X %02X %02X]\n",
               conn_id,(*p_peer_device_id).address[0],(*p_peer_device_id).address[1],(*p_peer_device_id).address[2],
               (*p_peer_device_id).address[3],(*p_peer_device_id).address[4],(*p_peer_device_id).address[5]);
    #endif
#endif
	
    scps_cccIdx_connId[ii].conn_id = conn_id;
    scps_cccIdx_connId[ii].dev_id = *p_peer_device_id;        
  }
  else
  {/* Exceeded MAX_CONN_PER_DEV, TODO: Disconnect the client */
    #ifdef SRVC_DEBUG_ENABLED
    SRVC_DEBUG("scps_load_client_context: Exceeded MAX_CONN_PER_DEV=%d, Hence OVERWRITING the older context\n",MAX_CONN_PER_DEV);
    #endif
  }
}


/*!
 * @brief  Function to Fetch Client Characteristic Configuration Index
 *            This Function is for Fetching the CCC index corresponding to Connection ID.
 *
 * @param conn_id Gatt Server Connection Id
 *
 * @return uint8 Returns CCC Index value for Particular Gatt connection Handle, which is mapped to its CCC.
 */
static inline uint8 scps_fetch_cccid(tconn_id conn_id)
{
  uint8 idx;

  for(idx=0; idx<MAX_CONN_PER_DEV && conn_id != scps_cccIdx_connId[idx].conn_id; idx++);

#ifdef SRVC_DEBUG_ENABLED
  if(idx >= MAX_CONN_PER_DEV)
    SRVC_DEBUG("[scps_fetch_cccid]: CONN ID NOT PRESENT!!, ConnID=%p, MAX_CONN_PER_DEV=%d, CCC_ID=%d\n",conn_id,MAX_CONN_PER_DEV,idx);
#endif

  return idx;
}

