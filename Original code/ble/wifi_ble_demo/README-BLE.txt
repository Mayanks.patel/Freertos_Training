
*** This document shows the wifi_ble_demo application usage ****

A. Pre-requiste:

   1. wifi_ble_demo app should be compiled and flashed on our board.
   2. Need a remote device with BLE support.

B. Steps to execute on console ( minicom) for demo.

1. When wifi_ble_demo.bin file is flashed, you will observe below prints, which indicate that firmware is loaded, 
   BT driver is registered and bluetooth stack is initialized.

# [sdio] Card detected
[sdio] Card reset successful
[sdio] Card Version - (0x32)
[sdio] Card initialization successful
[bt_drv] revision=0x10
[bt_drv] IOPORT : (0x10000)
[bt_drv] Firmware Ready
[bt_drv] BT driver initialized
[af] app_ctrl: prev_fw_version=0
[net] Initializing TCP/IP stack
ble demo app handler: event id 1
BT_ENABLE_COMPLETED_EVENT

2. Press "Enter" key to go to command prompt

3. Type "help" at command prompt to show ble related test options


# help

<snip>

ble-opt
ble-create-bond
ble-gattc-connect
ble-gattc-disconnect

<snip>

4. You can press respective options which shows its usage. for e.g. If you type ble-opt

# ble-opt 
Please enter valid option 
Usage: ble-opt <number>
 where number = 1 - Start advertising(broadcaster role)
       number = 2 - Stop advertising
       number = 3 - Start Discovery(observer role)
       number = 4 - Send Mouse reports(broadcaster role)
 

5.  To start advertising, so that remote devices can search us, use option 1

# ble-opt 1
Selected menu option =1
Start Advertising

  On a remote device, which supports BLE, make sure bluetooth is switched ON. If you perform discovery (Scanning) from remote device,
  device with name "MRVL-WMSDK" should now show up. This is friendly name of our Bluetooth device.

6. To stop advertisement, use option 2.

# ble-opt 2
 Selected menu option =2
 Stop Advertising


7. In order to perform scan to find nearby remote devices, use option 3.

    NOTE: Make sure, that remote device with BLE support has it's bluetooth switched ON and is advertising, only then the remote device would show up in our discovery
          result.

# ble-opt 3
Selected menu option =3
Start Scanning

  Scanning is performed for 10 seconds and it displays devices found on console . Sample output is as below:

ble demo app handler: event id 4
 BT_DISCOVERY_RESULTS_EVENT
 BT_DISCOVERY_RESULTS_EVENT: Device 0x8F:0xBA:0x21:0x43:0x50:0x00
 Device Type 1 Address Type 0
 RSSI value -40 dbm
 Device Name Len 10 MRVl_SDK-1

ble demo app handler: event id 4
 BT_DISCOVERY_RESULTS_EVENT
 BT_DISCOVERY_RESULTS_EVENT: Device 0x59:0xAD:0x21:0x43:0x50:0x00
 Device Type 1 Address Type 0
 RSSI value -72 dbm
 Device Name Len 8 MRVL SDK

ble demo app handler: BT_DISCOVERY_COMPLETED_EVENT Discovery Completed

 Number of devices found can vary in each scan. Also multiple entries of same device can be seen, since the remote device may be advertising for more than one time in 10 seconds.


8. If you want to create bond with any BLE enabled device, use ble-create-bond 
# ble-create-bond
Usage: ble-create-bond <BLE device address> <adddress type> Take <BLE device address> string from Discovery result, eg. AA:BB:CC:DD:EE:FF <adddress type>  Set 0 for Public, 1 for random

 e.g.
# ble-create-bond 0x23:0x03:0x11:0x9E:0x15:0x00 0

# opcode 1
BT_GAP_LINK_CONNECT_COMPLETE_EVT: Status 0
GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device 0x23:0x03:0x11:0x9E:0x15:0x00
GAP_CB: BT_GAP_LINK_CONNECT_COMPLETE_EVT: Device Type 1 Address Type 0 opcode 0
BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Status 0
GAP_CB:  BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT: Device 0x23:0x03:0x11:0x9E:0x15:0x00

#  Bonding is completed Status 0 Security Level 0 UPDATED CONNECTION PARAMETERS: ConnINT=6, ConnLatency=64, Supervision_TO=4e2, BT_RESULT=0



9. Once bonding is done, then for outgoing connection use ble-gattc-connect

# ble-gattc-connect
Usage: ble-gattc-connect <BLE device address> <adddress type> <connect type> Take <BLE device address> string from Discovery result <adddress type>  Set 0 for Public, 1 for random <connect type>  Set 1for Direct connect, 0 for Indirect connect

e.g

 # ble-gattc-connect 0x23:0x03:0x11:0x9E:0x15:0x00 0 1


---Primary Service discovery Started---


Discover Primary Service Response: 
Service UUID : 0x1800
Start Handle : 0x1
End Handle: 0x7 

Service UUID : 0x1801
Start Handle : 0x8
End Handle: 0x8 

Service UUID : 0x1812
Start Handle : 0x9
End Handle: 0x2c 

Discover Primary Service Response: 
Service UUID : 0x0
Start Handle : 0x2d
End Handle: 0x2f 

Discover Primary Service Response: 
Service UUID : 0x180f
Start Handle : 0x30
End Handle: 0x33 

Service UUID : 0x1813
Start Handle : 0x34
End Handle: 0x39 

Service UUID : 0x180a
Start Handle : 0x3a
End Handle: 0xffff 

Discover Primary Service Response: 
---Primary Service discovery Completed---


---Included Service discovery Started---


Discover Included Service Response: 
---Included Service discovery Completed---


---Characteristics discovery Started---


Discover Characteristic Response: 
Characteristic Handle : 0x2
Properties: 0xa
Characteristic Value Handle: 0x3
Characteristic UUID : 0x2a00

Characteristic Handle : 0x4
Properties: 0x2
Characteristic Value Handle: 0x5
Characteristic UUID : 0x2a01

Characteristic Handle : 0x6
Properties: 0x2
Characteristic Value Handle: 0x7
Characteristic UUID : 0x2a04

Discover Characteristic Response: 
Characteristic Handle : 0xa
Properties: 0x2
Characteristic Value Handle: 0xb
Characteristic UUID : 0x2a4a

Characteristic Handle : 0xc
Properties: 0x2
Characteristic Value Handle: 0xd
Characteristic UUID : 0x2a4b

Characteristic Handle : 0xe
Properties: 0x12
Characteristic Value Handle: 0xf
Characteristic UUID : 0x2a33

Discover Characteristic Response: 
Characteristic Handle : 0x11
Properties: 0x12
Characteristic Value Handle: 0x12
Characteristic UUID : 0x2a4d

Characteristic Handle : 0x15
Properties: 0xe
Characteristic Value Handle: 0x16
Characteristic UUID : 0x2a4d

Characteristic Handle : 0x18
Properties: 0x12
Characteristic Value Handle: 0x19
Characteristic UUID : 0x2a4d

Discover Characteristic Response: 
Characteristic Handle : 0x1c
Properties: 0xa
Characteristic Value Handle: 0x1d
Characteristic UUID : 0x2a4d

Characteristic Handle : 0x20
Properties: 0xa
Characteristic Value Handle: 0x21
Characteristic UUID : 0x2a4d

Characteristic Handle : 0x23
Properties: 0xa
Characteristic Value Handle: 0x24
Characteristic UUID : 0x2a4d

Discover Characteristic Response: 
Characteristic Handle : 0x26
Properties: 0xa
Characteristic Value Handle: 0x27
Characteristic UUID : 0x2a4d

Characteristic Handle : 0x29
Properties: 0x4
Characteristic Value Handle: 0x2a
Characteristic UUID : 0x2a4c

Characteristic Handle : 0x2b
Properties: 0x6
Characteristic Value Handle: 0x2c
Characteristic UUID : 0x2a4e

Discover Characteristic Response: 
Characteristic Handle : 0x2e
Properties: 0xa
Characteristic Value Handle: 0x2f
Characteristic UUID : 0x0

Discover Characteristic Response: 
Characteristic Handle : 0x31
Properties: 0x12
Characteristic Value Handle: 0x32
Characteristic UUID : 0x2a19

Characteristic Handle : 0x35
Properties: 0x4
Characteristic Value Handle: 0x36
Characteristic UUID : 0x2a4f

Characteristic Handle : 0x37
Properties: 0x10
Characteristic Value Handle: 0x38
Characteristic UUID : 0x2a31

Discover Characteristic Response: 
Characteristic Handle : 0x3b
Properties: 0x2
Characteristic Value Handle: 0x3c
Characteristic UUID : 0x2a25

Characteristic Handle : 0x3d
Properties: 0x2
Characteristic Value Handle: 0x3e
Characteristic UUID : 0x2a24

Characteristic Handle : 0x3f
Properties: 0x2
Characteristic Value Handle: 0x40
Characteristic UUID : 0x2a27

Discover Characteristic Response: 
Characteristic Handle : 0x41
Properties: 0x2
Characteristic Value Handle: 0x42
Characteristic UUID : 0x2a26

Characteristic Handle : 0x43
Properties: 0x2
Characteristic Value Handle: 0x44
Characteristic UUID : 0x2a28

Characteristic Handle : 0x45
Properties: 0x2
Characteristic Value Handle: 0x46
Characteristic UUID : 0x2a29

Discover Characteristic Response: 
Characteristic Handle : 0x47
Properties: 0x2
Characteristic Value Handle: 0x48
Characteristic UUID : 0x2a50

Discover Characteristic Response: 
---Characteristics discovery Completed---


---Characteristics descriptors discovery Started---


CCC Handle : 0x10
CCC Handle : 0x13
CCC Handle : 0x1a
CCC Handle : 0x33
CCC Handle : 0x39
----Completed All discovery Requests----


---Configuring all notifications---


 Note: If you now move mouse, you should observe reports on console indicating data is received.

Got Notification with handle = 0X12
Value is: 
           0X0  0X0  0X2  0X0  0X0  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X10  0X0  0X3  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X1  0X0  0X0  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X2  0X0  0X0  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X1  0X0  0X0  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X1  0X0  0X0  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0Xc  0X0  0Xff  0Xff  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X11  0X0  0Xff  0Xff  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X2  0X0  0X0  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0X0  0X0  0X4  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0Xfe  0Xff  0Xc  0X0  0X0  0X0 Got Notification with handle = 0X12 Value is: 
           0X0  0X0  0Xff  0Xff  0X4  0X0  0X0  0X0  



10. To initiate disconnect, use ble-gattc-disconnect 

# ble-gattc-disconnect

# Disconnected with GATT server
[HOGP TEST APP][CLOSE_EVT]Stop Idle Timer, ID
BT_GAP_LINK_DISCONNECT_COMPLETE_EVT: Status 22
GAP_CB:  BT_GAP_LINK_DISCONNECT_COMPLETE_EVT: Device 0x23:0x03:0x11:0x9E:0x15:0x00 



11. In the event of remote device has initiated connection, and you want to send GATT data to remote, you can use the below option

#ble-opt 4 

 This will send HID mouse events to remote device. If it is a phone with a display you should see a moving pointer to indicate mouse events are being received by remote device.


