/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/* Application demonstrating secure firmware upgrades
 *
 * Summary:
 *
 * This application showcases the ED25519-Chacha20 and RSA-AES
 * firmware upgrade support in http server mode as well as http
 * client mode.
 *
 * Description:
 *
 * In order to use firmware upgrades, the device first needs to be provisioned.
 * If the device boots up in unconfigured mode, it starts the micro-AP
 *
 * The device then starts the web-server which will listen to incoming http
 * connections (e.g, from a browser).
 *
 * Device can be accessed at URI http://192.168.10.1
 * Through the Web UI, user can provision the device to an Access Point.
 * Once the device is provisioned successfully, the IP address will be
 * printed on the console. This can be then used to demonstrate firmware
 * upgrade.
 *
 * By default, ED25519-Chacha20 based upgrades are enabled. This can be
 * changed to RSA-AES by changing the config option in
 * fw_upgrade_demo/config.mk
 *
 * Two upgrade APIs are exposed by this app.
 * 1. Client mode firmware upgrades
 * curl -d '{"url":"<link/to/fw.bin.upg"}' http://<ip>/fw_url
 * - The secure upgrade image is fetched from the remote server based on
 *   the url.
 *
 * 2. Server mode firmware upgrades
 * curl -F 'file=@fw.bin.upg' http://<ip>/fw_data
 * - The secure upgrade image is POSTed directly to the device
 *
 * For details on steps to generate the secure upgrade image, refer to the
 * README.ed_chacha and README.rsa_aesctr files in wmsdk/tools/src/fw_generator
 *
 */
#include <wm_os.h>
#include <app_framework.h>
#include <appln_cb.h>
#include <appln_dbg.h>
#include <cli.h>
#include <wmstdio.h>
#include <wm_net.h>
#include <reset_prov_helper.h>
#include <httpd.h>
#include <led_indicator.h>
#include <board.h>
#include <wmtime.h>
#include <psm.h>
#include <ftfs.h>
#include <mdev_gpio.h>


/*-----------------------Global declarations----------------------*/
static char g_uap_ssid[IEEEtypes_SSID_SIZE + 1];
appln_config_t appln_cfg = {
	.passphrase = "marvellwm",
	.wps_pb_gpio = -1,
	.reset_prov_pb_gpio = -1
};

int ftfs_api_version = 100;
char *ftfs_part_name = "ftfs";

struct fs *fs;

static os_timer_t uap_down_timer;

#define UAP_DOWN_TIMEOUT (30 * 1000)

#define NETWORK_MOD_NAME	"network"
#define VAR_UAP_SSID		"uap_ssid"
#define VAR_PROV_KEY            "prov_key"

/** Provisioning done timer call back function
 * Once the provisioning is done, we wait for provisioning client to send
 * AF_EVT_PROV_CLIENT_DONE which stops uap and dhcp server. But if any case
 * client doesn't send AF_EVT_PROV_CLIENT_DONE event, then we wait for
 * 60seconds(timer) to shutdown UAP.
 */
static void uap_down_timer_cb(os_timer_arg_t arg)
{
	if (is_uap_started()) {
		app_uap_stop();
	}
}

/* This function initializes the SSID with the PSM variable network.uap_ssid
 * If the variable is not found, the default value is used.
 * To change the ssid, please set the network.uap_ssid variable
 * from the console.
 */
void appln_init_ssid()
{
	if (psm_get_single(NETWORK_MOD_NAME, VAR_UAP_SSID, g_uap_ssid,
			sizeof(g_uap_ssid)) == WM_SUCCESS) {
		dbg("Using %s as the uAP SSID", g_uap_ssid);
		appln_cfg.ssid = g_uap_ssid;
		appln_cfg.hostname = g_uap_ssid;
	} else {
			uint8_t my_mac[6];

			memset(g_uap_ssid, 0, sizeof(g_uap_ssid));
			wlan_get_mac_address(my_mac);
			/* Provisioning SSID */
			snprintf(g_uap_ssid, sizeof(g_uap_ssid),
				 "fwupg_demo-%02X%02X", my_mac[4], my_mac[5]);
			dbg("Using %s as the uAP SSID", g_uap_ssid);
			appln_cfg.ssid = g_uap_ssid;
			appln_cfg.hostname = g_uap_ssid;
	}
}

/*
 * A simple HTTP Web-Service Handler
 *
 * Returns the string "Hello World" when a GET on http://<IP>/hello
 * is done.
 */

static void app_reboot_cb()
{
	ll_printf("Rebooting...");
	pm_reboot_soc();
}

static int reboot_device()
{
	static os_timer_t app_reboot_timer;
	if (!app_reboot_timer) {
		if (os_timer_create(&app_reboot_timer, "app_reboot_timer",
					os_msec_to_ticks(2000),
					app_reboot_cb, NULL,
					OS_TIMER_ONE_SHOT,
					OS_TIMER_AUTO_ACTIVATE)
				!= WM_SUCCESS)
			return -WM_FAIL;

	}
	return WM_SUCCESS;
}

static int server_mode_data_fetch_cb(void *priv, void *buf, uint32_t max_len)
{
	int sockfd = (int)priv;
	int size_read = httpd_recv(sockfd, buf, max_len, 0);
	if (size_read < 0) {
		dbg("Server mode: Could not read data from stream");
		return -WM_FAIL;
	}
	return size_read;
}
#ifdef APPCONFIG_FWUPG_ED_CHACHA
#include <fw_upgrade_ed_chacha.h>
const uint8_t fwupg_verification_key[] =
	"cfedc540a8f11a6dbd58148570cda63b6d194367b5166b85ce8fc711b5cff680";
const uint8_t fwupg_encrypt_decrypt_key[] =
	"12f145a764fb412b6a0f9eecbc7c831df585816d267e47dfd636bd9d3bf94bdf";

int client_mode_upgrade_fw(char *url)
{
	uint8_t decrypt_key[ED_CHACHA_DECRYPT_KEY_LEN];
	uint8_t verification_key[ED_CHACHA_VERIFICATION_KEY_LEN];
	hex2bin(fwupg_verification_key, verification_key,
			sizeof(verification_key));
	hex2bin(fwupg_encrypt_decrypt_key, decrypt_key,
			sizeof(decrypt_key));
	return ed_chacha_client_mode_upgrade_fw(FC_COMP_FW,
			url, verification_key, decrypt_key);
}
int server_mode_upgrade_fw(int sock)
{
	uint8_t decrypt_key[ED_CHACHA_DECRYPT_KEY_LEN];
	uint8_t verification_key[ED_CHACHA_VERIFICATION_KEY_LEN];
	hex2bin(fwupg_verification_key, verification_key,
			sizeof(verification_key));
	hex2bin(fwupg_encrypt_decrypt_key, decrypt_key,
			sizeof(decrypt_key));
	return ed_chacha_server_mode_upgrade_fw(FC_COMP_FW,
			verification_key, decrypt_key,
			server_mode_data_fetch_cb, (void *)sock);

}
#endif /* !APPCONFIG_FWUPG_ED_CHACHA */

#ifdef APPCONFIG_FWUPG_RSA_AES
#include <fw_upgrade_rsa_aes.h>
int client_mode_upgrade_fw(char *url)
{
	return -WM_FAIL;
}
int server_mode_upgrade_fw(int sock)
{
	return -WM_FAIL;
}
#endif /* !APPCONFIG_FWUPG_RSA_AES */

#define NUM_TOKENS	5

int fw_url_handler(httpd_request_t *req)
{
	char buf[128];
	char fw_url[100];
	int ret;

	ret = httpd_get_data(req, buf, sizeof(buf));
	if (ret < 0) {
		dbg("Failed to get post request data");
		goto end;
	}
	/* Parse the JSON data */
	jsontok_t json_tokens[NUM_TOKENS];
	jobj_t jobj;
	ret = json_init(&jobj, json_tokens, NUM_TOKENS,
			buf, req->body_nbytes);
	if (ret != WM_SUCCESS) {
		goto end;
	}
	ret = json_get_val_str(&jobj, "url", fw_url, sizeof(fw_url));
	if (ret != WM_SUCCESS) {
		dbg("Failed to get URL");
		goto end;
	}
	ret = client_mode_upgrade_fw(fw_url);

end:
	if (ret == WM_SUCCESS) {
		ret = httpd_send_response(req, HTTP_RES_200,
				HTTPD_JSON_SUCCESS,
			strlen(HTTPD_JSON_SUCCESS), HTTP_CONTENT_JSON_STR);
		reboot_device();
	} else
		ret = httpd_send_response(req, HTTP_RES_200,
				HTTPD_JSON_ERROR,
			strlen(HTTPD_JSON_ERROR), HTTP_CONTENT_JSON_STR);
	return ret;
}
extern char *app_http_content_type_mp_form;
extern int app_http_get_mp_form_length(httpd_request_t *req, int sock,
				char *scratch, int scratch_len);
extern int app_consume_http_mp_end(int sock, char *scratch, int scratch_len);
int fw_data_handler(httpd_request_t *req)
{
	bool mp_header;
	char buf[512];
	int file_length;
	int ret = httpd_parse_hdr_tags(req, req->sock, buf,
			sizeof(buf));
	if (ret != WM_SUCCESS) {
		dbg("Failed to parse HTTP headers\r\n");
		goto end;
	}
	if (!strncmp(app_http_content_type_mp_form, req->content_type,
		     strlen(app_http_content_type_mp_form))) {
		if ((file_length =
		     app_http_get_mp_form_length(req, req->sock, buf,
					sizeof(buf))) == -WM_FAIL) {
			ret = -WM_FAIL;
			goto end;
		}
		/* multipart-header present in request */
		mp_header = 1;
	} else
		file_length = req->body_nbytes;
	dbg("Received File length = %d", file_length);
	ret = server_mode_upgrade_fw(req->sock);
	if (mp_header) {
		int err1 = app_consume_http_mp_end(req->sock, buf,
				sizeof(buf));
		if (err1 != WM_SUCCESS)
			dbg("recv failed for mp header boundary");
	}
end:
	if (ret == WM_SUCCESS) {
		ret = httpd_send_response(req, HTTP_RES_200,
				HTTPD_JSON_SUCCESS,
				strlen(HTTPD_JSON_SUCCESS),
				HTTP_CONTENT_JSON_STR);
		reboot_device();
	} else {
		ret = httpd_send_response(req, HTTP_RES_200,
				HTTPD_JSON_ERROR,
				strlen(HTTPD_JSON_ERROR),
				HTTP_CONTENT_JSON_STR);
	}
	return ret;
}
struct httpd_wsgi_call wm_demo_http_handlers[] = {
	{"/fw_url", HTTPD_DEFAULT_HDR_FLAGS, 0,
	NULL, fw_url_handler, NULL, NULL},
	{"/fw_data", HTTPD_DEFAULT_HDR_FLAGS, 0,
	NULL, fw_data_handler, NULL, NULL},
};

static int wm_demo_handlers_no =
	sizeof(wm_demo_http_handlers) / sizeof(struct httpd_wsgi_call);


/* This function is defined for handling critical error.
 * For this application, we just stall and do nothing when
 * a critical error occurs.
 *
 */
void appln_critical_error_handler(void *data)
{
	while (1)
		;
	/* do nothing -- stall */
}

/*
 * Register Web-Service handlers
 *
 */
int register_httpd_handlers()
{
	return httpd_register_wsgi_handlers(wm_demo_http_handlers,
		wm_demo_handlers_no);
}

/* This function must initialize the variables required (network name,
 * passphrase, etc.) It should also register all the event handlers that are of
 * interest to the application.
 */
int appln_config_init()
{
	/* Initialize reset to provisioning push button settings */
	appln_cfg.reset_prov_pb_gpio = board_button_2();
	return 0;
}

/*-----------------------Local declarations----------------------*/
static int provisioned;

/*
 * Event: INIT_DONE
 *
 * The application framework is initialized.
 *
 * The data field has information passed by boot2 bootloader
 * when it loaded the application.
 *
 * ?? What happens if app is loaded via jtag
 */
static void event_init_done(void *data)
{
#if APPCONFIG_DEBUG_ENABLE
	struct app_init_state *state;
	state = (struct app_init_state *)data;
#endif /* APPCONFIG_DEBUG_ENABLE */

	dbg("Event: INIT_DONE");
	dbg("Factory reset bit status: %d", state->factory_reset);
	dbg("Booting from backup firmware status: %d", state->backup_fw);
	dbg("Previous reboot cause: %u", state->rst_cause);

	int err = os_timer_create(&uap_down_timer,
				  "uap-down-timer",
				  os_msec_to_ticks(UAP_DOWN_TIMEOUT),
				  &uap_down_timer_cb,
				  NULL,
				  OS_TIMER_ONE_SHOT,
				  OS_TIMER_NO_ACTIVATE);
	if (err != WM_SUCCESS) {
		dbg("Unable to start uap down timer");
	}
}

/*
 * Handler invoked on WLAN_INIT_DONE event.
 *
 * When WLAN is started, the application framework looks to
 * see whether a home network information is configured
 * and stored in PSM (persistent storage module).
 *
 * The data field returns whether a home network is provisioned
 * or not, which is used to determine what network interfaces
 * to start (station, micro-ap, or both).
 *
 * If provisioned, the station interface of the device is
 * connected to the configured network.
 *
 * Else, Micro-AP network is configured.
 *
 * (If desired, the Micro-AP network can also be started
 * along with the station interface.)
 *
 * We also start all the services which don't need to be
 * restarted between provisioned and non-provisioned mode
 * or between connected and disconnected state.
 *
 * Accordingly:
 *      -- Start mDNS and advertize services
 *	-- Start HTTP Server
 *	-- Register WSGI handlers for HTTP server
 */
static void event_wlan_init_done(void *data)
{
	int ret;
	/* We receive provisioning status in data */
	provisioned = (int)data;

	dbg("Event: WLAN_INIT_DONE provisioned=%d", provisioned);

	/* Initialize ssid to be used for uAP mode */
	appln_init_ssid();

	if (provisioned) {
		app_sta_start();
	} else {
		app_uap_start_with_dhcp(appln_cfg.ssid, appln_cfg.passphrase);
	}

	if (provisioned)
		hp_configure_reset_prov_pushbutton();

	/*
	 * Start http server and enable webapp in the
	 * FTFS partition on flash
	 */
	ret = app_httpd_with_fs_start(ftfs_api_version, ftfs_part_name, &fs);
	if (ret != WM_SUCCESS)
		dbg("Error: Failed to start HTTPD");

	/*
	 * Register /hello http handler
	 */
	register_httpd_handlers();

	/*
	 * Initialize CLI Commands for some of the modules:
	 *
	 * -- psm:  allows user to check data in psm partitions
	 * -- ftfs: allows user to see contents of ftfs
	 * -- wlan: allows user to explore basic wlan functions
	 */

	ret = psm_cli_init();
	if (ret != WM_SUCCESS)
		dbg("Error: psm_cli_init failed");

	if (!provisioned) {
		/* Start Slow Blink */
		led_slow_blink(board_led_2());
	}
}

/*
 * Event: Micro-AP Started
 *
 * If we are not provisioned, then start provisioning on
 * the Micro-AP network.
 *
 * Also, enable WPS.
 *
 * Since Micro-AP interface is UP, announce mDNS service
 * on the Micro-AP interface.
 */
static void event_uap_started(void *data)
{

	dbg("Event: Micro-AP Started");
	if (!provisioned) {
		dbg("Starting provisioning");
		app_provisioning_start(PROVISIONING_WLANNW);

	}
}


/*
 * Event: PROV_DONE
 *
 * Provisioning is complete. We can stop the provisioning
 * service.
 *
 * Stop WPS.
 *
 * Enable Reset to Prov Button.
 */
static void event_prov_done(void *data)
{
	hp_configure_reset_prov_pushbutton();
	app_provisioning_stop();
	dbg("Provisioning successful");
}

/* Event: PROV_CLIENT_DONE
 *
 * Provisioning Client has terminated session.
 *
 * We can now safely stop the Micro-AP network.
 *
 * Note: It is possible to keep the Micro-AP network alive even
 * when the provisioning client is done.
 */
static void event_prov_client_done(void *data)
{
	int ret;

	ret = app_uap_stop();
	if (ret != WM_SUCCESS)
		dbg("Error: Failed to Stop Micro-AP");
}

/*
 * Event UAP_STOPPED
 *
 * Normally, we will get here when provisioning is complete,
 * and the Micro-AP network is brought down.
 *
 * If we are connected to an AP, we can enable IEEE Power Save
 * mode here.
 */
static void event_uap_stopped(void *data)
{
	dbg("Event: Micro-AP Stopped");
}

/*
 * Event: CONNECTING
 *
 * We are attempting to connect to the Home Network
 *
 * Note: We can come here:
 *
 *   1. After boot -- if already provisioned.
 *   2. After provisioning
 *   3. After link loss
 *
 * This is just a transient state as we will either get
 * CONNECTED or have a CONNECTION/AUTH Failure.
 *
 */
static void event_normal_connecting(void *data)
{
	net_dhcp_hostname_set(appln_cfg.hostname);
	dbg("Connecting to Home Network");
	/* Start Fast Blink */
	led_fast_blink(board_led_2());
}

/* Event: AF_EVT_NORMAL_CONNECTED
 *
 * Station interface connected to home AP.
 *
 * Network dependent services can be started here. Note that these
 * services need to be stopped on disconnection and
 * reset-to-provisioning event.
 */
static void event_normal_connected(void *data)
{
	char ip[16];

	led_off(board_led_2());

	led_on(board_led_1());

	app_network_ip_get(ip);
	dbg("Connected to Home Network with IP address = %s", ip);

	/*
	 * If micro AP interface is up
	 * queue a timer which will take
	 * micro AP interface down.
	 */
	if (is_uap_started()) {
		os_timer_activate(&uap_down_timer);
		return;
	}
}

/*
 * Event: CONNECT_FAILED
 *
 * We attempted to connect to the Home AP, but the connection did
 * not succeed.
 *
 * This typically indicates:
 *
 * -- Authentication failed.
 * -- The access point could not be found.
 * -- We did not get a valid IP address from the AP
 *
 */
static void event_connect_failed(void *data)
{
	char failure_reason[32];

	if (*(app_conn_failure_reason_t *)data == AUTH_FAILED)
		strcpy(failure_reason, "Authentication failure");
	if (*(app_conn_failure_reason_t *)data == NETWORK_NOT_FOUND)
		strcpy(failure_reason, "Network not found");
	if (*(app_conn_failure_reason_t *)data == DHCP_FAILED)
		strcpy(failure_reason, "DHCP failure");

	os_thread_sleep(os_msec_to_ticks(2000));
	dbg("Application Error: Connection Failed: %s", failure_reason);
	led_off(board_led_1());
}

/*
 * Event: USER_DISCONNECT
 *
 * This means that the application has explicitly requested a network
 * disconnect
 *
 */
static void event_normal_user_disconnect(void *data)
{
	led_off(board_led_1());
	dbg("User disconnect");
}

/*
 * Event: LINK LOSS
 *
 * We lost connection to the AP.
 *
 * The App Framework will attempt to reconnect. We dont
 * need to do anything here.
 */
static void event_normal_link_lost(void *data)
{
	dbg("Link Lost");
}

static void event_normal_reset_prov(void *data)
{
	led_off(board_led_1());
	/* Start Slow Blink */
	led_slow_blink(board_led_2());

	/* Cancel the UAP down timer timer */
	os_timer_deactivate(&uap_down_timer);

	/* Reset to provisioning */
	provisioned = 0;
	hp_unconfigure_reset_prov_pushbutton();
	if (is_uap_started() == false) {
		app_uap_start_with_dhcp(appln_cfg.ssid, appln_cfg.passphrase);
	} else {
		app_provisioning_start(PROVISIONING_WLANNW);
	}
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_INIT_DONE:
		event_init_done(data);
		break;
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTING:
		event_normal_connecting(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_NORMAL_CONNECT_FAILED:
		event_connect_failed(data);
		break;
	case AF_EVT_NORMAL_LINK_LOST:
		event_normal_link_lost(data);
		break;
	case AF_EVT_NORMAL_USER_DISCONNECT:
		event_normal_user_disconnect(data);
		break;
	case AF_EVT_NORMAL_RESET_PROV:
		event_normal_reset_prov(data);
		break;
	case AF_EVT_UAP_STARTED:
		event_uap_started(data);
		break;
	case AF_EVT_UAP_STOPPED:
		event_uap_stopped(data);
		break;
	case AF_EVT_PROV_DONE:
		event_prov_done(data);
		break;
	case AF_EVT_PROV_CLIENT_DONE:
		event_prov_client_done(data);
		break;
	default:
		break;
	}

	return 0;
}

static void modules_init()
{
	int ret;

	ret = wmstdio_init(UART0_ID, 0);
	if (ret != WM_SUCCESS) {
		dbg("Error: wmstdio_init failed");
		appln_critical_error_handler((void *) -WM_FAIL);
	}

	ret = cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: cli_init failed");
		appln_critical_error_handler((void *) -WM_FAIL);
	}
	/*
	 * Initialize Power Management Subsystem
	 */
	ret = pm_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: pm_init failed");
		appln_critical_error_handler((void *) -WM_FAIL);
	}

	/*
	 * Register Power Management CLI Commands
	 */
	ret = pm_cli_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: pm_cli_init failed");
		appln_critical_error_handler((void *) -WM_FAIL);
	}

	ret = gpio_drv_init();
	if (ret != WM_SUCCESS) {
		dbg("Error: gpio_drv_init failed");
		appln_critical_error_handler((void *) -WM_FAIL);
	}

	return;
}

int main()
{
	modules_init();

	dbg("Build Time: " __DATE__ " " __TIME__ "");

	appln_config_init();

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg("Failed to start application framework");
		appln_critical_error_handler((void *) -WM_FAIL);
	}
	return 0;
}
