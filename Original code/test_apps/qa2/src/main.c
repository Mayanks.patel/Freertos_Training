/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

#define CONFIG_PSM_TESTS
#include <wmstdio.h>
#include <wm_os.h>
#include <nw_utils.h>
#include <board.h>
#include <wmtime.h>
#include <cli.h>
#include <mdev_pm.h>


int psm_test_cli_init();

int modules_init()
{
	int ret = wmstdio_init(UART0_ID, 0);

	ret = cli_init();
	if (ret != WM_SUCCESS) {
		wmprintf("cli init failed: %d\r\n", ret);
		return ret;
	}
	return WM_SUCCESS;
}
int modules_cli_init()
{
	int ret;
	ret = wmtime_cli_init();
	if (ret != WM_SUCCESS) {
		wmprintf("wmtime cli init failed: %d\r\n", ret);
		return ret;
	}
	ret = psm_test_cli_init();
	if (ret != WM_SUCCESS) {
		wmprintf("psm_test_cli_init failed: %d\r\n", ret);
	}

	return WM_SUCCESS;
}



/*
 * The application entry point is main(). At this point, minimal
 * initialization of the hardware is done, and also the RTOS is
 * initialized.
 */
int main(void)
{
	int ret;
	ret = modules_init();
	if (ret != WM_SUCCESS) {
		wmprintf("module init failed: %d\r\n", ret);
		return ret;
	}

	ret = modules_cli_init();
	if (ret != WM_SUCCESS) {
		wmprintf("modules cli init failed: %d\r\n", ret);
		return ret;
	}

	return WM_SUCCESS;
}
